const elixir = require('laravel-elixir');
require('laravel-elixir-vue-2');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix)
{
    /*admin*/
    mix.webpack('admin/install.js', 'public/js/admin/install.js');
    mix.webpack('admin/users/app.js', 'public/js/admin/users/app.js');
    mix.scripts([
        "vendors/modernizr/modernizr.custom.js",
        "vendors/jQuery-Storage-API/jquery.storageapi.js",
        "vendors/jquery.easing/js/jquery.easing.js",
        "vendors/animo.js/animo.js",
        "vendors/slimScroll/jquery.slimscroll.min.js",
        "vendors/screenfull/dist/screenfull.js",
        "vendors/jquery-localize-i18n/dist/jquery.localize.js",
        "vendors/sparkline/index.js",
        "vendors/jquery-classyloader/js/jquery.classyloader.min.js",
        "vendors/moment/min/moment-with-locales.min.js",
        "vendors/Flot/jquery.flot.js",
        "vendors/flot.tooltip/jquery.flot.tooltip.min.js",
        "vendors/Flot/jquery.flot.resize.js",
        "vendors/Flot/jquery.flot.pie.js",
        "vendors/Flot/jquery.flot.time.js",
        "vendors/Flot/jquery.flot.categories.js",
        "vendors/flot-spline/jquery.flot.spline.min.js",

    ],"public/js/admin/vendors.js");

    mix.scripts([
        'admin/app.js',
        'admin/flot.js',
    ], 'public/js/admin/app.js');

    /*mix.styles([
     'vendors/fontawesome/font-awesome.min.css',
     'vendors/simple-line-icons/simple-line-icons.css',
     'vendors/animate.css/animate.min.css',
     'vendors/whirl/whirl.css',
     'vendors/weather-icons/weather-icons.min.css'
     ], 'public/css/admin/vendors.css');
     */

    /*
     mix.webpack('admin/install.js', 'public/js/admin/install.js');
     mix.scripts([
     "vendors/modernizr/modernizr.custom.js",
     "vendors/matchMedia/matchMedia.js",
     "vendors/bootstrap/dist/js/bootstrap.js",
     "vendors/jQuery-Storage-API/jquery.storageapi.js",
     "vendors/jquery.easing/js/jquery.easing.js",
     "vendors/animo.js/animo.js",
     "vendors/slimScroll/jquery.slimscroll.min.js",
     "vendors/screenfull/dist/screenfull.js",
     "vendors/jquery-localize-i18n/dist/jquery.localize.js",
     "vendors/sparkline/index.js",
     "vendors/Flot/jquery.flot.js",
     "vendors/flot.tooltip/jquery.flot.tooltip.min.js",
     "vendors/Flot/jquery.flot.resize.js",
     "vendors/Flot/jquery.flot.pie.js",
     "vendors/Flot/jquery.flot.time.js",
     "vendors/Flot/jquery.flot.categories.js",
     "vendors/flot-spline/jquery.flot.spline.min.js",
     "vendors/jquery-classyloader/js/jquery.classyloader.min.js",
     "vendors/moment/min/moment-with-locales.min.js"
     ], 'public/js/admin/vendors.js');
     /**/
    /*
     mix.scripts([
     'admin/app.js',
     'admin/flot.js',
     ], 'public/js/admin/app.js');
     mix.webpack('admin/users/app.js', 'public/js/admin/users/app.js');*/

    /*End admin*/


    //mix.copy("vendor/bower_components/jquery-ui/themes/smoothness/images", "public/images");

    mix.sass("app.scss", "resources/assets/css/app.css");
    mix.styles([
        "app.css",
        "bootstrap-datetimepicker.css",
        "vendors/jqueryUi/jquery-ui.min.css",
        "vendors/jqueryUi/theme.css",
        "vendors/dropzone/dropzone.css"
    ], 'public/css/app.css');
    mix.webpack('app.js');
    mix.scripts([
        "libs/jquery-ui.js",
        "libs/handlebars.min.js",
        "vendors/select2/select2.min.js",
        "vendors/dropzone/dropzone.js",
    ], 'public/js/vendor/all.js');

    mix.webpack("posts/app.js", "public/js/posts/app.min.js");
    mix.webpack("group/show.js", "public/js/group/show.js");
    mix.webpack("profile/profile.js", "resources/assets/js/profile/dist/app.js");
    mix.webpack("profile/image.js", "resources/assets/js/profile/image/app.js");
    mix.webpack("profile/announcement.js", "resources/assets/js/profile/announcement/app.js");
    mix.webpack("myBook/app.js", "public/js/myBook/app.min.js");

    mix.scripts([
        "profile/dist/app.js",
        "profile/image/app.js",
        "profile/announcement/app.js"
    ], 'public/js/profile/app.min.js');

    mix.webpack('organisations/groups.js', 'resources/assets/js/organisations/dist');
    mix.webpack('organisations/image.js', 'resources/assets/js/organisations/image/app.js');
    mix.webpack('organisations/announcement.js', 'resources/assets/js/organisations/announcement/app.js');
    mix.webpack('organisations/posts.js', 'resources/assets/js/organisations/post/app.js');
    mix.webpack('organisations/members.js', 'resources/assets/js/organisations/member/app.js');
    mix.scripts([
        'organisations/dist/groups.js',
        'organisations/image/app.js',
        'organisations/announcement/app.js',
        'organisations/post/app.js',
        'organisations/member/app.js',
    ], "public/js/group/app.min.js");

    mix.version([
        'css/app.css',
        "js/app.js",
        "js/vendor/all.js",
        'js/profile/app.min.js',
        'js/group/app.min.js',
        'js/myBook/app.min.js',
        "js/group/show.js",
        "js/admin/app.js",
        "js/admin/install.js",
        "js/admin/users/app.js",
        "js/admin/vendors.js"
    ]);
});