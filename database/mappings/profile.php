<?php

use Sleimanx2\Plastic\Map\Blueprint;
use Sleimanx2\Plastic\Mappings\Mapping;

class Profile extends Mapping
{
    /**
     * Full name of the model that should be mapped
     *
     * @var string
     */
    protected $model = Profile::class;

    /**
     * Run the mapping.
     *
     * @return void
     */
    public function map()
    {
        Map::create($this->getModelType(),function(Blueprint $map){
            $map->boolean('verified')->store('true')->index('analyzed');
            $map->completion('suggestion', ['analyzer' => 'simple', 'search_analyzer' => 'simple']);
        });
    }
}
