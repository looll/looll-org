<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('profiles', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->enum('gender', ["male", "female"]);
            $table->date('birthday')->nullable();
            $table->text('description')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('address')->nullable();;
            $table->string('zip')->nullable();
            $table->string('city')->nullable();
            $table->unsignedBigInteger('country_id')->unsigned();;
            $table->string('position')->nullable();
            $table->string('workplace')->nullable();
            $table->unsignedBigInteger('marital_status_id');
            $table->unsignedBigInteger('user_id');
            $table->timestamp('published_at')->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('profiles');
	}

}
