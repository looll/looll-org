<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companies', function(Blueprint $table) {
            $table->bigIncrements('id');
			$table->string('name');
			$table->string('tin');
            $table->string('email');
            $table->string('address');
            $table->string('city');
            $table->string('zip');
            //$table->string('phone_number');
            $table->string('contact');
            $table->unsignedBigInteger('country_id');
            $table->unsignedBigInteger('user_id');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('companies');
	}

}
