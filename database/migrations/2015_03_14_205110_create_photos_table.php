<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('photos', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->morphs('photoable');
            $table->string('filename');
            $table->boolean('primary_photo');
            $table->string('src');
            $table->string('path');
            $table->string("thumbnail_src");
            $table->string("thumbnail_path");
            $table->softDeletes();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('photos');
	}

}
