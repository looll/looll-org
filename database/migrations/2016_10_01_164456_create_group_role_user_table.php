<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupRoleUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_role_user', function (Blueprint $table) {
            $table->unsignedBigInteger('group_id')->unsigned();
            $table->unsignedBigInteger('role_id')->unsigned();
            $table->unsignedBigInteger('user_id')->unsigned();
            $table->boolean('accepted');
            $table->timestamps();

            /*$table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('role_id')
                ->references('id')->on('roles')
                ->onDelete('cascade');
            $table->foreign('group_id')
                ->references('id')->on('groups')
                ->onDelete('cascade');*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('group_role_user', function (Blueprint $table) {
            $table->dropForeign(['user_id','role_id','group_id']);
        });
        Schema::drop('group_role_user');
    }
}
