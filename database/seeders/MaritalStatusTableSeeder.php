<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MaritalStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $maritalStatus = new \App\MaritalStatus(["name"=>"No info"]);
        $maritalStatus->save();

        $maritalStatus = new \App\MaritalStatus(["name"=>"Single"]);
        $maritalStatus->save();

        $maritalStatus = new \App\MaritalStatus(["name"=>"In Relationship"]);
        $maritalStatus->save();

        $maritalStatus = new \App\MaritalStatus(["name"=>"Engaged"]);
        $maritalStatus->save();

        $maritalStatus = new \App\MaritalStatus(["name"=>"Complicated"]);
        $maritalStatus->save();

        $maritalStatus = new \App\MaritalStatus(["name"=>"Married"]);
        $maritalStatus->save();

        $maritalStatus = new \App\MaritalStatus(["name"=>"Divorced"]);
        $maritalStatus->save();

        $maritalStatus = new \App\MaritalStatus(["name"=>"Windowed"]);
        $maritalStatus->save();


    }
}
