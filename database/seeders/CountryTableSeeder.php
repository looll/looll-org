<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder {

    public function run()
    {
        factory(\App\Country::class,100)->create();
        //TestDummy::times(20)->create('App\Country');
    }

}