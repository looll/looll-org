<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::with([])->where("email","=", "lofturms@gmail.com")->first();
        $user->assignRole(["SuperAdmin"]);
        /*$role = \App\Role::findByName("Owner");
        $role->givePermissionTo(["create.group", "edit.group", "delete.group"]);*/
    }
}
