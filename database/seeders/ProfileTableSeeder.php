<?php
namespace Database\Seeders;

use App\Profile;
use Illuminate\Database\Seeder;

class ProfileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = \App\Role::findByName('Person');
        $role2 = \App\Role::findByName('SuperAdmin');
        //$user = \App\User::find(1);
        $count = 0;
        factory(Profile::class, 10)->create()->each(function ($profile) use($count, $role, $role2)
        {
            $count = $count + 1;

            $user = $profile->user()->first();
            $user->assignRole($role);

            if($count === 5){
                $user->assignRole($role2);
            }

            $user->save();
        });
    }
}
