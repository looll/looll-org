<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Profile;

class Profile2TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = \App\Role::findByName('Person');
        $role2 = \App\Role::findByName('SuperAdmin');
        //$user = \App\User::find(1);
        $count = 0;
        factory(Profile::class, 100)->create()->each(function ($profile) use($count, $role, $role2)
        {
            $count++;

            $user = $profile->user()->first();
            $user->assignRole($role);

            if($count === 5){
                $user->assignRole($role2);
            }

            $user->save();
        });
    }
}
