<?php
namespace Database\Factories;

/** @var Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(\App\Profile::class, function (Faker $faker) {
    $faker->locale = "is_IS";
    return [
        "name" => $faker->name,
        "email" => $faker->unique()->safeEmail,
        "birthday" => $faker->date($format = 'Y-m-d', $max = 'now'),
        'address'=>$faker->address,
        'city'=>$faker->city,
        'zip' => $faker->postcode,
        'country_id' => 1,
        'workplace'=>$faker->company,
        'description' => $faker->paragraph,
        'gender'=> "male",
        'marital_status_id' => 1,
        'position' => $faker->word,
        'user_id' => factory(\App\User::class)->create()->id,
    ];
});
