<?php
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Company::class, function ($faker)
{
    return [
        "iso" => $faker->iso,
        'name' => $faker->company
    ];

});

$factory->define(\App\MyBook::class,function(\Faker\Generator $faker)
{
    return [
        'name'=>$faker->word,
        'user_id'=>factory(\App\User::class)->create()->id
    ];
});

$factory->define(\App\Category::class, function(\Faker\Generator $faker)
{
    return [
        'name' => $faker->word,
        'my_book_id' => factory(\App\MyBook::class)->create()->id
    ];
});