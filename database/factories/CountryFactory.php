<?php
namespace Database\Factories;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Country::class, function (Faker $faker) {
    $faker->locale = "is_IS";
    return [
        "iso" => $faker->countryCode,
        "name" => $faker->country
    ];
});
