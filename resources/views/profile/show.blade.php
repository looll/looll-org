<x-layout>
    <div class="min-h-75 overflow-hidden pt-5 pb-5" style="background-color: #ffaa00;">
        <div class="container" style="width: 75%;">
            <div class="row row-eq-height">
                <div class="col-md-3">
                    <div class="card mb-2">
                        <img src="{{ $profile->photos->where('primary_photo')->first()->src ?? "/img/no-image.jpg" }}" class="card-img-top" alt="...">
                    </div>
                    @if(\Illuminate\Support\Facades\Auth::check() && $currentUser->myBook != null && $currentUser->has("myBook") && $currentUser->id !== $user->id && !$currentUser->hasPersonInMyBook($user->profile))
                        <form action="{{ route('myBooks.profiles.store', ['myBook'=>$currentUser->myBook->first()->id ?? 0]) }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="profile_id" value="{{ $profile->id }}" />
                            <button class="btn btn-primary">{{__("Add to my book")}}</button>
                        </form>
                    @endif
                </div>
                <div class="col-md-6">
                    <div class="card h-100">
                        <div class="card-body h-100">
                            <div class="d-flex justify-content-between">
                                <h1 class="font-weight-bolder">{{ $profile->name ?? ""}}</h1>
                                @if($currentUser->id === $user->id)
                                    <div>
                                        <a class="btn btn-primary" href="{{ route('profiles.edit', ["profile"=>$user->username])  }}">{{ __("Edit") }}</a>
                                    </div>
                                @endif
                            </div>
                            <div class="row mt-4">
                                <div class="col-sm-4">
                                    <span class="title address"><strong>{{ __("Address") }}</strong></span>
                                </div>
                                <div class="col-sm-8 text-right">
                                    {{$profile->address ?? ""}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <span class="title address"><strong>{{ __("City") }}</strong></span>
                                </div>
                                <div class="col-sm-8 text-right">
                                    {{ $profile->zip  }} {{$profile->city ?? ""}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <span class="title address"><strong>{{ __("Country") }}</strong></span>
                                </div>
                                <div class="col-sm-8 text-right">
                                    {{ $profile->country->name ?? "" }}
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-sm-4">
                                    <span class="title address"><strong>{{ __("Email") }}</strong></span>
                                </div>
                                <div class="col-sm-8 text-right">
                                    {{$profile->email}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <span class="title address"><strong>Mobile</strong></span>
                                </div>
                                <div class="col-sm-8 text-right">
                                    8473619
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-sm-4">
                                    <span class="title address"><strong>{{ __("Relationships") }}</strong></span>
                                </div>
                                <div class="col-sm-8 text-right">
                                    {{ $profile->marital_status->name ?? "" }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <span class="title address"><strong>{{ __("Birthday") }}</strong></span>
                                </div>
                                <div class="col-sm-8 text-right">
                                    {{ $birthday ?? "" }}
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-sm-4">
                                    <span class="title address"><strong>{{ __("Company") }}</strong></span>
                                </div>
                                <div class="col-sm-8 text-right">
                                    {{ $profile->workplace }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <span class="title address"><strong>{{ __("Position") }}</strong></span>
                                </div>
                                <div class="col-sm-8 text-right">
                                    {{ $profile->position }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <span class="title address"><strong>{{ __("Business Phone") }}</strong></span>
                                </div>
                                <div class="col-sm-8 text-right">
                                    <a href="tel:{{ $profile->phoneNumbers->where("phone_type", "business")->first()->phone_number ?? "" }}">{{ $profile->phoneNumbers->where("phone_type", "business")->first()->phone_number ?? "" }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card card-body h-100">
                        <p class="lead">{{ $profile->announcements->first()->body ?? "" }}</p>
                    </div>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="card card-body">
                        <p class="lead">{{ $profile->description }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-slot name="title">
        {{ $profile->name }}
    </x-slot>
    <x-slot name="metaDescription">
        {{ $profile->name }} Contact information
    </x-slot>
    <x-slot name="scriptsFooter">
    </x-slot>
</x-layout>