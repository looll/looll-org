{{--
@extends('layouts/master')
@section('content')
	<div class="row">
		<div class="col-sm-3 box">
			@include("photo.partials.upload", ['groupId'=>0, 'profileId'=>$profile->id])
			<div id="result">
				@if(count($photos))
					@foreach($photos as $photo)
						<div class="imageholder" id="imageholder{{{$photo->id}}}">
							<figure>
								<img src="{{$photo->src}}" alt="" class="image" data-image-id="{{$photo->id}}"/>
								<span class="fa fa-times-circle remove-images" data-image-id="{{$photo->id}}"></span>
								<span class="fa @if($image->image_primary) fa-star @else fa-star-o @endif primary-image" data-profile-id="{{$profile->id}}" data-photo-id="{{$photo->id}}" data-primary="{{$image['image_primary']}}"></span>
							</figure>
						</div>
					@endforeach
				@endif
			</div>
		</div>
		{{
			Form::model($profile, array('route' => array('users.profiles.update', 'user' => $username, 'profile'=>$profile->id), 'data-remote', 'method' => 'PUT'))
		}}
		<div class="col-sm-5 box">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">@lang('looll.Announcements')</h4>
				</div>
				<div class="panel-body">
					@if(count($announcementsByOrganisations))
						<div class="list-group">
							@foreach($announcementsByOrganisations as $organisation)
								@if(!empty($organisation->announcement) || !empty($organisation->private_announcement) )
									<div class="list-group-item">
										<h4 class="list-group-item-heading">{{ $organisation->name }} <small>{{$organisation->updated_at->diffForHumans(); $organisation->updated_at }}</small></h4>
										@if(!empty($organisation->announcement))
											<div class="list-group-item-text">{{ $organisation->announcement }}</div>
										@endif

										@if(!empty($organisation->private_announcement))
											<div class="list-group-item-text box">{{ $organisation->private_announcement }}</div>
										@endif
									</div>
								@endif
							@endforeach
						</div>
					@endif
					{{ $announcementsByOrganisations->links() }}
				</div>
			</div>
		</div>
		<div class="col-sm-4 box">
			<div class="panel-group" id="accordion">
				@include('includes.profile.form')
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#groups">
								@lang('looll.Organisations')
							</a>
						</h4>
					</div>
					<div id="groups" class="panel-collapse collapse">
						<div class="panel-body">
							@if(isset($organisations))
								<table class="table table-condensed">
									<thead>
									<tr>
										<th>@lang('looll.Name')</th>
										<th>@lang('Action')</th>
									</tr>
									</thead>
									<tbody>
									@foreach($organisations as $organisation)
										@if($organisation['pivot']['role_id'] == 3)
											<tr id="{{$organisation['id']}}">
												<td>
													<a href="/organisation/{{{$organisation['id']}}}">{{$organisation['name']}}</a></td>
												<td>
												<td>
													{{
                                                        link_to_route('organisations.users.destroy', Lang::get('looll.LeaveGroup'), [$organisation['id'], Auth::id()], ['data-organisation-id'=>$organisation['id'] ,'class' => 'btn btn-warning leaveGroup'])

                                                    }}
												</td>
											</tr>
										@endif

										@if($organisation['pivot']['role_id'] == 1)
											<tr id="{{$organisation['id']}}">
												<td>
													<a href="/organisation/{{{$organisation['id']}}}">{{$organisation['name']}}</a></td>
												<td>
												<td>
													{{
                                                        link_to_route('organisation.edit', Lang::get('looll.Edit'), $parameters = array($organisation['id']), $attributes = array('class'=>'btn btn-primary'))
                                                    }}

													{{
                                                        link_to_route('organisation.destroy', Lang::get('looll.Delete'), $parameters = array($organisation['id']), $attributes = array('data-organisation-id'=>$organisation['id'], 'data-user-id'=>Auth::id(), 'class'=>'btn btn-danger deleteGroup'))
                                                    }}
												</td>
											</tr>
										@endif
									@endforeach
									</tbody>
								</table>
							@endif
							{{
								link_to_route('organisation.create', Lang::get('looll.CreateOrganisation'),[],['class' => 'btn btn-primary'])
							}}
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<input type="submit" class="btn btn-primary" name="save" id="save" data-profile-id="{{{$profile->id}}}" value="@lang('looll.Save')" />
				<input type="submit" data-profile-id="{{{$profile->id}}}" class="btn btn-success hidden" value="Your profile was saved" id="success" >
			</div>
		</div>
		{{
			Form::close()
		}}
	</div>

@stop
@section("AngularController")
	ng-controller="ProfilesController"
@stop
@section('javascript')
	<script type="text/javascript" src="/js/modernizr.custom.js"></script>
	<script src="/js/profile/dist/main.bundle.min.js?v=2.3"></script>
	<script type="text/javascript">
		$('textarea').jqte();
	</script>
@stop
--}}