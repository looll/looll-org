<x-layout>
    <div class="min-h-75 overflow-hidden pt-5 pb-5" id="app" style="background-color: #ffaa00;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>{{ __("Edit Profile") }}</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <ul class="nav nav-tabs card-header-tabs">
                                <li class="nav-item">
                                    <a href="#home"
                                       class="nav-link active"
                                       aria-selected="true"
                                       aria-controls="home"
                                       role="tab"
                                       data-toggle="tab">{{ __("Profile") }}</a></li>
                                <li class="nav-item">
                                    <a class="nav-link"
                                       href="#image"
                                       aria-selected="false"
                                       aria-controls="image"
                                       role="tab"
                                       data-toggle="tab">{{ __("Images") }}</a></li>
                                <li class="nav-item">
                                    <a href="#announcement" class="nav-link" aria-selected="false" aria-controls="announcement" role="tab" data-toggle="tab">Announcement</a></li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane active"
                                     role="tabpanel"
                                     aria-labelledby="home-tab"
                                     id="home"
                                     data-profile-url="{{ route("api.profiles.edit", ['profile'=>$profile->id]) }}">
                                    <form action="{{ route("profiles.update", ["profile"=>$profile->id]) }}"
                                          method="post">
                                        @method("put")
                                        @csrf
                                        @include('profile.partials.form')
                                        <div class="form-group">
                                            <button class="btn btn-secondary"
                                                    type="button"
                                                    name="published"
                                                    v-on:click.prevent="saveProfile('{{ route('api.profiles.update', ['profile'=>$profile->id]) }}', 0)"
                                                    v-cloak
                                                    v-bind:class="{'btn-success':saved}"
                                            >{{ __("Save") }}</button>
                                            <button
                                                    class="btn btn-primary"
                                                    type="button"
                                                    name="published"
                                                    v-on:click.prevent="saveProfile('{{ route('api.profiles.update', ['profile'=>$profile->id]) }}', 1)"
                                                    v-bind:class="{'btn-success':publishedProfile}"
                                            >{{ _("Published profile") }}</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane"
                                     role="tabpanel"
                                     aria-labelledby="image-tab" id="image" >
                                    @include("photo.partials.upload", ['groupId'=>0, 'profileId'=>$profile->id])
                                </div>
                                <div class="tab-pane" role="tabpanel" aria-labelledby="announcement-tab" id="announcement"
                                     data-profile-id="{{$profile->id}}"
                                     data-announcement-url="{{ route('api.profiles.announcements.index',['profile'=>$profile->id]) }}">
                                    <form action="{{ $announcementUrl }}" method="post">
                                        {{ csrf_field() }}
                                        @if( $announcementID > 0)
                                            {{ method_field("PUT") }}
                                        @endif
                                        <div class="form-group">
                                        <textarea name="announcement[body]"
                                                  id="announcement"
                                                  class="form-control"
                                                  v-model="announcement.body"
                                        ></textarea>
                                            <input type="hidden"
                                                   name="announcement[id]"
                                                   v-model="announcement.id"
                                            />
                                            <input type="hidden"
                                                   name="announcement[public]"
                                                   v-model="announcement.public"
                                            />
                                        </div>
                                        <div class="form-group">
                                            <button type="submit"
                                                    v-on:click.prevent="saveAnnouncement('{{ $announcementApiUrl }}')"
                                                    v-bind:class="['btn', saved ? success : primary]"
                                                    name="save">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-slot name="scriptsFooter">
        <script src="{{ mix("js/profile/app.js") }}" defer></script>
    </x-slot>
    <x-slot name="title">Edit Profile</x-slot>
    <x-slot name="metaDescription">Edit Profile</x-slot>
</x-layout>