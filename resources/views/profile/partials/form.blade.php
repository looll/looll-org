<div class="form-row">
    <div class="form-group col-md-6">
        <label for="name">{{ Lang::get("looll.Name") }}</label>
        <input type="text"
               name="name"
               class="form-control"
               id="name"
               v-model="profile.name"
        />
    </div>
    <div class="form-group col-md-6">
        <label for="email">{{ Lang::get("looll.Email") }}</label>
        <input type="email"
               name="email"
               class="form-control"
               id="email"
               v-model="profile.email"
        />
        <span class="label label-danger message-email error @if($errors->has('email')) show @else hidden @endif ">{{ $errors->first('email') }}</span>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-4">
        <label for="birthday">{{ Lang::get("looll.Profiles.Birthday") }}</label>
        <input type="date"
               name="birthday"
               class="form-control"
               id="birthday"
               v-model="profile.birthday"
        />
        <span class="label label-danger message-birthday error @if($errors->has('birthday')) show @else hidden @endif ">{{ $errors->first('birthday') }}</span>
    </div>
    <div class="form-group col-md-4">
        <label for="gender">{{ Lang::get("looll.Profiles.Gender") }}</label>
        <select name="gender"
                id="gender"
                class="form-control"
                v-model="profile.gender"
        >
            <option value="male">{{ Lang::get("looll.Profiles.Male") }}</option>
            <option value="female">{{ Lang::get("looll.Profiles.Female") }}</option>
        </select>
    </div>
    <div class="form-group col-md-4">
        <label for="marital_status_id">{{ __("Relationships") }}</label>
        <select name="marital_status_id"
                id="marital_status_id"
                class="form-control"
                v-model="profile.marital_status_id"
        >
            @foreach($marital_status_list as $marital_status)
                <option value="{{ $marital_status->id }}">{{ $marital_status->name }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <label for="address">{{ Lang::get('looll.Address') }}</label>
    <input type="text"
           name="address"
           class="form-control"
           v-model="profile.address"
    />
    <span class="label label-danger message-address error @if($errors->has('address')) show @else hidden @endif ">{{ $errors->first('address') }}</span>
</div>
<div class="form-row">
    <div class="form-group col-md-2">
        <label for="zip">{{ Lang::get('looll.Zip') }}</label>
        <input type="text"
               name="zip"
               class="form-control"
               v-model="profile.zip"
        />
    </div>
    <div class="form-group col-md-4">
        <label for="city">{{ Lang::get('looll.City') }}</label>
        <input type="text"
               name="city"
               id="city"
               class="form-control"
               v-model="profile.city"
        />
    </div>
    <div class="form-group col-6">
        <label for="country">{{  Lang::get('looll.Country') }}</label>
        <select name="country_id"
                class="form-control"
                id="country_id"
                v-model="profile.country_id"
        >
            @foreach($countries as $country)
                <option
                        value="{{ $country->id }}"
                >{{ $country->name }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <label for="phone">Phone number:</label>
    <input type="tel"
           id="phone"
           class="form-control"
           name="phone_number"
           v-model="profile.phone_number"
           pattern="[0-9]{7}" />
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="workplace">{{ Lang::get('looll.Profiles.WorkPlace') }}</label>
        <input type="text"
               name="workplace"
               class="form-control"
               id="workplace"
               v-model="profile.workplace"
        />
    </div>
    <div class="form-group col-md-6">
        <label for="position">{{ Lang::get('looll.Profiles.Position') }}</label>
        <input type="text"
               name="position"
               class="form-control"
               v-model="profile.position"
        />
    </div>
</div>
<div class="form-group">
    <label for="businessPhone">Business phone number:</label>
    <input type="tel"
           id="businessPhone"
           v-model="profile.business_phone_number"
           class="form-control"
           name="business_phone_number">
</div>
<div class="form-group">
    <label for="description">{{ Lang::get("looll.Description")  }}</label>
    <textarea name="description"
              class="form-control"
              id="description"
              v-model="profile.description"
    >{{ old('description') }}</textarea>
</div>