<x-layout>
    <div class="min-h-75 overflow-hidden pt-5 pb-5" style="background-color: #ffaa00;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <form action="{{ route('profiles.store') }}" method="post">
                        {{ csrf_field() }}
                        <div class="card">
                            <div class="card-header">
                                <h2 class="card-title">Create a profile</h2>
                            </div>
                            <div class="card-body">
                                @include('profile.partials.form')
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <x-slot name="title">
       Create profile
    </x-slot>
    <x-slot name="metaDescription">
        Create a profile
    </x-slot>
</x-layout>