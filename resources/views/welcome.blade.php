<x-layout>
    <div class="container-fluid d-flex flex-column h-50" style="min-height: 40vh">
        <div class="row justify-content-center align-items-center bg- text-dark h-100"
             style="flex: 2; background-color: #ffaa00;">
            <div class="col-sm-6 pt-4 text-white">
                <h1 class="font-weight-bolder text-center mt-5">{{ __("Welcome to Looll") }}</h1>
                <h3 class="text-center">{{ __("Keep track of your network in one place.")}}</h3>
                <p class="text-center mb-3"> {{__("Friends, relatives, co-workers, and NGOs, all in one place") }}</p>
                <hr />
                <form action="{{ route('searches.index') }}" method="get">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="search" name="q" placeholder="{{ Lang::get('looll.SearchWords') }}" class="form-control" />
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-outline-secondary text-white">{{ Lang::get('looll.Search') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
                <p class="font-weight-bolder text-center">{{ __("Sign up for Looll today and start managing your network.") }}</p>
                @guest()
                <div class="d-flex justify-content-center">
                    <a class="btn btn-primary btn-lg mr-2" href="/register">{{ __("Register") }}</a>
                    <br />
                    <a class="btn btn-secondary btn-lg" href="/login">{{ __("Sign in") }}</a>
                </div>
                @endguest
            </div>
        </div>
    </div>
    <x-slot name="title">
        Home
    </x-slot>
    <x-slot name="metaDescription">
        Description
    </x-slot>
    <x-slot name="scriptsFooter">
    </x-slot>
</x-layout>
