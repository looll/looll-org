<div class="row">
    <div class="col-sm-12"
         id="photos"
         data-photos-url="{{ $photosIndexUrl }}"
    >
        <div class="card">
            <div class="card-header">
                <h4 class="panel-title">Photos <small style="padding-left:15px;">Drop photos here to upload max 3 MB</small></h4>
            </div>
            <div class="card-body">
                <form action="{{ $photosStoreUrl }}"
                      method="post"
                      id="looll-dropzone"
                      class="dropzone"
                >
                    @csrf
                    <div class="dz-default dz-message"><span>Drop photos here to upload max 3 MB</span></div>
                </form>
            </div>
        </div>
    </div>
</div>
