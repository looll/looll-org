<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} @yield("title") </title>

    <!-- Scripts -->
    <script data-ad-client="ca-pub-6456635748990138" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    @yield('scripts.head')

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.css')}}" />


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="d-flex flex-column min-vh-100">
    <div class="bg-light text-dark d-flex flex-column min-vh-100" style="flex: 1">
        <div class="collapse" id="navbarSearch">
            <div class="bg-dark p-4">
                <form action="" accept-charset="UTF-8" method="get">
                    <div class="input-group">
                        <input type="text"
                               name="search"
                               id="search"
                               placeholder="Search accounts, contracts and transactions" class="form-control">
                        <span class="input-group-append">
                            <input type="submit" name="commit" value="Search" class="btn " data-disable-with="Search">
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm" id="app">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item {{ set_active('about') }}">
                            <a class="nav-link" href="/about">@lang('looll.About')</a>
                        </li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ __("Language") }}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)

                                    <a class="dropdown-item"
                                       rel="alternate"
                                       hreflang="{{ $localeCode }}"
                                       href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                        {{ $properties['native'] }}
                                    </a>

                                @endforeach
                            </div>
                        </li>

                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('posts.index') }}">
                                    <span class="badge badge-info" @if($unReadPost == 0) hidden @endif>{{ $unReadPost ?? 0 }}
                                     <span class="fa fa-envelope"></span>
                                    </span>
                                    <span class="fa fa-envelope-o" @if($unReadPost > 0) hidden @endif></span>

                                </a>
                            </li>

                            @if(Auth::user()->hasRole('Person'))
                                <li class="nav-item">
                                    <a class="nav-link" href="@if(Auth::user()->hasMyBook() )
                                    {{ route('myBooks.show', ['myBook'=>$myBookId]) }}
                                    @else
                                    {{ route('myBooks.create') }}
                                    @endif"
                                    >{{ Lang::get('looll.MyBook') }}</a>
                                </li>
                            @endif
                            @if(Auth::user()->hasRole('Person'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('profiles.show', ['profile'=>Auth::user()->username]) }}">{{ Lang::get('looll.MySite') }}</a>
                                </li>
                            @endif
                            @if(Auth::user()->hasRole('Company') && Auth::user()->company()->exists())
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('companies.show', ['company'=>Auth::user()->username]) }}">{{ Lang::get('looll.MyCompany') }}</a>
                                </li>
                            @endif
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    @if(Auth::user()->hasRole(['Super Admin']))
                                        <a class="dropdown-item" href="{{ route('admin.dashboard') }}">Admin site</a>
                                    @endif
                                    <a class="dropdown-item" href="{{ route('users.edit', ['user'=>Auth::user()->id ]) }}">{{  Lang::get('looll.Settings') }}</a>

                                    @if(Auth::user()->hasRole('Person'))
                                        <a class="dropdown-item" href="{{ route('profiles.edit', ['profile'=>Auth::user()->username]) }}">{{  Lang::get('looll.EditProfile') }}</a>
                                        <a class="dropdown-item" href="{{ route('groups.index') }}">Groups</a>
                                    @endif
                                    @if(Auth::user()->hasRole('Company'))
                                        <a class="dropdown-item" href="{{ route('companies.edit', ['company'=>Auth::user()->company->id]) }}">{{ __("Edit Company")  }}</a>
                                    @endif
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse" data-target="#navbarSearch" aria-controls="navbarSearch" aria-expanded="false" aria-label="Search" ><span class="fa fa-search"></span></a>
                        </li>
                    </ul>
                </div>

            </div>
        </nav>

        <main class="py-0" style="flex: 1">
            @yield('content')
        </main>
        <script src="{{ mix('js/app.js') }}"></script>
        @yield("scripts.bottom")
        <script>

            var code = "";
            if(document.location.hostname === "looll.is")
            {
                code = "UA-53044404-1";
            }
            else
            {
                code = "UA-53044404-2"
            }

            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', code, 'auto');
            ga('send', 'pageview');


        </script>
    </div>
</body>
</html>
