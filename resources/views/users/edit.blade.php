<x-layout>
    <div class="min-h-75 overflow-hidden @if (!session('resent')) pt-5 @endif pb-5" style="background-color: #ffaa00;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-title text-center">{{ __("Settings") }}</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-offset-3 col-sm-6">
                    @if($value)
                        <div class="alert alert-success">{{ $value }}</div>
                    @endif
                    <form action="{{ route('users.update', ['user'=>$id]) }}" method="POST">
                        {{ method_field('put') }}
                        {{ csrf_field() }}
                        <div class="card">
                            <div class="card-header">
                                <h2 class="card-title">{{ __("Update account information") }}</h2>
                            </div>
                            <div class="card-body">
                                <div class="mb-3">
                                    <label for="email" class="form-label">{{ __("Email") }}</label>
                                    <input type="email" name="email"
                                           class="form-control @error("email") is-invalid" @enderror
                                           id="email" aria-describedby="emailHelp" value="{{ \Illuminate\Support\Facades\Auth::user()->email ?? old("email") }}" />
                                    @error("email")
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="current_password" class="form-label">{{ __("Current password") }}</label>
                                    <input type="password"
                                           id="current_password"
                                           name="current_password"
                                           class="form-control @error("current_password") is-invalid @enderror"
                                           aria-describedby="current_password" />
                                    @error("current_password")
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="password" class="form-label">{{ __("Password") }}</label>
                                    <input type="password"
                                           name="password"
                                           value="{{ old('password') }}"
                                           class="form-control @error("password") is-invalid @enderror" aria-describedby="password"/>
                                    @error("password")
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="password_confirmation" class="form-label">{{ __("Password Confirmation") }}</label>
                                    <input type="password" name="password_confirmation" id="password_confirmation"
                                           value="{{ old('password_confirmation') }}"
                                           class="form-control @error("password_confirmation") is-invalid @enderror" />
                                    @error("password_confirmation")
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" name="update" class="btn btn-primary">{{ __("Save") }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <x-slot name="title">
        {{ __("Settings") }}
    </x-slot>
    <x-slot name="metaDescription">
        {{ __("Settings") }}
    </x-slot>
    <x-slot name="scriptsFooter">

    </x-slot>
</x-layout>