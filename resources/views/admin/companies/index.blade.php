@extends('admin.layout.app')
@section('header')
    <!-- START Language list-->
    <div class="pull-right">
        <div class="btn-group">
            <button type="button" data-toggle="dropdown" class="btn btn-default">English</button>
            <ul role="menu" class="dropdown-menu dropdown-menu-right animated fadeInUpShort">
                <li><a href="#" data-set-lang="en">English</a>
                </li>
                <li><a href="#" data-set-lang="es">Spanish</a>
                </li>
            </ul>
        </div>
    </div>
    <!-- END Language list    -->
    Companies
    <small data-localize="dashboard.WELCOME"></small>
@stop
@section("content")
    <div class="container-fluid" id="companies">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Companies
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>{{ __("Name") }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->company->name }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
