@extends('admin.layout.app')
@section('header')
    <!-- START Language list-->
    <div class="pull-right">
        <div class="btn-group">
            <button type="button" data-toggle="dropdown" class="btn btn-default">English</button>
            <ul role="menu" class="dropdown-menu dropdown-menu-right animated fadeInUpShort">
                <li><a href="#" data-set-lang="en">English</a>
                </li>
                <li><a href="#" data-set-lang="es">Spanish</a>
                </li>
            </ul>
        </div>
    </div>
    <!-- END Language list    -->
    Dashboard
    <small data-localize="dashboard.WELCOME"></small>
@stop
@section('content')
    <div class="row">
        <div class="col-lg-3 col-sm-6">
            <!-- START widget-->
            <div class="panel widget bg-primary">
                <div class="row row-table">
                    <div class="col-xs-4 text-center bg-primary-dark pv-lg">
                        <em class="fa fa-user fa-3x"></em>
                    </div>
                    <div class="col-xs-8 pv-lg">
                        <div class="h2 mt0">{{ $users }}</div>
                        <div class="text-uppercase">Users</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6">
            <!-- START widget-->
            <div class="panel widget bg-purple">
                <div class="row row-table">
                    <div class="col-xs-4 text-center bg-purple-dark pv-lg">
                        <em class="fa fa-users fa-3x"></em>
                    </div>
                    <div class="col-xs-8 pv-lg">
                        <div class="h2 mt0"> {{ $groups }}
                        </div>
                        <div class="text-uppercase">Groups</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12">
            <!-- START widget-->
            <div class="panel widget bg-green">
                <div class="row row-table">
                    <div class="col-xs-4 text-center bg-green-dark pv-lg">
                        <em class="fa fa-briefcase fa-3x"></em>
                    </div>
                    <div class="col-xs-8 pv-lg">
                        <div class="h2 mt0">0</div>
                        <div class="text-uppercase">Companies</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12">
            <!-- START widget-->
            <!-- START date widget-->
            <div class="panel widget">
                <div class="row row-table">
                    <div class="col-xs-4 text-center text-info">
                        <!-- Bar chart-->
                        <div data-sparkline=""
                             data-bar-color="#5d9cec"
                             data-height="20"
                             data-bar-width="3"
                             data-bar-spacing="2"
                             data-values="5,9,4,1,3,4,7,5"
                        ></div>
                    </div>
                    <div class="col-xs-8 pv-lg">
                        <h4 class="h2 mt0">10</h4>
                        <div class="text-uppercase">Visitors</div>
                    </div>
                </div>
            </div>
            <!-- END date widget    -->
        </div>
    </div>
    <div class="row">
        <!-- START dashboard main content-->
        <div class="col-lg-12">
            <!-- START chart-->
            <div class="row">
                <div class="col-lg-12">
                    <!-- START widget-->
                    <div id="panelChart9" class="panel panel-default panel-demo">
                        <div class="panel-heading">
                            <a href="#"
                               data-tool="panel-refresh"
                               data-toggle="tooltip"
                               title="Refresh Panel" class="pull-right">
                                <em class="fa fa-refresh"></em>
                            </a>
                            <a href="#" data-tool="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
                                <em class="fa fa-minus"></em>
                            </a>
                            <div class="panel-title">Inbound visitor statistics</div>
                        </div>
                        <div class="panel-body">
                            <div class="chart-spline flot-chart" data-url="{{ route('api.visitors.index', ['api_token'=>Auth::user()->api_token]) }}"></div>
                        </div>
                    </div>
                    <!-- END widget-->
                </div>
            </div>
        </div>
        <!-- END dashboard main content-->
        <!-- START dashboard sidebar-->
        <!-- END dashboard sidebar-->
    </div>
@stop