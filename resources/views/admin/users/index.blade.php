@extends('admin.layout.app')
@section('header')
    <!-- START Language list-->
    <div class="pull-right">
        <div class="btn-group">
            <button type="button" data-toggle="dropdown" class="btn btn-default">English</button>
            <ul role="menu" class="dropdown-menu dropdown-menu-right animated fadeInUpShort">
                <li><a href="#" data-set-lang="en">English</a>
                </li>
                <li><a href="#" data-set-lang="es">Spanish</a>
                </li>
            </ul>
        </div>
    </div>
    <!-- END Language list    -->
    Users
@endsection
@section('content')
    <div class="container-fluid" id="users">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Users
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <td>Registered</td>
                                <td>Verified</td>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr id="user-{{ $user->id }}">
                                    <td>{{ $user->profile->name ?? $user->username }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->created_at->diffForHumans() }}</td>
                                    <td>@if($user->email_verified_at) Yes @else No @endif</td>
                                    <td>
                                        <form class="{{ route('admin.users.destroy', ['user'=>$user->id]) }}">
                                            <button class="btn btn-warning"
                                                    v-on:click.prevent="lockedUser('{{ route('api.users.update', ['user'=>$user->id]) }}')"
                                            ><i class="fa fa-lock"></i></button>
                                            <button class="btn btn-danger"
                                                    v-on:click.prevent="deleteUser('{{ route('api.users.destroy', ['user'=>$user->id]) }}', {{ $user->id }})"
                                            ><i class="fa fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts.footer')
    <script src="{{ asset('js/admin/users/app.js') }}"></script>
@endsection