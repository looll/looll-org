<x-layout>
    <div class="container-fluid d-flex flex-column h-100" style="min-height: 40vh">
        <div class="row justify-content-center align-items-center bg- text-dark h-50"
             style="flex: 2; background-color: #ffaa00;">
            <div class="col-sm-6 pt-4 text-white">
                <h1 class="font-weight-bolder text-center">{{ __("Search Result") }}</h1>
                <p class="text-center">
                    <img src="/img/search-by-algolia-light-background.png"
                         style="height: 20px;" class="img-preview" alt="Algolia" /></p>
                <hr />
                <form action="{{ route('searches.index') }}" method="get">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="search" name="q" placeholder="{{ Lang::get('looll.SearchWords') }}" class="form-control" />
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-outline-secondary">{{ Lang::get('looll.Search') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="container">
            <div class="row mt-3">
                @if(!empty($groups) || !empty($profiles))

                    @if(!empty($profiles))
                    @forelse($profiles as $profile)
                        @include("searches.partials._item", ['group'=>null, 'profile'=>$profile])
                    @empty
                    @endforelse
                    @endif

                    @if(!empty($groups))
                    @forelse($groups as $group)
                        @include("searches.partials._item", ['profile'=>null,'group'=>$group])
                    @empty
                    @endforelse
                        @endif
                @else
                    <div class="col-md-12">
                        <h5 class="text-center text-white">{{ __("Sorry we did not find anything, try again") }}</h5>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <x-slot name="title">{{ __("Search Result") }}</x-slot>
    <x-slot name="metaDescription">{{ __("Search Result") }}</x-slot>
    <x-slot name="scriptsFooter">
        <script>
            if ( ! Modernizr.objectfit )
            {
                if (document.documentMode || /Edge/.test(navigator.userAgent)) {
                    $('.profile__image-container').each(function ()
                    {
                        var $container = $(this),
                            imgUrl = $container.find('img').prop('src');

                        if (imgUrl) {
                            $container
                                .css('backgroundImage', 'url(' + imgUrl + ')')
                                .addClass('compat-object-fit');
                        }
                    });
                }
            }
        </script>
    </x-slot>
</x-layout>