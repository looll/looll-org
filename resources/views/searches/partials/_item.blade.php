<div class="col-sm-6 col-md-2">
    <div class="card">
        @if($profile !== null)
        <img src="{{ $profile->photos->first()->thumbnail_src ?? "/img/no-image.jpg" }}"
             @else
            <img src="{{ $group->photos->first()->thumbnail_src ?? "/img/no-image.jpg" }}"
                 @endif
             class="card-img-top" alt="{{ $profile->name ?? $group->name }}">
        <div class="card-body">
            <h5 class="card-title">{{ $profile->name ?? $group->name }}</h5>
            @if($profile !== null)
                <a href="{{ route('profiles.show', ['profile'=>$profile->user->username]) }}" class="btn btn-primary">{{ __("View Profile") }}</a>
            @else
                <a href="{{ route('groups.show', ['group'=>$group->id]) }}" class="btn btn-primary">{{ __("View Group") }}</a>
            @endif

        </div>
    </div>
</div>