<!DOCTYPE html>
<html class="h-100 w-100">
    <head>
        @include('partials._head')
    </head>
    <body class="h-100 w-90">
        <div class="container-fluid h-100 p-0">
            @include('partials._header')
            @yield('content')
        </div>
        <script src="{{ mix("js/app.js") }}"></script>
        <script src="{{ asset("js/vendor/all.js", env('APP_SECURITY', false)) }}"></script>
        <script src="{{ asset('js/modernizr.js', env('APP_SECURITY', false)) }}"></script>
        @yield('javascript')
        <script>

            var code = "";
            if(document.location.hostname === "looll.is")
            {
                code = "UA-53044404-1";
            }
            else
            {
                code = "UA-53044404-2"
            }
        
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
            ga('create', code, 'auto');
            ga('send', 'pageview');
        
        
        </script>
    </body>
</html>