<ul class="nav flex-column nav-pills">
                    <li class="nav-item">
                        <a class="nav-link @if(Request::segment(3) == "edit")active @endif" href="{{ route('companies.edit', ['company'=>$user->company->id]) }}"
                         >Information</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if(Request::segment(3) == "photos")active @endif" href="{{ route('companies.photos.index', ['company'=>$user->company->id]) }}"
                        >Photos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if(Request::segment(3) == "background")active @endif" href="{{ route('companies.backgrounds.index', ['company'=>$user->username]) }}"
                        >Background photos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if(Request::segment(3) == "hours")active @endif" href="{{ route('companies.hours.index', ['company'=>$company->id]) }}">Opening hours</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if(Request::segment(3) == "announcements")active @endif" href="{{ route('companies.announcements.index', ['company'=>$company->id]) }}">Announcements</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if(Request::segment(3) == "tags")active @endif"
                           href="{{ route('companies.tags.index', ['company'=>$company->id]) }}">{{ __("Tags") }}</a>
                    </li>
    <li class="nav-item">
        <a class="nav-link @if(Request::segment(3) == "messages")active @endif" href="{{ route('companies.messages.index', ['company'=>$company->id]) }}">{{__("Messages")}}</a>
    </li>
</ul>
