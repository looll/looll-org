<div class="card">
    <div class="card-body">
        <div class="form-group">
            <label>{{ __("Day") }}</label>
            <input type="text" placeholder="{{ __("Day") }}"
                   class="form-control" name="day" value="{{ $hour->day ?? old("day") }}" />
        </div>
        <div class="form-group">
            <label>{{__("Open time")}}</label>
            <input type="time" name="open_time" value="{{ $hour->open_time ?? old("open_time") }}" class="form-control" />
        </div>
        <div class="form-group">
            <label>{{__("Close time")}}</label>
            <input type="time" name="close_time" value="{{ $hour->close_time ?? old("close_time") }}" class="form-control" />
        </div>
    </div>
    <div class="card-footer">
        <button class="btn btn-primary">{{ __("Save") }}</button>
    </div>
</div>