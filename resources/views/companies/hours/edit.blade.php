@extends('companies.layout')
@section("layout.content")
    <form action="{{ route('companies.hours.update', ["company"=>$company->id, 'hour'=>$hour->id]) }}" method="post">
        @csrf
        @method('PUT')
        @include("companies.hours.partials._form");
    </form>
@stop