@extends('companies.layout')
@section("layout.content")
    <form action="{{ route('companies.hours.store', ["company"=>$company->id]) }}" method="post">
        @csrf
        @include("companies.hours.partials._form");
    </form>
@stop