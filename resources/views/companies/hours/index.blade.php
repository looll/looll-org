@extends('companies.layout')
@section("layout.content")
    <div class="card">
        <div class="card-body">
            <table class="table">
                <tr>
                    <th>Day</th>
                    <th>Open time</th>
                    <th>Close time</th>
                    <th></th>
                </tr>
                @foreach($hours as $hour)
                <tr>
                    <td>{{ $hour->day }}</td>
                    <td>{{ $hour->open_time }}</td>
                    <td>{{ $hour->close_time }}</td>
                    <td>
                        <form
                                action="{{ route('companies.hours.destroy', ['company'=>$company->id, 'hour'=>$hour->id]) }}" method="POST">
                            @csrf
                            @method("DELETE")
                            <a href="{{ route("companies.hours.edit", ["company"=>$company->id, "hour"=>$hour->id]) }}" class="btn btn-primary">{{__("Edit")}}</a>
                            <button class="btn btn-danger">{{__("Delete")}}</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
        <div class="card-footer">
            <a href="{{ route('companies.hours.create', ["company"=>$company->id]) }}" class="btn btn-primary">Create</a>
        </div>
    </div>
@stop