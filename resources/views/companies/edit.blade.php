@extends('companies.layout')
@section("title")
    <h1>Edit {{ $company->name }} <small class="pull-right"
        ><a href="{{ route('companies.show', ['company'=>Auth::user()->username]) }}" class="btn btn-primary">View</a></small></h1>
@stop
@section('layout.content')
    <div class="card">
        <div class="card-header">
        </div>
        <div class="card-body">
            <form action="{{ route('companies.update',['company'=>$company->id]) }}" method="post">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Contact information</h4>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text"
                                   class="form-control"
                                   name="name"
                                   value="{{ $company->name ?? null }}"
                            />
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <input type="text"
                                   class="form-control"
                                   name="address"
                                   value="{{ $company->address ?? null }}"
                            />
                        </div>

                        <div class="form-group">
                            <label>City</label>
                            <input type="text"
                                   name="city"
                                   class="form-control"
                                   value="{{ $company->city ?? null }}" />
                        </div>
                        <div class="form-group">
                            <label>Zip</label>
                            <input type="text"
                                   name="zip"
                                   class="form-control"
                                   value="{{ $company->zip ?? null }}" />
                        </div>
                        <div class="form-group">
                            <label>Country</label>
                            <select name="country_id" class="form-control">
                                @foreach($countries as $country)
                                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Phone number</label>
                            <input type="text"
                                   name="main_phone_number"
                                   class="form-control"
                                   value="{{ $mainPhoneNumber->phone_number ?? null }}"
                            />
                            <input type="hidden" name="main_phone_number_id" value="{{ $mainPhoneNumber->id ?? 0 }}">
                        </div>
                        <div class="form-group">
                            <label>Phone 2</label>
                            <input type="text"
                                   name="second_phone_number"
                                   class="form-control"
                                   value="{{ $secondPhoneNumber->phone_number ?? null }}"
                            />
                            <input type="hidden" name="second_phone_number_id" value="{{ $secondPhoneNumber->id ?? 0 }}">
                        </div>
                        <div class="form-group">
                            <label>Phone 3</label>
                            <input type="text"
                                   name="third_phone_number"
                                   class="form-control"
                                   value="{{ $thirdPhoneNumber->phone_number ?? null }}"
                            />
                            <input type="hidden"
                                   name="third_phone_number_id"
                                   value="{{ $thirdPhoneNumber->id ?? 0}}" />
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text"
                                   name="email"
                                   class="form-control"
                                   value="{{ $company->email ?? null }}"
                            />
                        </div>
                        <div class="form-group">
                            <label>Website url</label>
                            <input type="text"
                                   name="url"
                                   class="form-control"
                                   value="{{ $company->url ?? null }}"
                            />
                        </div>
                        <div class="form-group">
                            <label>Contact 1</label>
                            <input type="text"
                                   class="form-control"
                                   name="contact_1"
                                   value="{{ $contact_1->name ?? null }}" />
                            <input type="hidden"
                                   name="contact_1_id"
                                   class="form-control"
                                   value="{{ $contact_1->id ?? 0 }}"
                            />
                        </div>
                        <div class="form-group">
                            <label>Contact 2</label>
                            <input type="text"
                                   class="form-control"
                                   name="contact_2"
                                   value="{{ $contact_2->name ?? null }}"  />
                            <input type="hidden"
                                   name="contact_2_id"
                                   class="form-control"
                                   value="{{ $contact_2->id ?? 0 }}"
                            />
                        </div>
                        <div class="form-group">
                            <label>Contact 3</label>
                            <input type="text"
                                   class="form-control"
                                   name="contact_3"
                                   value="{{ $contact_3->name ?? null }}" />
                            <input type="hidden"
                                   name="contact_3_id"
                                   class="form-control"
                                   value="{{ $contact_3->id ?? 0 }}"
                            />
                        </div>
                        <div class="form-group">
                            <label>{{ __("Description") }}</label>
                            <textarea name="description" id="description" class="form-control">{{$company->description ?? null}}</textarea>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
@section('javascript')
@stop