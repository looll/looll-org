@extends('layouts.app')
@section('content')
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    @yield("title")
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body">
                        @include('companies.partials._menu')
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                @yield("layout.content")
            </div>
        </div>
    </div>
@endsection