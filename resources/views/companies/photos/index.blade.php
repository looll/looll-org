@extends('companies.layout')
@section("title")
    <h1>Photos</h1>
@stop
@section("scripts.head")
    <link type="text/css" rel="stylesheet" href="/css/dropzone.css" />
@stop
@section("layout.content")
    <div class="card">
        <div class="card-body" id="app">
            @include("photo.partials.upload", ['groupId'=>0, 'profileId'=>0, 'companyId'=>$company->id])
        </div>
    </div>
@stop
@section("scripts.bottom")
    <script src="{{ mix('js/companies/photos/app.js') }}"></script>
    <script id="thumbnail-view" type="text/x-handlebars-template">
        <div class="imageholder" id="imageholder@{{id}}">
            <figure>
                <img src="@{{src}}" alt="@{{filename}}" class="image" data-image-id="@{{id}}">
                <span class="fa fa-times-circle remove-images" data-image-id="@{{id}}"></span>
                <span class="fa @{{star}} primary-image" data-image-id="@{{id}}" data-primary="@{{image_primary}}"></span>
            </figure>
        </div>
    </script>
@stop