@extends('companies.layout')
@section("layout.content")
    <div class="row">
        <div class="col-md-12">
            <h1>{{ __("Announcements") }}</h1>
            <form action="{{ route("companies.announcements.store", ["company"=>$company->id]) }}"
                  method="post">
                @csrf
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="published_at" value="{{ now() }}" class="custom-control-input" id="customCheck1">
                                <label class="custom-control-label" for="customCheck1">Publish announcement</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>{{ __("Body") }}</label>
                            <textarea name="body"
                                      class="form-control @error('body') is-invalid @enderror">{{ $announcement->body ?? old("body") }}</textarea>
                            @error('body')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary">{{ __("Save") }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
@section('scripts.footer')

@stop