@extends('companies.layout')
@section("layout.content")
    <div class="col-md-12">
        <form action="{{ route("companies.tags.store", ["company"=>$company->id]) }}" method="post">
            @csrf
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control" name="title" value="{{ old("title") }}" />
                    </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary">{{ __("Save") }}</button>
                </div>
            </div>
        </form>
    </div>
@stop