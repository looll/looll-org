@extends('companies.layout')
@section("layout.content")
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>{{ __("Title") }}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($company->tags->all() as $tag)
                        <tr>
                            <td>{{ $tag->title }}</td>
                            <td>
                                <form action="{{ route("companies.tags.destroy", ["company"=>$company->id, "tag"=>$tag->id]) }}" method="post">
                                    @method("DELETE")
                                    @csrf
                                    <button class="btn btn-danger">{{__("DELETE")}}</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <a class="btn btn-primary" href="{{ route("companies.tags.create", ['company'=>$company->id]) }}">{{ __("Create") }}</a>
            </div>
        </div>
    </div>
@stop