@extends("layouts.app");
@section("content")
    <div class="jumbotron jumbotron-fluid mb-0 bg-gradient-secondary">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <img src="/img/no-image.jpg" class="img-thumbnail mr-3 float-left" width="150px" />
                                <h1 class="font-weight-bolder">Name</h1>
                                <address>
                                    <span class="fa fa-phone-square"></span><a href="tel:123-456-7890">CLICK TO CALL</a><br />
                                    <span class="fa fa-envelope"></span><a href="mailto:webmaster@example.com">Jon Doe</a>
                                </address>

                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid p-0">
        <div class="alert alert-secondary" role="alert">
            <div class="container h3 mb-0">
                <span class="h3 mb-0 fa fa-exclamation-circle text-"></span> This is announcement
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">About</div>
                    <div class="card-body">
                        <p>The European languages are members of the same family. Their separate existence is a myth. For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ in their grammar, their pronunciation and their most common words. Everyone realizes why a new common language would be desirable: one could refuse to pay expensive translators.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <ul class="list-unstyled opening-hours">
                            <li>Sunday <span class="pull-right">Closed</span></li>
                            <li>Monday <span class="pull-right">9:00-22:00</span></li>
                            <li>Tuesday <span class="pull-right">9:00-22:00</span></li>
                            <li>Wednesday <span class="pull-right">9:00-22:00</span></li>
                            <li>Thursday <span class="pull-right">9:00-22:00</span></li>
                            <li>Friday <span class="pull-right">9:00-23:30</span></li>
                            <li>Saturday <span class="pull-right">14:00-23:30</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9">
            </div>
        </div>
    </div>
@stop