@extends("layouts.app")
@section("content")
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>{{ __("Register company") }}</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('companies.store') }}" method="post">
                    @csrf
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label>{{ __("Name") }}</label>
                                <input type="text"
                                       name="name"
                                       class="form-control @error("name") is-invalid @enderror" value="{{ Auth::user()->name }}"  />
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>{{ __("Email") }}</label>
                                <input type="email" name="email" value="{{ Auth::user()->email }}" class="form-control @error("email") is-invalid @enderror" />
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>{{__("Tin number")}}</label>
                                <input type="text" name="tin" class="form-control @error("tin") is-invalid @enderror" value="{{ old("tin") }}" />
                                @error('tin')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-row">
                                <div class="form-group col-sm-4">
                                    <label>{{ __("Phone") }}</label>
                                    <input type="text" name="phone_number[]['phone_number']" class="form-control" value="{{ old("phone_number.0.phone_number") }}" inputmode="tel" placeholder="8473619"  />
                                    <input type="hidden" name="phone_number[]['phone_type']" value="main" />
                                </div>
                                <div class="form-group col-sm-4">
                                    <label>{{ __("Phone 2") }}</label>
                                    <input type="text" name="phone_number[]['phone_number']" class="form-control" inputmode="tel" placeholder="8473619"  />
                                    <input type="hidden" name="phone_number[]['phone_type']" value="second" />
                                </div>
                                <div class="form-group col-sm-4">
                                    <label>{{ __("Phone 3") }}</label>
                                    <input type="text" name="phone_number[]['phone_number']" class="form-control" inputmode="tel" placeholder="8473619"  />
                                    <input type="hidden" name="phone_number[]['phone_type']" value="third" />
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-sm-4">
                                    <label>{{ __("Contact") }}</label>
                                    <input type="text" name="contact[][name]" value="{{ old("contact.0.name") }}" class="form-control @error("contact.0.name") is-invalid @enderror" />
                                    @error('contact.0.name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group col-sm-4">
                                    <label>{{ __("Contact 2") }}</label>
                                    <input type="text" name="contact[][name]" class="form-control" />
                                </div>
                                <div class="form-group col-sm-4">
                                    <label>{{ __("Contact 3") }}</label>
                                    <input type="text" name="contact[][name]" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Website url</label>
                                <input type="text"
                                       name="url"
                                       class="form-control @error("url") is-invalid @enderror"
                                       value=""
                                />
                                @error('url')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>{{ __("Address") }}</label>
                                <input type="text" name="address" class="form-control @error("address") is-invalid @enderror" />
                                @error('address')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-row">
                                <div class="form-group col-sm-4">
                                    <label>{{__("Zip")}}</label>
                                    <input type="text" name="zip" class="form-control @error("zip") is-invalid @enderror" />
                                    @error('zip')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group col-sm-4">
                                    <label>{{__("City")}}</label>
                                    <input type="text" name="city" class="form-control @error("city") is-invalid @enderror" />
                                    @error('city')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group col-sm-4">
                                    <label>{{ __("Countries") }} </label>
                                    <select name="country_id" class="form-control">
                                        @foreach($countries as $country)
                                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" type="submit">{{ __("Save") }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop