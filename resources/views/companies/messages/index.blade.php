@extends('companies.layout')
@section("layout.content")
    <div class="col-md-12">
        <form action="{{ route("companies.messages.store", ["company"=>$company->id]) }}" method="post">
        <div class="card">
            <div class="card-body">
               <table class="table">
                   <thead>
                       <tr>
                           <th>Subject</th>
                           <th></th>
                       </tr>
                   </thead>
                   <tbody>
                   @foreach($messages as $message)
                   <tr>
                       <td>{{ $message->subject }}</td>
                       <td></td>
                   </tr>
                   @endforeach
                   </tbody>
               </table>
            </div>
            <div class="card-footer">
                <a class="btn btn-primary" href="{{ route("companies.messages.create", ["company"=>$company->id]) }}">{{ __("Create") }}</a>
            </div>
        </div>
        </form>
    </div>
@stop