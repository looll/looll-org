@extends('companies.layout')
@section("layout.content")
    <div class="col-md-12">
        <form action="{{ route("companies.messages.store", ["company"=>$company->id]) }}" method="post">
            @csrf
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label>{{ __("Subject") }}</label>
                        <input type="text" name="subject" value="{{ old("subject")  }}" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>{{ __("Body") }}</label>
                        <textarea name="body" class="form-control">{{old("body")}}</textarea>
                    </div>
                </div>
                <div class="card-footer">
                    <button name="publish" value="publish" class="btn btn-primary">{{ __("Publish") }}</button>
                    <button name="draft" value="draft" class="btn btn-default">{{ __("Save as draft") }}</button>
                </div>
            </div>
        </form>
    </div>
@stop