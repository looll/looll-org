    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('meta.description')" />
    <meta name="robots" content="index,follow" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ mix('css/app.css', env('APP_SECURITY', false)) }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.css ', env('APP_SECURITY', false)) }}" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="/img/favicon.ico" />
    <link rel="canonical" href="http://looll.is/" />
    <!--[if IE]>
    <script type="text/javascript" src="/js/c3-multi-column.js"></script>
    <script src="/js/modernizer.js"></script>

        <![endif]-->