<div class="sticky-top">
  <div class="collapse" id="navbarSearch">
    <div class="bg-dark p-4">
      <form action="" accept-charset="UTF-8" method="get">
        <div class="input-group">
          <input type="text" 
          name="search" 
          id="search"  
          placeholder="Search accounts, contracts and transactions" class="form-control">
          <span class="input-group-append">
            <input type="submit" name="commit" value="Search" class="btn " data-disable-with="Search">
          </span> 
        </div>
      </form>
    </div>
  </div>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
      <a class="navbar-brand font-weight-bolder" href="/">Looll</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse w-100 dual-collapse2 order-1 order-md-0" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item {{ set_active('about') }}">
            <a class="nav-link" href="/about">@lang('looll.About')</a>
          </li>
        </ul>
        <ul class="navbar-nav">
          @if(Auth::check())
          <li class="nav-item">
            <a class="nav-link" href="{{ route('posts.index') }}">
                <span class="badge badge-info" @if($unReadPost == 0) hidden @endif>{{ $unReadPost ?? 0 }}
                    <span class="fa fa-envelope-o" aria-hidden="true" style="font-size: 17px;"></span>
                </span>
                <span class="fa fa-envelope-o" @if($unReadPost > 0) hidden @endif></span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="@if(Auth::user()->hasMyBook() )
            {{ route('myBooks.show', ['myBook'=>$myBookId]) }}
            @else
              {{ route('myBooks.create') }}
            @endif"
            >{{ Lang::get('looll.MyBook') }}</a>
          </li>
          @if(Auth::user()->hasRole('Person'))
          <li class="nav-item">
            <a class="nav-link"
               href="{{ route('profile', ['profile'=>Auth::user()->username]) }}">{{ Lang::get('looll.MySite') }}</a>
          </li>
          @endif 
          @if(Auth::user()->hasRole('Company') && Auth::user()->company()->exists())
          <li class="nav-item">
            <a class="nav-link"
               href="{{ route('profile', ['profile'=>Auth::user()->username]) }}">
              {{ Lang::get('looll.MyCompany') }}
            </a>
          </li>
          @endif 
          <li class="nav-item dropdown">
            <a href="#" 
            class="nav-link dropdown-toggle" 
            data-toggle="dropdown" 
            role="button" 
            id="navbarDropdown"
            aria-haspopup="true" 
            aria-expanded="false"
            >
              <span class="caret"></span></a>
                <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">
                  @if(Auth::user()->hasRole('admin'))
                  <a class="dropdown-item" href="{{ route('admin.dashboard') }}">Admin site</a>
                  @endif
                    <a class="dropdown-item" href="{{ route('users.edit', ['user'=>Auth::user()->id ]) }}">{{  Lang::get('looll.Settings') }}</a>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                    </form>
                </div>
        </li>
          @else
          <li class="nav-item {{ set_active('register') }}">
            <a class="nav-link" href="/register">{{ Lang::get('looll.Users.Register') }}</a>
          </li>
          <li class="nav-item {{ set_active('login') }}">
            <a class="nav-link" href="/login">{{ Lang::get('looll.Users.Login') }}</a>
          </li>
          @endif
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" data-target="#navbarSearch" aria-controls="navbarSearch" aria-expanded="false" aria-label="Search" ><span class="fa fa-search"></span></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</div>

