<div class="container" style="height: 90%">
    <div class="row d-flex justify-content-center align-items-center h-75">
        <div class="col-sm-6">
            <h1>looll <small class="white">Search</small></h1>
            <form action="{{ route('searches.index') }}" method="get">
                <div class="form-group">
                    <div class="input-group">
                        <input type="search" name="q" placeholder="{{ Lang::get('looll.SearchWords') }}" class="form-control" />
                        <div class="input-group-append">
  
                            <button type="submit" class="btn btn-outline-secondary">{{ Lang::get('looll.Search') }}</button>
                          </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
</div>
</div>
