<x-layout>
    <div class="min-h-75 overflow-hidden pt-5 pb-5" style="background-color: #ffaa00;">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-sm-6">
                    <div class="card mt-4" style="opacity: 0.8;">
                        <div class="card-body">
                            <h3><strong>Why sign up?</strong></h3>
                            <p>Looll.is is a website that helps individuals and organizations to provide contact information to those who need it. All information placed on the site is visible to everyone and when a user deletes information from the site, it is permanently deleted from our servers.</p>
                            <h4><strong>Individuals</strong></h4>
                            <p>For individuals, the site is an advanced address book with information entered by the users. It is our hope that users make their page personal with pictures and descriptive texts about themselves. It is up to each user to decide what information he or she likes to share.</p>
                            <h4><strong>Groups</strong></h4>
                            <p>The site makes it easier for organizations to get messages to members.  Pages for organizations are created by their current members who enter the relevant information and become administrators for the organization´s page.  The administrator can publish messages or news, which are easy to change or update.  In the near future the messages will automatically be sent to members  </p>
                            <address>
                                <strong>Leikfang ehf.</strong><br>
                                Loftur Már Sigurðsson<br>
                                <a href="mailto:loftur@looll.is">loftur@looll.is</a>
                                <a href="/disclaimer" class="btn btn-primary pull-right">Disclaimer</a>
                            </address>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-slot name="title">
        {{ __("About Us") }}
    </x-slot>
    <x-slot name="metaDescription">
    </x-slot>
    <x-slot name="scriptsFooter">
    </x-slot>
</x-layout>