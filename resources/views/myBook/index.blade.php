<x-layout>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <input type="text" placeholder="Search" class="form-control" value="" />
                                </div>
                            </div>
                            <div class="col-sm-7">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-slot name="title">
        MyBook
    </x-slot>
    <x-slot name="metaDescription">
        Description
    </x-slot>
    <x-slot name="scriptsFooter"></x-slot>
</x-layout>

