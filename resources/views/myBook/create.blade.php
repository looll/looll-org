<x-layout>
    <div class="min-h-75 overflow-hidden pt-5 pb-5" style="background-color: #ffaa00;">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-6">
                    <h1>{{__("My Book")}}</h1>
                    <form action="{{ route('myBooks.store') }}" method="post">
                        {{ csrf_field() }}
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>{{ __("Name") }}</label>
                                    <input type="text" name="name" class="form-control" value="My book" />
                                    @if($errors->any())
                                        <label class="label label-danger">
                                            {{ $errors->first('name') }}
                                        </label>
                                    @endif
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">{{ __("Create My book") }}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <x-slot name="metaDescription">{{ __("Create a new myBook") }}</x-slot>
    <x-slot name="title">{{ __("Create a new myBook") }}</x-slot>
    <x-slot name="scriptsFooter"></x-slot>
</x-layout>