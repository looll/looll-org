<x-layout>
    <div class="min-h-75 overflow-hidden pt-5 pb-5" style="background-color: #ffaa00;">
        <div class="container"
             id="myBook"
             data-category-index="{{ route('api.categories.index') }}"
             data-mybook-id="{{ $myBook->id }}"
        >
            <div class="row">
                <div class="col-md-12">
                    <h1 class="font-weight-bold">{{ $myBook->name }}</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="list-group mb-4" v-if="categoryLists.length > 0" >
                        <a class="list-group-item" v-for="category in categoryLists"
                               v-on:click="selectCategory(category.id, category)"
                               :href="'#'+category.id"
                               :aria-controls="category.id"
                               :id="'category-'+category.id"
                               role="tab"
                               data-toggle="tab"
                               v-bind:class="[category.id == current ? active : '']"
                            >
                            @{{ category.name }}
                        </a>
                    </div>
                    <form action="{{ route('categories.store') }}"
                          method="post"
                          v-on:submit.prevent="saveCategory('{{ route('api.categories.store') }}')"
                    >
                        <div class="card">
                            <div class="card-body">
                                <input type="hidden"
                                       name="parent_id"
                                       v-model="parentId"
                                />
                                <div class="form-group">
                                    <label>New Category</label>
                                    <input type="text"
                                           v-model="name"
                                           class="form-control"
                                           name="name"
                                           required
                                    />
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit"
                                        class="btn btn-primary btn-sm"
                                >Save</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-9">
                    <div class="tab-content">
                        <div v-for="category in categoryLists"
                             :id="category.id"
                             class="tab-pane"
                             v-bind:class="[category.id == current ? active : '']"
                             role="tabpanel"
                        >
                            <div class="card">
                                <div class="card-header d-flex justify-content-between align-items-center">
                                    <h2 class="card-title font-weight-normal h6 mb-0">@{{ category.name }}</h2>
                                    <ul class="nav navbar-nav mb-0 py-0">
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle py-0"
                                               v-show="category.name != 'All'"
                                               href="#" role="button" id="dropdownMenuLink"
                                               data-toggle="dropdown"
                                               aria-haspopup="true"
                                               aria-expanded="false">
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                                <a href="#" class="dropdown-item" v-on:click="deleteCategory(category)">
                                                    <span class="fa fa-trash"></span> Delete</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="card-body">
                                   <div class="row">
                                       <div class="col-md-3"
                                            v-for="profile in category.profiles"
                                            :id="'cat-'+category.id+'-pro-'+profile.id">
                                           <div class="card">
                                               <img class="card-img-top"
                                                    v-bind:src="profile.primary_photos[0].src"
                                                    :alt="profile.name"
                                                    v-if="profile.primary_photos.length > 0"
                                               />
                                               <img class="card-img-top"
                                                    src="/img/no-image.jpg"
                                                    :alt="profile.name"
                                                    v-else />
                                               <div class="card-body">
                                                   <h5>
                                                       <a :href="'/'+profile.user.username"
                                                          target="_blank"
                                                       >
                                                           <strong v-text="truncate(profile.name)"></strong>
                                                       </a>
                                                   </h5>
                                                   <p>
                                                       <span>@{{ profile.address}}</span>
                                                       @{{ profile.city }}
                                                   </p>
                                                   <div class="dropdown card-img-overlay p-0">
                                                       <a class="btn btn-circle dropdown-toggle" href="#" role="button" id="'link-collapse-cat-'+category.id+'-profile-'+profile.id" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>

                                                       <div class="dropdown-menu" aria-labelledby=id="'link-collapse-cat-'+category.id+'-profile-'+profile.id">
                                                           <form class="px-4 py-3">
                                                               <div class="form-group" v-for="category_item in categoryLists">
                                                                   <div class="form-check">
                                                                       <input type="checkbox"
                                                                              class="form-check-input"
                                                                              :value="category_item.id"
                                                                              v-model="profile.selected"
                                                                              :disabled="category_item.name == 'All'"
                                                                              :id="'dropdownCheck'+category_item.id"
                                                                              v-on:change="addToCategory(category, category_item, profile, '{{ route('myBooks.show', ['myBook'=>$myBook->id]) }}', checkForCategory(profile.categories, category_item) )"
                                                                       />
                                                                       <label class="form-check-label" for="'dropdownCheck'+category_item.id">
                                                                           @{{ category_item.name }}
                                                                       </label>
                                                                   </div>
                                                               </div>
                                                           </form>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-slot name="title">
        {{ $myBook->name }}
    </x-slot>
    <x-slot name="metaDescription">
        my own {{ $myBook->name }};
    </x-slot>
    <x-slot name="scriptsFooter">
        <script src="{{ mix("js/myBook/app.js") }}" defer></script>
    </x-slot>
</x-layout>