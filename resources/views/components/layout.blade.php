<!DOCTYPE html>
<html class="m-h-100 w-100">
<head>
    <x-head>
        <x-slot name="title">
            {{ $title }}
        </x-slot>
        <x-slot name="metaDescription">
            {{ $metaDescription }}
        </x-slot>
    </x-head>
</head>
<body class="m-h-100 w-90" style=" background-color: #ffaa00;">
<div class="container-fluid m-h-100 p-0">
    <x-header></x-header>
    {{ $slot }}
</div>

<script>
    @if(\Illuminate\Support\Facades\Auth::check())
        window.accessToken = <?php echo json_encode(
        ['accessToken'=> \Illuminate\Support\Facades\Auth::user()->createToken('looll.personal')->accessToken])
    ?>
    @endif
</script>

<script src="{{ mix("js/app.js") }}" defer></script>
<script src="{{ asset('js/modernizr.js', env('APP_SECURITY', false)) }}"></script>

    {{ $scriptsFooter ?? "" }}

<x-slot name="scriptsFooter">
    {{ $scriptsFooter }}
</x-slot>
<script>
    var code = "";
    if(document.location.hostname === "looll.is")
    {
        code = "UA-53044404-1";
    }
    else
    {
        code = "UA-53044404-2"
    }

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', code, 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>