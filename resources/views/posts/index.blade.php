<x-layout>
    <div class="min-h-100  pt-5 pb-5" style="background-color: #ffaa00;">
        <div class="container" id="posts">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center">{{ __("Posts") }}</h1>
                </div>
            </div>

                @foreach($posts as $post )
                <div class="row d-flex justify-content-center">
                    <div class="col-sm-6 mb-3">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title font-weight-bold">{{ $post->subject }}</h4>
                                <p>{{ $post->body }}</p>
                            </div>
                            <div class="card-footer">
                                {{ __("By:", ["name"=>$post->postable->name ?? null]) }}
                                <span class="pull-right">{{ $post->published_at->diffForHumans() }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
        </div>
    </div>
    <x-slot name="title">
        Posts
    </x-slot>
    <x-slot name="metaDescription">
        Posts
    </x-slot>
    <x-slot name="scriptsFooter">
    </x-slot>
</x-layout>