<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Welcome to looll</h2>
<div>
    <p>
        <a href="{{ route('auth.confirmation', ['code' => $token]) }}">To finish the registration you need to verify you e-mail.</a>
    </p>
    <p>
        If you did not create the account, please do nothing.  The account will be deleted after one week.
    </p>
</div>
</body>
</html>