<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Reset password</h2>
<div>
    <p>
        <a href="{{ url('password/reset/'.$token) }}">Click here to reset your password</a>
    </p>
</div>
</body>
</html>
