<x-layout>
    <div class="min-h-75 overflow-hidden @if (!session('resent')) pt-5 @endif pb-5" style="background-color: #ffaa00;">
        @if (session('resent'))
            <div class="alert alert-success mb-5 text-center" role="alert">
                <div class="container">
                {{ __('A fresh verification link has been sent to your email address.') }}
                </div>
            </div>
        @endif
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <h1 class="font-weight-bold">{{ __('Verify Your Email Address') }}</h1>
                    <div class="card">
                        <div class="card-body">

                            {{ __('Before proceeding, please check your email for a verification link.') }}
                            {{ __('If you did not receive the email') }},
                            <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                                @csrf
                                <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-slot name="title">
        Verify
    </x-slot>
    <x-slot name="metaDescription">
        Verify
    </x-slot>
    <x-slot name="scriptsFooter">
    </x-slot>
</x-layout>
