<x-layout>
    <div class="min-h-75 overflow-hidden pt-5 pb-5 flex flex-column" style="background-color: #ffaa00;">
        <div class="row align-content-center justify-content-center" style="flex: 1">
            <div class="col-md-6">
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="card">
                        <div class="card-body">
                            <div class="mb-3">
                                <label for="name" class="form-label">{{ __('Name') }}</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror"
                                       id="name"
                                       name="name"
                                       required
                                       autocomplete="name"
                                       aria-describedby="nameHelp"
                                       autofocus
                                       value="{{ old("name") }}"
                                >
                                @error('name')
                                <div class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="gender" class="form-label">{{  Lang::get("looll.Profiles.Gender")  }}</label>
                                <select name="gender" class="form-control">
                                    <option value="male" selected="selected">{{ Lang::get("looll.Profiles.Male") }}</option>
                                    <option value="female">{{ Lang::get("looll.Profiles.Female") }}</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="email" class="form-label">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                            </div>
                            <div class="mb-3">
                                <label for="username" class="form-label">{{ __('Username') }}</label>
                                <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username">
                                @error('username')
                                    <div class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="password" class="form-label">{{ __('Password') }}</label>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                            </div>
                            <div class="mb-3">
                                <label for="password-confirm"
                                       class="form-label">{{ __('Confirm Password') }}</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input @error("disclaimer") is-invalid @enderror" name="disclaimer" type="checkbox" value="1" id="invalidCheck3" required>
                                    <label class="form-check-label" for="invalidCheck3">
                                        {{ __("Agree to ") }}<a href="/disclaimer">disclaimer</a>
                                    </label>

                                    @error("disclaimer")
                                        <div class="invalid-feedback">
                                            {{ __("You must agree before submitting.") }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">{{ __("Register") }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <x-slot name="title">
        Register
    </x-slot>
    <x-slot name="metaDescription">
        Registers
    </x-slot>
    <x-slot name="scriptsFooter">
    </x-slot>
</x-layout>