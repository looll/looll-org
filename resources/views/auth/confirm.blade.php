<x-layout>
    <div class="container">
        <div class="row box">
            <div class="col-sm-12">
                <h1>@lang("looll.VerifyYourEmail")</h1>
                <p>@lang("looll.VerifyText")</p>
            </div>
        </div>
    </div>
    <x-slot name="title">
        Confirm
    </x-slot>
    <x-slot name="metaDescription">
        Looll is a website that helps individuals and organizations to provide contact information and to be visible on the web.
    </x-slot>
</x-layout>