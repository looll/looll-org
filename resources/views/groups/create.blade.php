<x-layout>
    <div class="min-h-100  pt-5 pb-5" style="background-color: #ffaa00;">
        <div class="container" id="edit-group">
            <div class="row">
                <div class="col-sm-12">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="edit-profile">
                            <form action="{{ route('groups.store') }}" method="post">
                                {{ csrf_field() }}
                                @include("groups.partials._form")
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-slot name="title">
    </x-slot>
    <x-slot name="metaDescription">
    </x-slot>
    <x-slot name="scriptsFooter">
    </x-slot>
</x-layout>