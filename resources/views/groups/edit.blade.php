<x-layout>
    <div class="min-h-100  pt-5 pb-5" style="background-color: #ffaa00;">
    <div class="container" id="edit-group">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header ">
                        <ul class="nav nav-pills card-header-pills" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active"
                                   href="#home"
                                   aria-controls="home"
                                   role="tab"
                                   data-toggle="tab">{{ __("Edit") }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"
                                   href="#edit-image"
                                   aria-controls="edit-image"
                                   role="tab"
                                   data-toggle="tab">{{ __("Edit image") }}</a>
                            </li>
                            <li class="nav-item">
                                <a href="#edit-announcement"
                                   class="nav-link"
                                   aria-controls="announcement"
                                   role="tab"
                                   data-toggle="tab"
                                >{{ __("Announcement") }}</a>
                            </li>
                            <li class="nav-item">
                                <a href="#posts"
                                   aria-controls="posts"
                                   role="tab"
                                   class="nav-link"
                                   data-toggle="tab"
                                >{{ __("Post to members") }}</a></li>
                            <li class="nav-item">
                                <a href="#members"
                                   aria-controls="members"
                                   class="nav-link"
                                   role="tab"
                                   data-toggle="tab">{{ __("Members") }}</a></li>
                        </ul>
                    </div>
                    <div class="card-body tab-content">
                        <div class="tab-pane active" id="home" data-group-save-url="{{ route("api.groups.update", ['group'=>$group->id]) }}" data-group-url="{{ route("api.groups.show", ["group"=>$group->id]) }}">
                            <form action="{{ route('groups.update', ["group"=>$group->id]) }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <div class="row">
                                    <div class="col-sm-12">
                                        @include("groups.partials._vueform")
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="edit-image">
                            @include("photo.partials.upload", ['groupId'=>$group->id, 'profileId'=>0])
                        </div>
                        <div class="tab-pane" id="edit-announcement">
                            <form action="{{ route('groups.announcements.store', ['group'=>$group->id]) }}">
                                <div class="form-group">
                                <textarea name="body"
                                          id="announcement"
                                          class="form-control"
                                          v-model="announcement.body"
                                ></textarea>
                                </div>
                                <div class="form-group">
                                    <button v-bind:class="['btn', saved ? success : primary]"
                                            @click.prevent="saveAnnouncement('{{ route('api.groups.announcements.store', ['group'=>$group->id]) }}')"
                                            type="submit">@lang('looll.Save')</button>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="posts" data-group-posts-url="{{ route("api.groups.posts.index", ["group"=>$group->id]) }}">
                            <div class="row">
                                <div class="col-md-6">
                                    <table class="table table-condensed table-striped">
                                        <thead>
                                        <tr>
                                            <th>Subject</th>
                                            <th>Published</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr v-for="post in shared.posts">
                                            <td>@{{ post.subject }}</td>
                                            <td>@{{ post.published_at_humans}}</td>
                                            <td><button type="button"
                                                        class="btn btn-primary"
                                                        v-on:click="openModel(post)"
                                                        data-toggle="modal"
                                                        data-target="#exampleModalLong"
                                                >View</button>
                                                <button class="btn btn-danger"
                                                        v-on:click="deletePost(post)">Eyða</button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <table class="table table-condensed table-striped">
                                        <thead>
                                        <tr>
                                            <th>Subject</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr v-for="post in shared.draftPosts">
                                            <td>@{{ post.subject }}</td>
                                            <td><button type="button"
                                                        class="btn btn-primary"
                                                        v-on:click="openModel(post)"
                                                        data-toggle="modal"
                                                        data-target="#exampleModalLong"
                                                >View</button>
                                                <button class="btn btn-danger" v-on:click="deletePost(post)">Eyða</button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <form action="" method="" class="mt-4">
                                <div class="form-group">
                                    <label>Subject</label>
                                    <input type="text"
                                           name="subject"
                                           class="form-control"
                                           v-model="subject"
                                    />
                                </div>
                                <div class="form-group" >
                                    <label>Post</label>
                                    <textarea id="post"
                                              name="body"
                                              v-model="body"
                                              class="form-control"
                                    ></textarea>
                                </div>
                                <button type="submit"
                                        name="draft"
                                        value="true"
                                        v-on:click.prevent="saveAsDraft('{{ route('api.groups.posts.store', ['group'=>$group->id]) }}')"
                                        class="btn btn-primary"
                                >Save as draft</button>
                                <button type="submit"
                                        name="publish"
                                        value="publish"
                                        class="btn btn-primary"
                                        v-on:click.prevent="publishPost('{{ route('api.groups.posts.store', ["group"=>$group->id]) }}')"
                                >Publish</button>
                            </form>
                            <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">@{{ post.subject}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>Subject</label>
                                                <input type="text"
                                                       name="subject"
                                                       v-model="post.subject"
                                                       class="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label>Body</label>
                                                <textarea name="body" class="form-control" v-model="post.body"></textarea>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-default" v-on:click="editPost(post.id)">Save</button>
                                            <button type="button" class="btn btn-primary" v-on:click="publishSavedPost(post.id)">Publish post</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane"
                             id="members" data-group-users-url="{{ route('api.groups.users.index', ['group'=>$group->id,'type'=>'contacts']) }}">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-header">
                                            {{ __("Members") }}
                                        </div>
                                        <div class="card-body">
                                            <table class="table table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>{{ __("Name") }}</th>
                                                    <th>@lang("looll.Role") </th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if(Auth::user()->isOwnerOf($group) || Auth::user()->isAdminOf($group))
                                                    @foreach($members as $member)
                                                    <tr id="member{{ $member->id }}">
                                                        <td>{{ $member->profile->name ?? $member->name }}</td>
                                                        <td>{{ $member->roleOnGroup($group)->first()->name ?? "" }}</td>
                                                        <td>
                                                            <button class="btn btn-primary @if($member->role->accepted === 0) d-none @endif"
                                                                    id="editUser{{ $member->id }}"
                                                                    data-toggle="modal"
                                                                    data-target="#editModal"

                                                                    v-on:click="updateContact('{{ route('api.users.groups.update', ['group'=>$group->id, 'user'=>$member->id]) }}')"
                                                                        :disabled="contacts.length === 2 && checkMembers({{ $member->id }})"
                                                            >
                                                                <span class="fa fa-edit" ></span></button>
                                                            <button class="btn btn-success @if($member->role->accepted === 1) d-none @endif"
                                                                    v-on:click="acceptUser({{ $member->id }},'{{ route('api.groups.users.update',['group'=>$group->id, 'user'=>$member->id ]) }}')"
                                                                    id="acceptUser{{ $member->id }}"><span class="fa fa-check"></span></button>
                                                            <button class="btn btn-danger"
                                                                    v-on:click="removeUser('{{ route('api.groups.users.destroy',['group'=>$group->id, 'user'=>$member->id]) }}', {{ $member->id }})"
                                                            >
                                                                <span class="fa fa-times"></span>
                                                            </button>

                                                        </td>
                                                    </tr>
                                                @endforeach
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="" method="post">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="editModalLabel">Edit Role of User in the group</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form_group">
                                                    <select class="form-control" name="role_id" v-model="role" >
                                                        <option v-for="role in roles" :value="role.id">@{{ role.name }}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="button" @click="updateRole" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <x-slot name="title">
    </x-slot>
    <x-slot name="metaDescription">
    </x-slot>
    <x-slot name="scriptsFooter">
        <script>
        window.default_locale = "{{ config('app.locale') }}";
        window.fallback_locale = "{{ config('app.fallback_locale') }}";
        window.messages = @json($messages);
        window.postsIndexUrl = "{{ route("api.posts.index") }}"
        </script>
        <script src="{{ asset("js/groups/app.js") }}" defer></script>
    </x-slot>
</x-layout>