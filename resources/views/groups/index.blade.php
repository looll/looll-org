<x-layout>
    <div class="min-h-100  pt-5 pb-5" style="background-color: #ffaa00;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="pull-left">{{ __("Groups") }}</h1>
                <a href="{{ route('groups.create') }}" class="btn btn-primary pull-right">{{  __("Create a group") }}</a>
            </div>
        </div>
        <div class="row top-buffer">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header card-title">{{ __("Member of these groups") }}</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>{{ __("Name") }}</th>
                                    <th>{{ __("Action") }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($groupsByMember !== null)
                                    @foreach($groupsByMember as $group)
                                        <tr>
                                            <td><h4><strong>{{ $group->name }}</strong></h4></td>
                                            <td>
                                                <form action="{{ route('groups.users.destroy', ['group'=>$group->id, 'user'=>Auth::id()]) }}" method="post">
                                                    {{ csrf_field() }}
                                                    {{ method_field('delete') }}
                                                    <a href="{{ route('groups.show', ['group'=>$group->id]) }}" class="btn btn-primary">{{ __("View") }}</a>
                                                    <button type="submit" class="btn btn-danger">{{ Lang::get('looll.LeaveGroup') }}</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card mt-4">
                    <div class="card-header">{{ __("Pending groups") }}</div>
                    <div class="card-body">
                        <table class="table table-condensed">
                            <thead>
                            <tr>
                                <th>{{ __("looll.Name") }}</th>
                                <th{{ __("looll.Action") }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($pendingGroupsByMember !== null)
                                @foreach($pendingGroupsByMember as $group)
                                    <tr>
                                        <td><h4><strong>{{ $group->name }}</strong></h4></td>
                                        <td>
                                            <form action="{{ route('groups.users.destroy', ['group'=>$group->id, 'user'=>Auth::id()]) }}" method="post">
                                                {{ csrf_field() }}
                                                {{ method_field('delete') }}
                                                <a href="{{ route('groups.show', ['group'=>$group->id]) }}" class="btn btn-primary">{{ __("View") }}</a>
                                                <button type="submit" class="btn btn-danger">{{ Lang::get('looll.LeaveGroup') }}</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">{{ __("Admin of these groups") }}</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>{{ __("Name") }}</th>
                                    <th>{{ __("Action") }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($groupsByOwner !== null)
                                    @foreach($groupsByOwner as $group)
                                        <tr>
                                            <td><h4><strong>{{ $group->name }}</strong></h4></td>
                                            <td>
                                                <form action="{{ route('groups.destroy', ['group'=>$group->id]) }}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('delete') }}
                                                    <a href="{{ route('groups.edit', ['group'=>$group->id]) }}" class="btn btn-primary">{{ __("Edit") }}</a>
                                                    <a href="{{ route('groups.show', ['group'=>$group->id]) }}" class="btn btn-primary">{{ __("View") }}</a>
                                                    @if(Auth::user()->canByGroup("delete.group", $group->id, true))
                                                        <button class="btn btn-outline-danger">Delete</button>
                                                    @endif
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <x-slot name="title">
    </x-slot>
    <x-slot name="metaDescription">
    </x-slot>
    <x-slot name="scriptsFooter">
    </x-slot>
</x-layout>