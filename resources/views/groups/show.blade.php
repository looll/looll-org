<x-layout>
    <div class="min-h-100  pt-5 pb-5" style="background-color: #ffaa00;">
    <div class="container" id="show-group">
        <div class="row">
            <div class="col-md-3">
                @if(!empty($group->photos))
                    <div class="row">
                        <div class="col-sm-12">
                            @foreach($group->photos as $photo)
                                @if($photo->primary_photo)
                                    <div class="card">
                                        <img src="{{$photo->src}}" alt="{{$photo->filename}}" class="card-img-top img-responsive" />
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        <div class="col-sm-12">
                            <div class="crop">
                                @foreach($group->photos as $photo)
                                    @if(!$photo->primary_photo)
                                        <div class="card">
                                            <img src="{{$photo->src}}"
                                                 alt="{{$photo->filename}}"
                                                 class="img-responsive card-img-top" />
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-sm-6">
                <div class="card mb-4">
                    <div class="card-body">
                        <h1 class="font-weight-bolder">{{ $group->name ?? ""}}</h1>
                        <address>
                            {{$group->address}}<br>
                            {{$group->zip}} {{$group->city}}<br>
                            @if($group->country != null)
                                {{$group->country->name ?? null}}<br />
                            @endif
                            {{$group->email}}
                        </address>
                        <h5><strong>@lang('looll.Contacts')</strong></h5>
                        <ul class="nav nav-admin">
                            @foreach($group->contacts as $contact)
                                <li><a href="{{ route('profile', ['profile'=>$contact->username]) }}"
                                       class="adminLists"
                                    >{{ $contact->profile->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="card mb-4">
                    <div class="card-body">
                        {{ $group->announcements()->first()->body ?? "" }}
                    </div>
                </div>
                @if(Auth::check() &&
               $isAdmin == false && $groupUser == null)
                    <a href="/"
                       class="btn btn-primary"
                       id="connect"
                       v-on:click.prevent="joinGroup({{$userId}}, {{$group->id}}, '{{ route('api.groups.users.store',['group'=>$group->id])}}')"
                    >@lang('looll.Connect')</a>
                @endif
            </div>
            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body">
                        {!! $group->description !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <x-slot name="title">
        {{ $group->name }}
    </x-slot>
    <x-slot name="metaDescription">
        {!! $group->description !!}
    </x-slot>
    <x-slot name="scriptsFooter">
        <script src="{{ asset('js/groups/show.js')  }}"></script>
    </x-slot>
</x-layout>