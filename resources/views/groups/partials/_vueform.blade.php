<div class="form-group">
    <label for="name">{{ Lang::get('looll.Name') }}</label>
    <input type="text"
           name="name"
           id="name"
           class="form-control"
           :class="{ 'is-invalid' : is_invalid }"
           v-model="group.name"
           aria-describedby="helpBlock2"
    />
    <div class="invalid-feedback" v-if="is_invalid" v-for="message in errors.name">@{{ message }}</div>
</div>
<div class="form-group">
    <label for="description">{{ Lang::get('looll.About')}}</label>
    <textarea id="description"
              name="description"
              class="form-control editor"
              v-model="group.description"
    ></textarea>
</div>
<div class="form-group">
    <label for="email">{{ Lang::get('looll.Email') }}</label>
    <input type="email"
           name="email"
           class="form-control"
           v-model="group.email"
    />
    <label class="label label-danger hidden email"></label>
</div>
<div class="form-group">
    <label for="address">{{  Lang::get('looll.Address') }}</label>
    <input type="text"
           name="address"
           id="address"
           v-model="group.address"
           class="form-control"
    />
</div>
<div class="form-group">
    <label for="zip">{{ Lang::get('looll.Zip') }}</label>
    <input type="text"
           name="zip"
           id="zip"
           class="form-control"
           v-model="group.zip"
    />
</div>
<div class="form-group">
    <label for="city">{{ Lang::get('looll.City') }}</label>
    <input type="text"
           name="city"
           id="city"
           v-model="group.city"
           class="form-control"
    />
</div>
<div class="form-group">
    <label for="country_id">{{ Lang::get('looll.Country') }}</label>
    <select name="country_id" class="form-control" id="country_id" v-model="group.country_id">
        @foreach($countries as $country)
            <option value="{{ $country->id }}">{{ $country->name }}</option>
        @endforeach
    </select>
</div>
<button v-bind:class="['btn', saved ? success : primary]"
        @click.prevent="saveGroup"
        type="button">@lang('looll.Save')</button>