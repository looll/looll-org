<div class="card">
    <div class="card-header">
        <h4 class="card-title">
            @lang("looll.Information")
        </h4>
    </div>
        <div class="card-body">
            <div class="form-group @if($errors->has('name'))  has-error @endif">
                <label for="name">{{ Lang::get('looll.Name') }}</label>
                <input type="text"
                       name="name"
                       id="name"
                       class="form-control"
                       value="{{ $group->name ?? old('name') }}"
                       aria-describedby="helpBlock2"
                />
                @if($errors->has('name'))
                <span id="helpBlock2" class="help-block">{{$errors->first('name')}}</span>
                @endif
            </div>
            <div class="form-group">
                <label for="description">{{ Lang::get('looll.About')}}</label>
                <textarea id="description" name="description" class="form-control" cols="70" rows="5">{{ $group->description ?? old('description') }}</textarea>
            </div>
            <div class="form-group">
                <label for="email">{{ Lang::get('looll.Email') }}</label>
                <input type="email" name="email" class="form-control" value="{{ $group->email ?? old('email') }}" />
                <label class="label label-danger hidden email"></label>
            </div>
            <div class="form-group">
                <label for="address">{{  Lang::get('looll.Address') }}</label>
                <input type="text"
                       name="address"
                       id="address"
                       class="form-control"
                       value="{{ $group->address ?? old('address') }}"
                />
            </div>
            <div class="form-group">
                <label for="zip">{{ Lang::get('looll.Zip') }}</label>
                <input type="text" name="zip" id="zip" class="form-control" value="{{ $group->zip ?? old('zip') }}" />
            </div>
            <div class="form-group">
                <label for="city">{{ Lang::get('looll.City') }}</label>
                <input type="text" name="city" id="city" class="form-control" value="{{ $group->city ?? old('city') }}" />
            </div>
            <div class="form-group">
                <label for="country_id">{{ Lang::get('looll.Country') }}</label>
                <select name="country_id" class="form-control" id="country_id">
                    @foreach($countries as $country)
                        <option @if(isset($group->country) && $group->country->id == $country->id)
                                selected="selected"
                                @endif
                                value="{{ $country->id }}"
                        >{{ $country->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="card-footer">
            <button class="btn btn-primary" type="submit">@lang('looll.Save')</button>
        </div>
</div>
