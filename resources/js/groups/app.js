const Home = {
    data: function ()
    {
        return {
            groupUrl:"",
            groupUpdateUrl: "",
            group: {
                name:"",
                description:"",
                email:"",
                address:"",
                zip:"",
                city:"",
                country_id:1,
            },
            errors: {
                name:null
            },
            saved:false,
            primary: "btn-primary",
            success:"btn-success",
            is_invalid: false
        }
    },
    created() {

    },
    mounted() {
        let vm = this;
        this.groupUrl = $("#home").attr("data-group-url");
        axios.get(this.groupUrl, {
            headers:{
                Authorization: 'Bearer '+window.accessToken.accessToken
            }}).then(result => {
            vm.group = result.data;
        });
    },
    methods: {
        saveGroup()
        {
            let vm = this;

            this.groupUpdateUrl = $("#home").attr("data-group-save-url");
            this.saved = true;

            axios.put(this.groupUpdateUrl, this.group, {
                headers:{
                    Authorization: 'Bearer '+window.accessToken.accessToken
                }}).then(function (response)
            {
                vm.is_invalid = false;

                setInterval(function (){
                    vm.saved = false;
                }, 3000);

            }).catch((error)=>{

                vm.is_invalid = true;
                vm.errors = error.response.data.errors;

                setInterval(function (){
                    vm.saved = false;
                }, 3000);
            })


        }
    }
}
const app = Vue.createApp(Home);
app.mount("#home");

const Image = {
    mounted(){
        let photosUrl = $("#photos").attr("data-photos-url");
        Dropzone.Dropzone.options.loollDropzone = {
            maxFiles:1,
            thumbnailHeight:200,
            thumbnailWidth:200,
            addRemoveLinks:true,
            uploadMultiple:false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Authorization': 'Bearer '+window.accessToken.accessToken
            },
            init: function () {
                let thisDropzone = this;
                axios.get(photosUrl, {
                    headers:{
                        Authorization: 'Bearer '+window.accessToken.accessToken
                    }}).then((response)=>{
                    let data = response.data;
                    if (data.length > 0 )
                    {
                        for(let i = 0; i < data.length; i++)
                        {
                            let upload = {bytesSent: 12345};
                            let mockFile = {id:data[i].id, name: data[i].filename, primary_photo:data[i].primary_photo, size: 0, accepted: true};
                            mockFile.upload = upload;
                            mockFile.kind = "file";
                            thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                            thisDropzone.files.push(mockFile);
                            thisDropzone.options.thumbnail.call(thisDropzone, mockFile, data[i].thumbnail_src);
                            thisDropzone.options.success.call(thisDropzone, mockFile, data[i].id);
                            thisDropzone.options.complete.call(thisDropzone, mockFile);
                        }
                    }
                });
            },
            success: function(file, response)
            {

            },
            removedfile: function(file){
                let _ref;
                if (file.previewElement) {
                    if ((_ref = file.previewElement) != null) {
                        _ref.parentNode.removeChild(file.previewElement);
                    }
                }
                if(typeof file.id !== "undefined")
                    id = file.id;

                file.previewElement.remove();
                axios.delete("/api/photos/"+id+"", {
                    headers:{
                        Authorization: 'Bearer '+window.accessToken.accessToken
                    }});

                this.setupEventListeners();
                return this._updateMaxFilesReachedClass();
            }
        }
    }
}

const imageApp = Vue.createApp(Image);
imageApp.mount("#edit-image");

const Announcement = {
    data: function (){
        return {
            announcement: {
                body:""
            },
            saved: false,
            primary: "btn-primary",
            success:"btn-success",
        }
    },
    methods: {
        saveAnnouncement: function (url) {
            let vm = this;
            this.saved = true;
            axios.post(url, { body: this.announcement.body}, {
                headers:{
                    Authorization: 'Bearer '+window.accessToken.accessToken
                }}).then(function (){
                setTimeout(function (){
                    vm.saved = false;
                }, 3000);
            })
        },
    }
};
const announcementApp = Vue.createApp(Announcement);
announcementApp.mount("#edit-announcement");

import moment from 'moment';
import ModalComponent from 'vue-bootstrap4-modal'
let d = new Date();

const Post = {
    data: function (){
        return {
            subject: "",
            body: "",
            shared: {
                posts: [],
                draftPosts: []
            },
            post: {},
            showModal: true,
            published: false,
            published_at: moment().format()
        }
    },
    mounted() {
        let vm = this;
        let postsIndexUrl =  $("#posts").attr("data-group-posts-url");

        axios.get(postsIndexUrl, {
            headers:{
                Authorization: 'Bearer '+window.accessToken.accessToken
            }}).then(response => {

            vm.shared.posts = response.data.posts;
            vm.shared.draftPosts = response.data.drafts;
        });
    },
    methods: {
        openModel: function (post){
            this.post = post;
        },
        deletePost: function (post)
        {
        },
        editPost: function (post){

            let indexOfPosts = this.shared.posts.indexOf(this.post);

            if(indexOfPosts > -1)
            {
                this.shared.posts.splice(indexOfPosts, 1);
                this.shared.draftPosts.push(this.post);
            }


            axios.put("/api/posts/"+this.post.id+"", this.post, {
                headers:{
                    Authorization: 'Bearer '+window.accessToken.accessToken
                }});
        },
        publishSavedPost(post){
            this.post.published_at = moment().format();
            this.post.published_at_humans = moment().fromNow();

            let indexOfDraftPost = this.shared.draftPosts.indexOf(this.post);

            if(indexOfDraftPost > -1)
            {
                this.shared.draftPosts.splice(indexOfDraftPost, 1);
                this.shared.posts.push(this.post);
            }

            axios.put("/api/posts/"+this.post.id+"", this.post, {
                headers:{
                    Authorization: 'Bearer '+window.accessToken.accessToken
                }});
        },
        saveAsDraft: function (url){

            this.published = false;
            this.createPost(this.published, url);
        },
        publishPost: function (url){

            let vm  = this;
            this.published = true;
            this.createPost(this.published, url);

        },
        createPost(published, url)
        {
            let vm = this;
            if (published === false)
                this.published_at = null;

            axios.post(url, {
                subject: this.subject,
                body: this.body,
                published_at: this.published_at
            }, {
                headers:{
                    Authorization: 'Bearer '+window.accessToken.accessToken
                }}).then(function (response){

                if (vm.published)
                {
                    vm.shared.posts.push(response.data);
                }
                else{
                    vm.shared.draftPosts.push(response.data);
                }
            });
        }

    }
};

const postApp = Vue.createApp(Post);
postApp.mount("#posts");

const Member = {
    data: function ()
    {
        return {
            roles: [],
            role: 0,
            contacts: [],
            memberId:0
        }
    },
    mounted() {
        let vm = this;
        axios.get('/api/roles').then(response => {
            vm.roles = response.data;
        }, {
            headers:{
                Authorization: 'Bearer '+window.accessToken.accessToken
            }});
    },
    methods: {
        updateContact(url)
        {
            this.groupUserRoleUrl = url;
        },
        acceptUser(memberId, url){
            let vm = this;
            axios.put(url,{accepted:true}, {
                headers:{
                    Authorization: 'Bearer '+window.accessToken.accessToken
                }});

            $("#acceptUser"+memberId).addClass('d-none');
            $("#editUser"+memberId).removeClass('d-none');
        },
        removeUser(url, memberId){

            axios.delete(url, {
                headers:{
                    Authorization: 'Bearer '+window.accessToken.accessToken
                }});
            $("#member"+memberId).addClass('d-none');
        },
        updateRole: function()
        {
            axios.put(this.groupUserRoleUrl, {role_id:this.role, type:'updateRole'}, {
                headers:{
                    Authorization: 'Bearer '+window.accessToken.accessToken
                }});
        }
    }
}
const memberApp = Vue.createApp(Member);
memberApp.mount("#members");



