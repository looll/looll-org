let photos = new Vue({
    el:"#app",
    created: ()=>{

    },
    mounted: function () {
        this.$nextTick(function () {
            let vm = this;
            let photosUrl = $("#photos").attr("data-photos-url");
            let previewNode = document.querySelector("#previews");
            previewNode.id = "";
            let previewTemplate = previewNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);
            Dropzone.options.loollDropzone = {
                maxFiles: 5,
                maxFilesize: 3,
                thumbnailHeight: 200,
                thumbnailWidth: 200,
                previewTemplate: previewTemplate,
                addRemoveLinks: false,
                uploadMultiple: false,
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                init: function ()
                {
                    let thisDropzone = this;
                    axios.get(photosUrl).then((response)=>{
                        let data = response.data;
                        for(let i = 0; i < data.length; i++)
                        {
                            let upload = {bytesSent: 12345};
                            let mockFile = {id:data[i].id, name: data[i].filename, primary_photo:data[i].primary_photo, size: 0, accepted: true};
                            mockFile.upload = upload;
                            mockFile.kind = "file";
                            thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                            thisDropzone.files.push(mockFile);
                            thisDropzone.options.thumbnail.call(thisDropzone, mockFile, data[i].thumbnail_src);
                            thisDropzone.options.success.call(thisDropzone, mockFile, data[i].id);
                            thisDropzone.options.complete.call(thisDropzone, mockFile);
                        }
                    });

                    this.on("success", function(file, response) {
                        file.serverId = response.id;
                        console.log(response);
                        $(".dz-preview:last-child").attr('id', "document-" + file.serverId);
                    });

                },
                success: function(file, response)
                {
                    let dzPreview =  $(".dz-preview:last-child");
                    let id = 0;
                    let primaryPhoto = false;

                    typeof file.id === "undefined" ? id = response.id : id = file.id;
                    typeof file.primary_photo === "undefined" ? primaryPhoto = response.primary_photo : primaryPhoto = file.primary_photo;

                    dzPreview.children('.card-footer').children(".star").attr('id', id);
                    if(primaryPhoto === 1)
                    {
                        let star =  $("#"+id).children(".star2");
                        star.removeClass("fa-star-o");
                        star.addClass("fa-star");
                    }

                },
                removedfile: function(file) {
                    let _ref;
                    if (file.previewElement) {
                        if ((_ref = file.previewElement) != null) {
                            _ref.parentNode.removeChild(file.previewElement);
                        }
                    }

                    let dzPreview =  $(".dz-preview:last-child");
                    let id = dzPreview.children('.closebutton').children(".star").attr('id');

                    if(typeof file.id !== "undefined")
                        id = file.id;

                    file.previewElement.remove();
                    axios.delete("/api/photos/"+id+"");

                    this.setupEventListeners();
                    return this._updateMaxFilesReachedClass();
                }
            };

            $(document).on('click', '.star', function(e)
            {
                //$(e).preventDefault();

                let star = $(".star2");
                let id = $(this).attr("id");
                star.removeClass("fa-star");
                star.addClass("fa-star-o");

                $(this).children(".star2").removeClass("fa-star-o");
                $(this).children(".star2").addClass("fa-star")

                axios.put("/api/photos/"+id).then((response)=>{});

            });
        });
    }
});