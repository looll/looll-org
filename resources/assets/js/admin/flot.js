// CHART SPLINE
// -----------------------------------
(function(window, document, $, undefined)
{
    $(function()
    {
        var route = $('.chart-spline').attr("data-url");
        var options = {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true,
                    radius: 4
                },
                splines: {
                    show: true,
                    tension: 0.4,
                    lineWidth: 1,
                    fill: 0.5
                }
            },
            grid: {
                borderColor: '#eee',
                borderWidth: 1,
                hoverable: true,
                backgroundColor: '#fcfcfc'
            },
            tooltip: true,
            tooltipOpts: {
                content: function (label, x, y) { return x + ' : ' + y; }
            },
            xaxis: {
                tickColor: '#fcfcfc',
                mode: 'categories'
            },
            yaxis: {
                min: 0,
                max: 150, // optional: use it for a clear represetation
                tickColor: '#eee',
                //position: 'right' or 'left',
                tickFormatter: function (v) {
                    return v/* + ' visitors'*/;
                }
            },
            shadowSize: 0
        };
        var d = [
            []
        ];
        var values = [];
        var chart = $('.chart-spline');

        $.ajax({
            url: route,
            dataType: "json",
            success: function(data)
            {
                var date = null;
                var month = "";
                for(var i = 0; data.length >= i; i++)
                {
                    if(typeof data[i] != 'undefined')
                    {
                        date = new Date(data[i].date.date);
                        month = moment(date).format("MMM");
                        // date = new Date(data[i].date.date);
                        d.push([date.getDate()+"."+month, data[i].visitors]);
                        // = ["Jan", 20];
                    }
                }
                values = [{
                    "label": "Total visitors",
                    "color": "#768294",
                    "data": d
                }];

                if(chart.length)
                    $.plot(chart, values, options);
            }
        });



    });
})(window, document, window.jQuery);;