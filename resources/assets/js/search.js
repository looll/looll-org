 $(document).ready(function(){
    $('#autosuggest').keyup(function(){
        var the_search_term = $(this).val();
        $.post('/searchSuggestions', {query:the_search_term}, function(data){
            $('.result').html(data);
        });
    });
});