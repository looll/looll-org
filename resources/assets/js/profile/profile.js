let profile = new Vue({
    el:"#edit-profile",
    data:{
        saveProfileText:"Save",
        saved:false,
        profile:{
            name:"",
            address:"",
            zip:"",
            city:"",
            country_id:0,
            email:"",
            birthday:"",
            description:"",
            gender:"",
            marital_status_id:0,
            workplace:"",
            position:""
        },
        business:{
            id:0,
            phone_type:"business",
            phone_number:""
        },
        home:{
            id:0,
            phone_type:"home",
            phone_number:""
        },
        mobile:{
            id:0,
            phone_type:"mobile",
            phone_number:""
        },
        birthday:"",

    },
    mounted: function (){
        this.$nextTick(require('./methods/ready.js'));
    },
    methods:
    {
        saveProfile: require('./methods/saveProfile')
    },
    watch:{
        birthday: function (val) {
            this.profile.birthday = val;
        },
    }
});