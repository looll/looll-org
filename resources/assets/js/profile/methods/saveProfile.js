module.exports = function(url, published)
{
    let vm = this;
    if(published === 0)
    {
        this.saved = true;
        setTimeout(function () {
            vm.saved = false;
            vm.saveProfileText = "Save";
        }, 3000);

    }
    else
    {
        this.publishedProfile = true;

        setTimeout(function () {
            vm.publishedProfile = false;
        }, 3000);
    }

    axios.put(url, {"profile":this.profile, "published":published}, {
        headers:{
            Authorization: 'Bearer '+window.accessToken.accessToken
        }});
};

