module.exports = function (url)
{
    let vm = this;

    //et announcementId = this.announcement.id;
    let data = {
        body:vm.announcement.body
    };

    axios.post(url, data, {
        headers:{
            Authorization: 'Bearer '+window.accessToken.accessToken
        }}).then((response)=>{
        //vm.announcement.id = response.body.id;
        vm.saved = true;
        vm.hasErrors = false;
        setTimeout(function(){
            vm.saved = false;
        }, 3000);
    });

    /*let vm = this;

    let announcementId = this.announcement.id;
    vm.announcement.body = CKEDITOR.instances["announcement"].getData();

    let data = {
        body:vm.announcement.body
    };

    if(announcementId > 0)
    {
        vm.$http.put('/api/profiles/'+this.profile_id+'/announcements/'+this.announcement.id, data).then((response)=>{
            vm.saved = true;
            vm.hasErrors = false;
            setTimeout(function(){
                vm.saved = false;
            }, 3000);
        }, (response)=>{
            vm.hasErrors = true;
            vm.errors = response.body;
        });
    }
    else
    {
        vm.$http.post(url, data).then((response)=>{
            vm.announcement.id = response.body.id;
            vm.saved = true;
            vm.hasErrors = false;
            setTimeout(function(){
                vm.saved = false;
            }, 3000);
        }, (response)=>{
            vm.hasErrors = true;
            vm.errors = response.body;
        });
    }
    //vm.$http*/
};