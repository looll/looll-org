const Home = {
    data() {
        return {
            counter: 0,
            profile:{
                name:"",
                email:"",
                birthday:"",
                gender: "",
                address: "",
                city: "",
                zip:"",
                country:0,
                phone_number:"",
                workplace:"",
                business_phone_number:"",
                announcement: {
                    body:""
                }
            },
            publishedProfile:false,
            saved:false,
            published:0,
            saveProfileText:"Save"
        }
    },
    mounted: function () {
        this.$nextTick(function () {
            let vm = this;
            let url  = $('#home').attr('data-profile-url');

            axios.get(url, {
                headers:{
                    Authorization: 'Bearer '+window.accessToken.accessToken
                }}).then(result=>{
                vm.profile = result.data.profile;
            });
        });
    },
    methods: {
        saveProfile: require("./methods/saveProfile")
    }
}
Vue.createApp(Home).mount('#home');
const Image = {
    data() {
        return {
            counter: 0,
            dropzone: Dropzone
        }
    },
    mounted: function () {
        let vm = this;
        let photosUrl = $("#photos").attr("data-photos-url");
        Dropzone.Dropzone.options.loollDropzone = {
            maxFiles: 1,
            thumbnailHeight:200,
            thumbnailWidth:200,
            addRemoveLinks:true,
            uploadMultiple:false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Authorization': 'Bearer '+window.accessToken.accessToken
            },
            init: function (){
               let thisDropzone = this;
               axios.get(photosUrl, {
                   headers:{
                       Authorization: 'Bearer '+window.accessToken.accessToken
                   }}).then((response)=>{
                  console.log(response.data);
                  let data = response.data;
                  let dataLength = data.length;
                  let upload = {bytesSent: 12345};
                  let mockFile;

                  if(dataLength > 0)
                  {
                      for(let i = 0; i < dataLength; i++)
                      {
                          mockFile = {id:data[i].id, name: data[i].filename, primary_photo:data[i].primary_photo, size: 0, accepted: true};
                          mockFile.upload = {bytesSent: 12345};
                          mockFile.kind = "file";

                          thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                          thisDropzone.files.push(mockFile);
                          thisDropzone.options.thumbnail.call(thisDropzone, mockFile, data[i].thumbnail_src);
                          thisDropzone.options.success.call(thisDropzone, mockFile, data[i].id);
                          thisDropzone.options.complete.call(thisDropzone, mockFile);
                      }
                  }


                  /* let data = response.data;



                   for(let i = 0; i < data.length; i++)
                   {
                       let upload = {bytesSent: 12345};
                       let mockFile = {id:data[i].id, name: data[i].filename, primary_photo:data[i].primary_photo, size: 0, accepted: true};
                       mockFile.upload = upload;
                       mockFile.kind = "file";
                       thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                       thisDropzone.files.push(mockFile);
                       thisDropzone.options.thumbnail.call(thisDropzone, mockFile, data[i].thumbnail_src);
                       thisDropzone.options.success.call(thisDropzone, mockFile, data[i].id);
                       thisDropzone.options.complete.call(thisDropzone, mockFile);
                   }*/
               });
           },
            removedfile: function(file){
                let _ref;
                if (file.previewElement) {
                    if ((_ref = file.previewElement) != null) {
                        _ref.parentNode.removeChild(file.previewElement);
                    }
                }

                let dzPreview =  $(".dz-preview:last-child");
                let id = dzPreview.children('.closebutton').children(".star").attr('id');

                if(typeof file.id !== "undefined")
                    id = file.id;

                file.previewElement.remove();
                axios.delete("/api/photos/"+id+"", {
                    headers:{
                        Authorization: 'Bearer '+window.accessToken.accessToken
                    }});

                this.setupEventListeners();
                return this._updateMaxFilesReachedClass();
            }
       };
    }
}
Vue.createApp(Image).mount('#image');

const Announcement = {
    data: function (){
        return {
            announcement:{
                body:"",
                id:0,
                public:true
            },
            errors:{
                body:[],
            },
            hasErrors:false,
            profile_id:0,
            saved:false,
            primary:"btn-primary",
            success:"btn-success"
        }
    },
    created:function (){
        let vm = this;
        let messages = $("#announcement");
        //let accessToken = window.accessToken.accessToken;
        // if(typeof accessToken !== "undefined")
        //     window.axios.defaults.headers.common['Authorization'] = 'Bearer '+accessToken;

        let announcementUrl = messages.attr('data-announcement-url');
        vm.profile_id = messages.attr('data-profile-id');
        axios.get(announcementUrl,{
            headers:{
                Authorization: 'Bearer '+window.accessToken.accessToken
            }}).then((response)=>
        {
            console.log(response.data);
            if (response.data != null)
            {
                vm.announcement.id = response.data.id;
                vm.announcement.body = response.data.body;
            }
        });
    },
    methods:{
        saveAnnouncement:require('./methods/announcement/saveAnnouncement.js')
    }
}

Vue.createApp(Announcement).mount("#announcement");