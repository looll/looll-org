let announcment = new Vue({
    el:"#messages",
    data:{
        announcement:{
            body:"",
            id:0,
            public:true
        },
        errors:{
            body:[],
        },
        hasErrors:false,
        profile_id:0,
        saved:false,
        primary:"btn-primary",
        success:"btn-success"
    },
    created:function ()
    {
        let vm = this;
        let messages = $('#messages');
        let announcementUrl = messages.attr('data-announcement-url');
        vm.profile_id = messages.attr('data-profile-id');

        vm.$http.get(announcementUrl).then((response)=>
        {
            vm.announcement.id = response.body.id;
            vm.announcement.body = response.body.body;
        });
    },
    mounted: function ()
    {
        this.$nextTick(function ()
        {
            let vm = this;
            CKEDITOR.replace('announcement');
            setTimeout(function(){
                CKEDITOR.instances["announcement"].setData(vm.announcement.body);
            }, 2000);
        });
    },
    methods:{
        saveAnnouncement:require('./methods/announcement/saveAnnouncement.js')
    }
});