let Image = new Vue({
    el:"#edit-image",
    data:{},
    mounted: function ()
    {
        this.$nextTick(function () {
            let vm = this;
            let photosUrl = $("#photos").attr("data-photos-url");
            let previewNode = document.querySelector("#previews");
            previewNode.id = "";
            let previewTemplate = previewNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);

            Dropzone.options.loollDropzone = {
                maxFiles:5,
                maxFilesize: 3, // MB
                thumbnailHeight:200,
                thumbnailWidth:200,
                previewTemplate: previewTemplate,
                addRemoveLinks:false,
                uploadMultiple:false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                init: function ()
                {
                    let thisDropzone = this;
                    vm.$http.get(photosUrl).then((response)=>{
                        let data = response.body;
                        for(let i = 0; i < data.length; i++)
                        {
                            let upload = {bytesSent: 12345};
                            let mockFile = {id:data[i].id, name: data[i].filename, primary_photo:data[i].primary_photo, size: 0, accepted: true};

                            mockFile.upload = upload;
                            mockFile.kind = "file";
                            thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                            thisDropzone.files.push(mockFile);
                            thisDropzone.options.thumbnail.call(thisDropzone, mockFile, data[i].thumbnail_src);
                            thisDropzone.options.success.call(thisDropzone, mockFile, data[i].id);
                            thisDropzone.options.complete.call(thisDropzone, mockFile);
                        }
                    });
                },
                accept: function (file, done)
                {
                    if ((file.type).toLowerCase() != "image/jpg" &&
                        (file.type).toLowerCase() != "image/gif" &&
                        (file.type).toLowerCase() != "image/jpeg" &&
                        (file.type).toLowerCase() != "image/png"
                    ) {
                        done("Invalid file");
                    }
                    else {
                        done();
                    }
                },
                success: function(file, response)
                {
                    let dzPreview =  $(".dz-preview:last-child");


                    let id = 0;
                    let primaryPhoto = false;

                    typeof file.id === "undefined" ? id = response.id : id = file.id;
                    typeof file.primary_photo === "undefined" ? primaryPhoto = response.primary_photo : primaryPhoto = file.primary_photo;

                    dzPreview.children('.closebutton').children(".star").attr('id', id);

                    if(primaryPhoto === true)
                    {
                        let star =  $("#"+id);
                        star.removeClass("fa-star-o");
                        star.addClass("fa-star");
                    }

                    //dzPreview.children('.closebutton').children(".star").
                    //file.id = response.id;
                },
                removedfile:function(file)
                {
                    let _ref;
                    if (file.previewElement) {
                        if ((_ref = file.previewElement) != null) {
                            _ref.parentNode.removeChild(file.previewElement);
                        }
                    }

                    let dzPreview =  $(".dz-preview:last-child");
                    let id = dzPreview.children('.closebutton').children(".star").attr('id');

                    if(typeof file.id !== "undefined")
                        id = file.id;

                    vm.$http.delete("/api/photos/"+id+"");

                    this.setupEventListeners();
                    return this._updateMaxFilesReachedClass();
                }
            };

            $(document).on('click', '.star', function(e)
            {
                e.preventDefault();
                let star = $(".star");
                let id = $(this).attr("id");
                star.removeClass("fa-star");
                star.addClass("fa-star-o");
                $(this).removeClass("fa-star-o");
                $(this).addClass("fa-star");

                vm.$http.put("/api/photos/"+id).then((response)=>{
                });
            });
        });
    },
    methods:{
    }
});


/*
* ready:function()
 {
 var vm = this;
 var photosUrl = $("#photos").attr("data-photos-url");
 var previewNode = document.querySelector("#previews");
 previewNode.id = "";
 var previewTemplate = previewNode.innerHTML;
 previewNode.parentNode.removeChild(previewNode);

 Dropzone.options.loollDropzone = {
 maxFiles:5,
 maxFilesize: 3, // MB
 thumbnailHeight:200,
 thumbnailWidth:200,
 previewTemplate: previewTemplate,
 addRemoveLinks:false,
 uploadMultiple:false,
 init: function ()
 {
 var thisDropzone = this;
 vm.$http.get(photosUrl).then(function (resource) {
 var data = resource.data;
 for(var i = 0; i < data.length; i++)
 {
 var upload = {bytesSent: 12345};
 var mockFile = {id:data[i].id, name: data[i].filename, primary_photo:data[i].primary_photo, size: 0, accepted: true};

 mockFile.upload = upload;
 mockFile.kind = "file";
 thisDropzone.options.addedfile.call(thisDropzone, mockFile);
 thisDropzone.files.push(mockFile);
 thisDropzone.options.thumbnail.call(thisDropzone, mockFile, data[i].thumbnail_src);
 thisDropzone.options.success.call(thisDropzone, mockFile, data[i].id);
 thisDropzone.options.complete.call(thisDropzone, mockFile);
 }
 });

 /* $.getJSON(photosUrl, function(data)
 {
 for(var i = 0; i < data.length; i++)
 {
 var upload = {bytesSent: 12345};
 var mockFile = {id:data[i].id, name: data[i].filename, primary_photo:data[i].primary_photo, size: 0, accepted: true};

 mockFile.upload = upload;
 mockFile.kind = "file";
 thisDropzone.options.addedfile.call(thisDropzone, mockFile);
 thisDropzone.files.push(mockFile);
 thisDropzone.options.thumbnail.call(thisDropzone, mockFile, data[i].thumbnail_src);
 thisDropzone.options.success.call(thisDropzone, mockFile, data[i].id);
 thisDropzone.options.complete.call(thisDropzone, mockFile);
 }
 });

},
accept: function (file, done)
{
    if ((file.type).toLowerCase() != "image/jpg" &&
        (file.type).toLowerCase() != "image/gif" &&
        (file.type).toLowerCase() != "image/jpeg" &&
        (file.type).toLowerCase() != "image/png"
    ) {
        done("Invalid file");
    }
    else {
        done();
    }
},
success: function(file, response)
{
    var dzPreview =  $(".dz-preview:last-child");
    dzPreview.children('.closebutton').children(".star").attr('id', file.id);

    if(file.primary_photo === true || file.primary_photo === 1)
    {
        var star =  $("#"+file.id);
        star.removeClass("fa-star-o");
        star.addClass("fa-star");
    }

    //dzPreview.children('.closebutton').children(".star").
    //file.id = response.id;
},
removedfile:function(file)
{
    var _ref;
    if (file.previewElement) {
        if ((_ref = file.previewElement) != null) {
            _ref.parentNode.removeChild(file.previewElement);
        }
    }
    var token = $('#photos').attr('data-api-token');


    $.ajax("/api/photos/"+file.id,  {
        type:"DELETE",
        data:{
            api_token:token
        },
        headers:{
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    this.setupEventListeners();
    return this._updateMaxFilesReachedClass();
}
};

$(document).on('click', '.star', function()
{
    var star = $(".star");
    var id = $(this).attr("id");
    star.removeClass("fa-star");
    star.addClass("fa-star-o");

    $(this).removeClass("fa-star-o");
    $(this).addClass("fa-star");

    var token = $('#photos').attr('data-api-token');
    $.ajax("/api/photos/"+id,{
        type:"PUT",
        data:{
            api_token:token
        },
        headers:{
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});
},
*
* */