var removedUrl = "";
var photosUrl = $("#photos").attr("data-photos-url");
/*var previewNode = document.querySelector("#template");
previewNode.id = "";
var previewTemplate = previewNode.parentNode.innerHTML;
previewNode.parentNode.removeChild(previewNode);*/

var previewTemplate = $("#previews").html();

Dropzone.options.loollDropzone = {
    maxFiles:5,
    maxFilesize: 1, // MB
    thumbnailHeight:150,
    thumbnailWidth:200,
    previewTemplate: previewTemplate,
    //previewsContainer: "#previews", // Define the container to display the previews
    accept: function (file, done) {
        if ((file.type).toLowerCase() != "image/jpg" &&
            (file.type).toLowerCase() != "image/gif" &&
            (file.type).toLowerCase() != "image/jpeg" &&
            (file.type).toLowerCase() != "image/png"
        ) {
            done("Invalid file");
        }
        else {
            done();
        }
    },
    init: function () {
        var thisDropzone = this;
        $.getJSON(photosUrl, function(data)
        {
            for(var i = 0; i < data.length; i++)
            {
                var upload = {bytesSent: 12345};
                var mockFile = {id:data[i].id, name: data[i].filename, size: 0, accepted: true};
                mockFile.upload = upload;
                mockFile.kind = "file";
                thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                thisDropzone.files.push(mockFile);
                thisDropzone.options.thumbnail.call(thisDropzone, mockFile, data[i].thumbnail_src);
                thisDropzone.options.success.call(thisDropzone, mockFile, data[i].id);
                thisDropzone.options.complete.call(thisDropzone, mockFile);
            }
        });




              /* var mockFile = { name: "myimage.jpg", size: 12345, type: 'image/jpeg' };
        this.addFile.call(this, mockFile);
        this.options.thumbnail.call(this, mockFile, "http://someserver.com/myimage.jpg");*/
    },
    addRemoveLinks:false,
    uploadMultiple:false,
    success: function(file, response)
    {
        //file.id = response.id;
    },
    removedfile:function(file)
    {
        var _ref;
        if (file.previewElement) {
            if ((_ref = file.previewElement) != null) {
                _ref.parentNode.removeChild(file.previewElement);
            }
        }
        console.log(file);

        $.ajax("/photo/"+file.id,  {
            type:"DELETE",
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        this.setupEventListeners();
        return this._updateMaxFilesReachedClass();

    },
    maxfilesreached:function(file)
    {
        this.removeEventListeners();
    },
    maxfilesexceeded:function()
    {
        console.log("hello how are you");
    }
};