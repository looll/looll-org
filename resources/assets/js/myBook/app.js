let categoryLists = [];

const myBook = {
    data: function (){
        return {
            categoryLists:categoryLists,
            active:'active',
            current:0,
            listGroupItem:"",
            selectCategories:[],
            name:"",
            parentId:0,
            myBookId:0,
        }
    },
    methods:{
        truncate(value){
            let string = '15';
            if(value.length > 15)
                return value.substring(0, 15) + ' ...';
            return value;
        },
        selectCategory:require('./methods/selectCategory.js'),
        deleteCategory:require('./methods/deleteCategory'),
        saveCategory:require('./methods/saveCategory.js'),
        addToCategory:require('./methods/addToCategory'),
        checkForCategory: function(categories, category)
        {
            for(let i = 0; i < categories.length; i++)
            {
                if(categories[i].id === category.id )
                {
                    return true;
                }
            }
            return false;
        },
        removeItem: function(list, key) {
            this.$delete(list, key);
        }
    },
    created: function()
    {
        let myBook = $("#myBook");
        let url = myBook.attr("data-category-index");
        let vm = this;
        vm.myBookId = myBook.attr("data-mybook-id");
        axios.get(url).then((result)=>{
            let categories = result.data;
            /*
            let categories = result.data;
            categories.map(category => {
                category.profiles.map(profile => {
                    profile.selected = [];

                    profile.categories.map(category2 => {
                        console.log(category);
                        profile.selected.push(category2.id);
                        delete category2.pivot;
                    })
                });

                vm.categoryLists.push(category);
                vm.categoryLists.current = false;

                if(category.name === "All")
                {
                    vm.parentId = category.id;
                }

            });
            */

            for(let i = 0; i < categories.length; i++)
            {
                categoryLists.push(categories[i]);
                categoryLists.current = false;

                for(let ii = 0; ii < categoryLists[i].profiles.length; ii++)
                {
                    categoryLists[i].profiles[ii].selected = [];
                    //delete vm.categoryLists[i].profiles[ii].pivot;
                    for(let iii = 0; iii < categoryLists[i].profiles[ii].categories.length; iii++ )
                    {
                        categoryLists[i].profiles[ii].selected.push(""+vm.categoryLists[i].profiles[ii].categories[iii].id+"");
                        //delete categoryLists[i].profiles[ii].categories[iii].pivot;
                    }
                }

                if(categoryLists[i].name === "All")
                {
                    vm.parentId = categoryLists[i].id;
                }
            }

            vm.current = categoryLists[0].id;
            vm.current = vm.categoryLists[0].id;
        });
        //axios.get();
    },
    mounted: function()
    {
        this.$nextTick(function () {
        });
    }
};

Vue.createApp(myBook).mount("#myBook");