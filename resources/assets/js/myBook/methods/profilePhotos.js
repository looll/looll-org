/**
 * Created by jondijam on 30.4.2016.
 */
module.exports = function(photos)
{
    if(photos.length > 0)
    {
        return photos[0].src;
    }

    return '/img/no-image.jpg';
};