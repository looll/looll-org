/**
 * Created by jondijam on 14.4.2016.
 */
module.exports = function(category)
{
    let vm = this;
    let c = confirm("Are you sure you want to delete!");



    if(c === true)
    {
        axios.delete("/api/categories/"+category.id).then(result => {
            const id = vm.categoryLists.indexOf(category);
            vm.categoryLists.splice(id, 1);
        });
        //location.reload();
    }

};