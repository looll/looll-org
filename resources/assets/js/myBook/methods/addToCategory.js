module.exports = function(category, category_item, profile, url, boolean)
{
    let vm = this;
    let indexOfCategory = 0;
    let profileIndex = 0;
    let count = 0;
    let a;

    for(let i = 0; i < vm.categoryLists.length; i++)
    {
        if(vm.categoryLists[i].id === category_item.id)
        {
            if(boolean === true)
            {
                let categoryIndex = profile.categories.indexOf(category_item);
                profile.categories.splice(categoryIndex, 1);

                for(let ii = 0; ii < vm.categoryLists[i].profiles.length; ii++)
                {
                    if(vm.categoryLists[i].profiles[ii].id === profile.id)
                    {
                        vm.categoryLists[i].profiles.splice(ii, 1);
                    }

                }

            }

            if(boolean === false)
            {
                profile.categories.push(category_item);
                vm.categoryLists[i].profiles.push(profile);
            }
        }
        else if(vm.categoryLists[i].id !== category_item.id && vm.categoryLists[i].id != category.id)
        {
            for(let ii = 0; ii < vm.categoryLists[i].profiles.length; ii++)
            {
                if(vm.categoryLists[i].profiles[ii].id === profile.id)
                {
                    vm.categoryLists[i].profiles[ii].selected = profile.selected;
                    vm.categoryLists[i].profiles[ii].categories = profile.categories;
                    console.log(boolean);


                }

            }
        }
    }

    if(boolean === false)
    {
        axios.put("/api/profiles/"+profile.id+"/categories/"+category_item.id).then(function () {

        });

        /*vm.$http.put("/api/profiles/"+profile.id+"/categories/"+category_item.id, {}).then(function(response)
        {

        });*/
    }

    if(boolean === true)
    {
        axios.delete("/api/profiles/"+profile.id+"/categories/"+category_item.id, {});
        //vm.$http.delete("/api/profiles/"+profile.id+"/categories/"+category_item.id, {});
    }

};