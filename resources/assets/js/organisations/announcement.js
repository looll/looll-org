new Vue({
    el:"#edit-announcement",
    data:{
        announcement:{
            id:0,
            body:"",
        },
        group_id:0,
        saved:false,
        primary:"btn-primary",
        success:"btn-success"
    },
    created:function ()
    {
        let vm = this;
        let editAnnouncement = $('#edit-announcement');
        let url = editAnnouncement.attr('data-announcement-url');
        this.$http.get(url).then(function (response)
        {
            vm.announcement.id = response.body.id;
            vm.announcement.body = response.body.body;
        });

        this.group_id = editAnnouncement.attr('data-group-id');
    },
    mounted: function ()
    {
        this.$nextTick(function ()
        {
            let vm = this;
            CKEDITOR.replace('announcement');

            setTimeout(function(){
                CKEDITOR.instances["announcement"].setData(vm.announcement.body);
            }, 2000);
        });
    },
    methods: {
        onSubmit:function (url)
        {
            let vm = this;
            vm.announcement.body = CKEDITOR.instances["announcement"].getData();

            if(this.announcement.id > 0)
            {
                this.$http.put('/api/groups/'+this.group_id+'/announcements/'+this.announcement.id, {
                    body:vm.announcement.body
                }).then(function (response) {
                    //vm.announcement.body = response.body.body;
                    vm.saved = true;
                    setTimeout(function(){
                        vm.saved = false;
                    }, 3000);

                });
            }
            else
            {
                this.$http.post(url, { body:this.announcement.body }).then(function (response)
                {
                    //vm.announcement.body = response.body.body;
                    vm.announcement.id = response.body.id;
                    vm.saved = true;
                    setTimeout(function(){
                        vm.saved = false;
                    }, 3000);
                });
            }
        }
    }
});