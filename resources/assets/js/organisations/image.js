new Vue({
    el:'#edit-image',
    created:function () {
    },
    mounted: function ()
    {
        this.$nextTick(function ()
        {
            let vm = this;
            let photosUrl = $("#photos").attr("data-photos-url");
            let previewNode = document.querySelector("#previews");
            previewNode.id = "";
            let previewTemplate = previewNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);

            Dropzone.options.loollDropzone = {
                maxFiles:5,
                maxFilesize: 3, // MB
                thumbnailHeight:200,
                thumbnailWidth:200,
                previewTemplate: previewTemplate,
                addRemoveLinks:false,
                uploadMultiple:false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                init: function () {
                    const thisDropzone = this;
                    vm.$http.get(photosUrl).then(function (resource)
                    {
                        let data = resource.body;
                        for(let i = 0; i < data.length; i++)
                        {
                            let upload = {bytesSent: 12345};
                            let mockFile = {id:data[i].id, name: data[i].filename, primary_photo:data[i].primary_photo, size: 0, accepted: true};

                            mockFile.upload = upload;
                            mockFile.kind = "file";
                            thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                            thisDropzone.files.push(mockFile);
                            thisDropzone.options.thumbnail.call(thisDropzone, mockFile, data[i].thumbnail_src);
                            thisDropzone.options.success.call(thisDropzone, mockFile, data[i].id);
                            thisDropzone.options.complete.call(thisDropzone, mockFile);
                        }
                    });
                },
                accept: function (file, done)
                {
                    if ((file.type).toLowerCase() != "image/jpg" &&
                        (file.type).toLowerCase() != "image/gif" &&
                        (file.type).toLowerCase() != "image/jpeg" &&
                        (file.type).toLowerCase() != "image/png"
                    ) {
                        done("Invalid file");
                    }
                    else {
                        done();
                    }
                },
                success: function(file, response)
                {
                    let dzPreview =  $(".dz-preview:last-child");
                    dzPreview.children('.closebutton').children(".star").attr('id', file.id);

                    if(file.primary_photo === true || file.primary_photo === 1)
                    {
                        let star =  $("#"+file.id);
                        star.removeClass("fa-star-o");
                        star.addClass("fa-star");
                    }
                },
                removedfile:function(file)
                {
                    let _ref;
                    if (file.previewElement) {
                        if ((_ref = file.previewElement) != null) {
                            _ref.parentNode.removeChild(file.previewElement);
                        }
                    }

                    let dzPreview =  $(".dz-preview:last-child");
                    let id = dzPreview.children('.closebutton').children(".star").attr('id');

                    if(typeof file.id !== "undefined")
                        id = file.id;

                    vm.$http.delete("/api/photos/"+id+"");

                    this.setupEventListeners();
                    return this._updateMaxFilesReachedClass();
                }
            };

            $(document).on('click', '.star', function(e)
            {
                e.preventDefault();
                let star = $(".star");
                let id = $(this).attr("id");
                star.removeClass("fa-star");
                star.addClass("fa-star-o");
                $(this).removeClass("fa-star-o");
                $(this).addClass("fa-star");

                vm.$http.put("/api/photos/"+id).then((response)=>{
                });
            });

        });

    }
});
