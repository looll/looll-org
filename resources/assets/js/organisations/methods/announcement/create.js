/**
 * Created by jondijam on 2.1.2016.
 */
module.exports = function(url)
{
    var vm = this;
    vm.body = CKEDITOR.instances.announcement.getData();

   vm.$http.post(url, {
        _token:vm.token,
        subject:vm.subject,
        public:vm.public,
        body:vm.body,
        publish_at:vm.publish_at
    }, function(data)
   {
       var source   = $("#private-announcement").html();
       var template = Handlebars.compile(source);
       var context = {id:data.id, body:vm.body, subject:vm.subject };
       var html    = template(context);

       $(".private_announcements").prepend(html);
   });
};