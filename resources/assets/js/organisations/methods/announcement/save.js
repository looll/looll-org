module.exports = function (token)
{
    var vm = this;
    var id = vm.announcementPublicId;
    var url = vm.announcementPublicUrl;

    vm.body = CKEDITOR.instances.announcement_public.getData();

    var today = new Date();
    today.toISOString().substring(0, 10);

    if(id === 0)
    {
        vm.$http.post(url, {
            public:true,
            _token:token,
            body:vm.body,
            subject:null,
            publish_at:today
        }).then( function (response)
        {
            vm.announcementPublicUrl = response.url;
        });
    }
    else if(id > 0)
    {
        vm.$http.put(url, {_token:token, body:body, subject:null});
    }
};