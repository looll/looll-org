module.exports = function ()
{
    let vm = this;
    let url = $('#home').attr('data-api-url');

    this.$http.get(url).then((response) =>
    {
        let data = response.body;

        vm.name = data.name;
        vm.description = data.description;
        vm.address = data.address;
        vm.email = data.email;
        vm.country_id = data.country.id;
        vm.member_type_id = data.member_type.id;
        vm.zip = data.zip;
        vm.city = data.city;
        // success callback
    }, (response) => {
        // error callback
    });
};