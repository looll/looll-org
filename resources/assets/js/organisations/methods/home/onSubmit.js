module.exports = function (url)
{
    let vm = this;
    vm.description =  CKEDITOR.instances["description"].getData();

    vm.$http.put(url, {
        name:vm.name,
        email:vm.email,
        description:vm.description,
        address:vm.address,
        city:vm.city,
        zip:vm.zip,
        country_id: vm.country_id,
        member_type_id: vm.member_type_id
    }).then((response) => {
        vm.saved = true;

        setTimeout(function(){
            vm.saved = false;
        }, 3000);
    }, (response)=>{

    });



};