/**
 * Created by jondijam on 14.12.2016.
 */
module.exports = function (url) {

    let vm = this;
    vm.$http.post(url, {
        draft:true,
        subject:vm.subject,
        body:vm.body,
        published_at:null
    }).then(function (response)
    {

        vm.shared.draftPosts.push(response.body);
    }, function ()
    {

    });
};