module.exports = function (url)
{
    let vm = this;
    console.log(vm.published_at);
    vm.$http.post(url, {
        draft:false,
        publish:true,
        subject:vm.subject,
        body:vm.body,
        published_at:vm.published_at
    }).then(function (response)
    {
        vm.shared.posts.push(response.body);

    }, function ()
    {

    });
};