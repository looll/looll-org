/**
 * Created by jondijam on 17.1.2016.
 */

module.exports = function (userId, groupId, url, token)
{
    var vm = this;
    vm.$http.post(url, {_token:token, userId:userId});

    $("#connect").hide();
};