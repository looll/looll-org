/**
 * Created by jondijam on 17.1.2016.
 */
module.exports = function(userId, url, token)
{
    var vm = this;
    vm.$http.put(url,{accepted:false, _token:token});
    $("#acceptUser"+userId).removeClass('hidden');
    $("#declineUser"+userId).addClass('hidden');

};