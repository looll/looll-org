/**
 * Created by jondijam on 29.12.2016.
 */
module.exports = function (url, userId) {

    let vm = this;
    let role = "admin";
    for(let i = 0; i < vm.contacts.length; i++)
    {
        if(vm.contacts[i] == userId)
        {
            role = "member";
        }
    }

    this.$http.put(url, { role:role, accepted:true});

};
