/**
 * Created by jondijam on 17.1.2016.
 */
module.exports = function(userId, url, token)
{
    let vm = this;
    vm.$http.put(url,{accepted:true, _token:token});

    $("#acceptUser"+userId).addClass('hidden');
    $("#declineUser"+userId).removeClass('hidden');
};