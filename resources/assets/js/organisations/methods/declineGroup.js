/**
 * Created by jondijam on 16.1.2016.
 */
module.exports = function(id, url, token)
{
    var vm = this;

    vm.$http.put(url,{'accepted_by_parent':false, '_token':token});
    $('#acceptGroup'+id).removeClass('hidden');
    $('#declineGroup'+id).addClass('hidden');
};