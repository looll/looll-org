/**
 * Created by jondijam on 2.1.2016.
 */
module.exports = function()
{
    var vm = this;
    $('#publish_at').datepicker({dateFormat: "yy-mm-dd"});

    CKEDITOR.replace('description');

    CKEDITOR.replace('announcement');
    CKEDITOR.instances['announcement'].on('key', function () {
        var data = CKEDITOR.instances.announcement.getData();
        vm.announcement.body = data;
    });

    /*if($("#announcement").length > 0)
    {
        CKEDITOR.replace('announcement');
        CKEDITOR.instances['announcement'].on('key', function () {
            var data = CKEDITOR.instances.announcement.getData();
            vm.body = data;
        });
    }

    if($('#announcement_public').length > 0)
    {
        CKEDITOR.replace('announcement_public');
        CKEDITOR.instances['announcement_public'].on('key', function () {
            var data = CKEDITOR.instances.announcement.getData();
            vm.body = data;
        });
    }*/
};