let member = new Vue({
    el:'#members',
    data:{
        membersOnlyGroups:0,
        pendingGroups:[],
        invitationShow:true,
        contacts:[]
    },
    created:function () {
        let vm = this;
            let members = $('#members');
            let groupId = members.attr('data-group-id');
            let url = members.attr('data-group-users-url');

            vm.$http.get(url).then((resource)=>{
                for(let ii = 0; ii < resource.body.length; ii++)
                {
                    vm.contacts.push(""+resource.body[ii]+"");
                }
            });
    },
    mounted: function ()
    {
        this.$nextTick(function ()
        {
            console.log(this.contacts);



            //$('#invite-group').select2();
        });
    },
    methods:{
        inviteGroups:require('./methods/members/inviteGroups'),
        acceptInvitation:require('./methods/members/acceptInviation'),
        updateContact:require('./methods/members/updateContacts'),
        acceptUser:require('./methods/acceptUser.js'),
        removeUser:require('./methods/removeUser'),
        checkMembers:function (id)
        {
            for(let i = 0; i < this.contacts.length; i++)
            {
                if(parseInt(this.contacts[i]) == id)
                {
                    return false
                }
            }

            return true;
        }
    }
});

/*
 data:
 {
 contacts: [],
 contactsCount:0,
 membersOnlyGroups:[],
 disable:false,
 },
 ready:require('./methods/member-ready.js'),
 methods:{
 acceptUser:require('./methods/acceptUser.js'),
 removeUser:require('./methods/removeUser'),
 checkMembers:function (id)
 {
 for(var i = 0; i < this.contacts.length; i++)
 {
 if(parseInt(this.contacts[i]) == id)
 {
 return false
 }
 }

 return true;
 },
 updateContact: function (id)
 {
 var role = "member";
 if(this.checkMembers(id))
 {
 role = "contact";
 }

 this.$http.put("/api/groups/"+this.groupId+"/users/"+id, {
 role:role,
 _token:this.csrfToken,
 api_token:this.apiToken,
 accepted:true
 });
 },
 inviteGroups:function () {
 console.log("hello");
 }
 },
 watch: {
 contacts: {
 handler: function (val, oldVal)
 {

 //this.$http.put("/api/groups/"+this.groupId+"/users/"+)
 },
 deep: true
 }
 },

 */
