let store = {
    posts:[],
    draftPosts:[],
};

var d = new Date();

let postVue = new Vue({
    el:"#posts",
    data:{
        shared:store,
        subject:"",
        body:"",
        published_at: d.getUTCFullYear()+"-"+(parseInt(d.getMonth())+1)+"-"+d.getDate()+" "+d.getUTCHours()+":"+d.getUTCMinutes()+":"+d.getSeconds()
    },
    computed:{
        currentDate:function () {
            let today = new Date();
            let dd = today.getDate();
            let mm = today.getMonth()+1; //January is 0!
            let yyyy = today.getFullYear();

            if(dd<10){
                dd='0'+dd
            }

            if(mm<10){
                mm='0'+mm
            }

            today = yyyy+'-'+mm+'-'+dd+' 00:00:00';
            return today;
        }
    },
    mounted: function ()
    {
        this.$nextTick(function ()
        {
            let post =  $("#posts");
            let postsUrl = post.attr("data-posts-url");
            let vm = this;
            this.$http.get(postsUrl).then(function (response) {
                vm.shared.posts = response.body.posts;
                vm.shared.draftPosts = response.body.drafts;
            });
        });
    },
    methods:{
        modal:function (id)
        {
            $('#'+id).modal('show');
        },
        saveAsDraft:require('./methods/post/saveAsDraft'),
        publishPost:require('./methods/post/publishPost')
    }
});

new Vue({
    el:"#modals",
    data:{
        shared:store,
    },
    methods:{
        updatePost:require('./methods/post/updatePost')
    }
});