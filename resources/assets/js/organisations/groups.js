//require('../bootstrap');
new Vue({
    el:"#home",
    data:{
        name:"",
        description:"",
        email:"",
        address:"",
        city:"",
        zip:"",
        country_id:0,
        member_type_id:0,
        saved:false,
        primary:"btn-primary",
        success:"btn-success"
    },
    props:['group_id'],
    created:require('./methods/home/created'),
    mounted: function ()
    {
        this.$nextTick(function ()
        {
            let vm = this;
            $('textarea.editor').each(function(){
                CKEDITOR.replace(this.id);
            });

            setTimeout(function(){
                CKEDITOR.instances["description"].setData(vm.description);
            }, 2000);
        });
    },
    methods:{
        onSubmit:require('./methods/home/onSubmit')
    }
});

/*new Vue({
 el:"#home",
 data:{

 },
 created:function ()
 {

 },
 mounted: function () {
 this.$nextTick(function ()
 {
 $('textarea.editor').each(function(){
 CKEDITOR.replace(this.id);
 });
 });
 },
 methods:{
 onSubmit:require('./methods/home/onSubmit')
 }
 });

 /*new Vue({
 el:'#home',
 ready:require('./methods/ready.js')
 });*/
