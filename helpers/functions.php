<?php
if (! function_exists('testExport')) {
    /**
     * Change all instances of :argument to {argument}
     *
     * @param $string
     * @return void
     *
     */
    function testExport($string) {
        array_walk_recursive($string, function (&$v, $k) { $v = preg_replace('/:(\w+)/', '{$1}', $v); });

        return $string;
    }
}