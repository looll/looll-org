const mix = require('laravel-mix');
const webpack = require("webpack");


mix.webpackConfig({
    resolve: {

    },
    plugins: [
        new webpack.DefinePlugin({
            __VUE_OPTIONS_API__: true,
            __VUE_PROD_DEVTOOLS__: false,
        }),
    ],
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js').vue(3).version()
    .sass('resources/sass/app.scss', 'public/css');

mix.js('resources/assets/js/myBook/app.js', 'public/js/myBook/app.js').vue(3).version();
mix.js("resources/assets/js/profile/app.js", "public/js/profile/app.js").vue({ version: 3 }).version();
mix.js("resources/js/groups/app.js", "public/js/groups/app.js").vue({ version: 3 }).version();
mix.js("resources/js/groups/show.js", "public/js/groups/show.js").vue({ version: 3 }).version();
//mix.js("resources/js/companies/photos/app.js", "public/js/companies/photos/app.js");