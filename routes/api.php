<?php
use Illuminate\Support\Facades\Route;

Route::group(['middleware'=>['auth:api'], 'namespace'=>'Api', 'as'=>'api.'], function (){
    Route::resource('photos', 'PhotosController',['only'=>['store','update','destroy']]);
    Route::resource('companies.photos', 'CompanyPhotoController');
    Route::resource('categories', 'CategoriesController');
    Route::resource('profiles', 'ProfilesController');
    Route::resource('profiles.announcements', 'ProfileAnnouncementController');
    Route::resource('profiles.photos', 'ProfilePhotoController');
    Route::resource('profiles.categories', 'ProfileCategoryController');
    Route::resource('announcements', 'AnnouncementsController');
    Route::resource('groups', 'GroupsController');
    Route::resource('groups.posts', 'GroupPostController');
    Route::resource('groups.photos', 'GroupPhotoController');
    Route::resource('groups.announcements', 'GroupAnnouncementController');
    Route::resource('groups.users', 'GroupUserController');
    Route::resource("posts", "PostsController");
    Route::resource('groups.children', 'GroupChildController');
    Route::resource('groups.parents', 'GroupParentController');
    Route::resource('users', 'UsersController');
    Route::resource('users.profiles', 'UserProfileController', ['only' => ['store', 'update', 'destroy']]);
    Route::resource('users.groups', 'UserGroupController');
    Route::resource("roles", "RolesController");
    Route::resource('visitors', 'VisitorsController');

});