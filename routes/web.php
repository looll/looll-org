<?php

use App\Http\Controllers\MyBookProfileController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Mcamara\LaravelLocalization\LaravelLocalization;


$laravelLocalization = new LaravelLocalization();
Route::group(["prefix"=>"admin", 'as' => 'admin.', "namespace"=>"Admin", "middleware"=>['role:SuperAdmin', 'auth']], function ()
{
    Route::get('/', ['as'=>'dashboard', "uses"=>'AdminController@index']);
    Route::get('/home', ['as'=>'home', "uses"=>'AdminController@index']);
    Route::resource('users', 'UsersController');
    Route::resource('groups', 'GroupsController');
    Route::resource('companies', 'CompaniesController');
});

Route::group([
    'prefix' => $laravelLocalization->setLocale(),
    "middleware" => [
        'localeSessionRedirect',
        'localizationRedirect',
        'localeViewPath'

    ]
], function()
{
    Route::group(["middleware"=>['auth', 'verified', 'user.has.profile']], function ()
    {
        Route::get('/home', 'HomeController@index')->name('home');
        Route::resource('posts', 'PostsController');
        Route::resource('myBooks', 'MyBookController');
        Route::resource("myBooks.profiles", MyBookProfileController::class);
        Route::resource('users', 'UsersController');
        Route::resource('categories', 'CategoriesController');
        Route::resource('myBooks.profiles',"MyBookProfileController")->only(['store']);
        Route::resource('profiles.announcements', 'ProfileAnnouncementController');
        Route::resource('announcements', 'AnnouncementsController');
        Route::resource('groups.announcements', 'GroupAnnouncementController');
        Route::resource('groups.users', "GroupUserController");
        Route::resource('groups.roles', "GroupRoleController");
        Route::resource('companies.photos', "CompanyPhotoController");
        Route::resource('companies.backgrounds', "CompanyBackgroundController");
        Route::resource('companies.hours', "CompanyHourController");
        Route::resource("companies.announcements", "CompanyAnnouncementController");
        Route::resource("companies.tags", "CompanyTagController");
        Route::resource("companies.messages", "CompanyMessageController");

    });


    Route::get('/', function () {
        return view('welcome');
    });
    Auth::routes(['verify'=>true]);
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/disclaimer', 'PagesController@disclaimer');
    Route::get('/about', 'PagesController@about');
    Route::resource('groups', 'GroupsController');
    Route::resource('searches', 'SearchesController');
    Route::resource('profiles', 'ProfilesController');
    Route::get('/{profile}', ['as' => 'profile', 'uses' => 'ProfilesController@show']);



});


