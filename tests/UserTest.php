<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    use DatabaseTransactions;
    /** @test */
    public function a_user_add_a_person_to_his_mybook()
    {
        $user = \App\User::whereId(1)->first();
        $this->actingAs($user);
        $this->assertFalse($user->hasMyBook());
        $this->visit('/ledner.nakia');
        $this->press('Add to MyBook');
    }

    /** @test */
    public function he_should_be_able_to_create_a_profile()
    {
        $user = \App\User::with([])->find(95);

        //$user = Auth::user();

        $faker = Faker\Factory::create();
        $paragraph = $faker->paragraph(4);
        $birthday = $faker->date('Y-m-d');
        $address = $faker->address;
        $postcode = $faker->postcode;
        $city = $faker->city;


        $site = route('profiles.edit', ['profiles'=>$user->username]);
        $this->actingAs($user)
            ->visit("profiles/{$user->username}/edit");

        $this->type('Jon Arnar', 'name')
            ->type( $paragraph, 'description')
            ->type($user->email, 'email')
            ->type($birthday, 'birthday')
            ->select('1', 'marital_status_id')
            ->select('male', 'gender')
            ->type($address, 'address')
            ->type($postcode, 'zip')
            ->type($city, 'city')
            ->select('1', 'country_id')
            ->submitForm("Save");

        $this->seeInDatabase('profiles', [
            'address'=>$address,
            'birthday'=>$birthday,
            'city'=>$city,
            'email' => $user->email,
            'name' => "Jon Arnar"
        ]);

    }

    /** @test */
    public function he_should_visit_my_site()
    {
        $user = \App\User::with([])->find(94);
        $this->actingAs($user)
            ->visit("/{$user->username}");
    }



}