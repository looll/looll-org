<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\MyBook;

class MyBookTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function a_my_book_has_a_name()
    {
        $myBook = new MyBook(['name'=>'MyBook']);
        $this->assertEquals('MyBook', $myBook->name);
    }

    /** @test */
    public function a_user_can_add_person_to_myBook()
    {
        // given I have a mybook
        $user = factory(\App\User::class)->create();
        $myBook = factory(MyBook::class)->create(['user_id'=>$user->id]);
        $category = factory(\App\Category::class)->create(['my_book_id'=>$myBook->id]);
        $profile = factory(\App\Profile::class)->create();
        // and a user
        // and that user is logged in
        $this->actingAs($user);
        //we add profile to the mybook of user.
        $category->add($profile);
        // then we should evidence in the database
        //this->seeInDatabase('category_profile', ['category_id'=>$category->id]);

        // and that person is added to mybook
    }

    /** @test */
    public function when_user_open_my_book_he_should_create_a_new_one_if_no_book_exist()
    {
        // given that we a have a user
        $user = \App\User::where('id', 1)->with('myBook')->first();
        $this->actingAs($user)->visit('/')->click('My Book');
        if($user->myBook == null)
        {
            $this->seePageIs('/myBook/create');
            $this->press('Create a new my Book');
        }
        else
        {
            $this->seePageIs('/myBook/'.$user->myBook->id);
        }


        // user click on link mybook
        // if user does not have mybook the user is redirect to create
        // else the user can see his mybook
    }

    /** @test */
    public function a_user_add_a_profile_to_my_book()
    {
        $user = \App\User::find(7);
        $this->actingAs($user)->visit('/telma');
        $this->press('Add to my book');
    }
}
