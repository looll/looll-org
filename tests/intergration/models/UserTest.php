<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserModelTest extends TestCase
{
    /** @test */
    public function it_finds_a_user_with_profile_by_username()
    {
        $user = \App\User::with('profile')->where('username', 'jondijam')->first();
        dd($user->profile);
        //$this->assertEquals("Jón Arnar Magnússon", $user->profile->name);
    }
}
