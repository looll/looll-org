<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MailTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @test
     */
    public function send_mail_to_a_user()
    {
        Mail::send('emails.test', [], function($message)
        {
            $message->to("lofturms@gmail.com", "Loftur")->subject("Loftur is good");
            $message->from('looll@looll.org', 'looll.org');
        });
    }
}
