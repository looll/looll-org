<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A user need to register.
     *
     * @test
     */
    public function register_new_user()
    {
        $this->visit('/register');
        $this->type("Gunnar Andri Gunnarsson", 'name');
        $this->type("gunnar1234", 'username');
        $this->type("gunnar@example.com", 'email');
        $this->type("1234567", 'password');
        $this->type("1234567", 'password_confirmation');
        $this->submitForm('Register');
    }
}
