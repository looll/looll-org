<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;

class UserSendPostTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @test
     */
    public function user_send_post_from_group()
    {
    	$user = User::find(1);
        $this->actingAs($user)->visit("groups/5/edit?tab=post");
        $this->submitForm('publish',[
            "subject" => "Subject",
            "body" => "Text",
            "published_at" => \Carbon\Carbon::now()->format("Y-m-d")
        ]);

    }
}
