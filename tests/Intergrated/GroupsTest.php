<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GroupsTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * @test
     */
    public function a_user_create_a_group()
    {
        $user = \App\User::first();
        $this->actingAs($user)->visit('/groups/create')
            ->type('JCI', 'name')
            ->type('Description', 'description')
            ->type('jci@jci.is', 'email')
            ->type('Hellusund 3', 'address')
            ->type('111', 'zip')
            ->select('1', 'country_id')
            ->select('1', 'member_type_id')
            ->press('Save');

    }
}
