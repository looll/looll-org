<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProfileTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A user create a new profile.
     *
     * @test
     */
    public function user_create_a_new_profile()
    {
        // login a user
        Auth::loginUsingId(6);
        $user = Auth::user();
        //$user = \App\User::with([])->find(1);
        $this->actingAs($user);
        $this->visit("/profiles/{$user->username}/edit");
        $this->type('Gunnar Jonsson', "name");
        $this->type("description is good", "description");
        $this->type("gunnar@example.com", "email");
        $this->type("1981-02-20 00:00:00", "birthday");
        $this->select("1","marital_status_id");
        $this->select("male", "gender");
        $this->type("Vesturberg 70", "address");
        $this->type("Reykjavik", "city");
        $this->select("1", "country_id");
        //$this->submitForm("save");
        $this->press("save");

        //$this->seeInDatabase("profiles",['name'=>"Gunnar Jonsson"]);



        // user fill out form.
        // user save the profile
        // check if data is in database.

        //$user = factory();
        //$this->actingAs()
    }

    /**@test*/
    public function test_user_edit_a_profile()
    {
        $user = \App\User::where('id', 7)->first();
        $this->actingAs($user)->visit('/profiles/'.$user->username.'/edit');
    }

    /**
     * @test
     */
    public function a_user_visit_my_site()
    {
        $user = \App\User::first();
        $this->actingAs($user)->visit('/'.$user->username);
    }

    /**
     * @test
     */
    public function test_user_visit_home_page()
    {
        $user = \App\User::where('id', 7)->first();
        $this->actingAs($user)->visit('/');
    }
}
