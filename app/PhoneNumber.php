<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class PhoneNumber extends Model {
    use Searchable;

	protected $table = "phone_numbers";
    protected $fillable = ['phone_number','phone_type'];

    public function phoneAble()
    {
        return $this->morphTo();
    }


    public function profile()
    {
        return $this->belongsTo('App\Profile');
    }
}
