<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role as SpatieRole;

class Role extends SpatieRole {

	//protected $table = 'roles';
    //protected $fillable = ['name'];

    public function groups()
    {

        return $this->belongsToMany('App\Group', 'group_role_user', 'role_id', 'id')->withPivot('accepted', 'group_id')->withTimestamps();
    }




    /*
    public function groupUsers()
    {
        return $this->belongsToMany('App\User', 'group_role_user', 'role_id', 'id')->withPivot('accepted','user_id')->withTimestamps();
    }

    public function groups()
    {
        return $this->belongsToMany('App\Group', 'group_role_user', 'role_id', 'id')->withPivot('accepted', 'group_id')->withTimestamps();
    }*/

}
