<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model {

	protected $table = "positions";

	public function profile()
	{
		return $this->belongsTo('App\Profile');
	}

	public function company()
	{
		return $this->belongsTo('App\Company');
	}
}
