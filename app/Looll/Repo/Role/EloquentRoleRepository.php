<?php
/**
 * Created by PhpStorm.
 * User: jondijam
 * Date: 18.9.2016
 * Time: 05:47
 */

namespace App\Looll\Repo\Role;


use App\Looll\Repo\DatabaseRepository;
use App\Role;

class EloquentRoleRepository extends DatabaseRepository implements RoleRepository
{
    public function __construct(Role $model)
    {
        parent::__construct($model);
    }

    public function getFirstBy($key, $value, array $with = array())
    {
        if(is_array($value))
        {
            foreach ($value as $v)
            {
                return $this->getFirstBy($key, $v, $with);
            }
        }
        else
        {
            return $this->make($with)->where($key, '=', $value)->first();
        }
    }

}