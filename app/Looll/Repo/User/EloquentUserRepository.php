<?php 
namespace App\Looll\Repo\User;

use App\Looll\Repo\DatabaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class EloquentUserRepository extends DatabaseRepository implements UserRepository
{
	protected $model;

	public function __construct(Model $model)
	{
		$this->model = $model;
	}

	public function confirm($code)
	{
		$user = $this->model->where("confirmation_code", $code)->first();
		$user->confirmed = 1;
		$user->save();

		return $user;
	}

	public function getRandomBy($number)
	{

		$users = [];
		$counts = $this->model->whereHas('profile', function($q)
		{
			$q->whereHas('photos', function($q)
			{
				$q->where('primary_photo', true);
			});

		})->with(['profile'])->count();

		if($counts >= $number)
		{
			$users = $this->model->whereHas('profile', function($q)
			{
				$q->whereHas('photos', function($q)
				{
					$q->where('primary_photo', true);
				});

			})->with(['profile'=>function($q)
			{
				$q->with(['photos'=>function($qq)
				{
					$qq->where('primary_photo', true)->first();
				}]);
			}])->get()->random($number);
		}
		elseif($counts < $number)
		{
			$users = $this->model->whereHas('profile', function($q)
			{
				$q->whereHas('photos', function($q)
				{
					$q->where('primary_photo', true);
				});

			})->with(['profile'=>function($q)
			{
				$q->with(['photos' => function($qq)
				{
					$qq->where('primary_photo', true)->first();
				}]);
			}])->get();
		}

		return $users;


	}

    public function getGroupsByRole($userId, $roleId, $ownerId = 0, $accepted = true)
    {
        $user = $this->model->find($userId);
        if($user->groups()->count())
        {
            if($ownerId > 0)
            {
                $groupsByOwner = $user->groups()
                    ->wherePivot('role_id', $ownerId)
                    ->wherePivot('accepted', $accepted)
                    ->get();

                $groupsByAdmin = $user->groups()
                    ->wherePivot('role_id', $roleId)
                    ->wherePivot('accepted', $accepted)
                    ->get();

                $groupsByOwner = $groupsByOwner->merge($groupsByAdmin);


                return $groupsByOwner;
            }

            return $user->groups()->wherePivot('role_id', $roleId)->wherePivot('accepted', $accepted)->get();
        }
        return null;
        /*if($ownerId > 0)
            return $this->groups()->wherePivot('role_id', $roleId)->orWherePivot('role_id', $ownerId)->wherePivot('accepted', $accepted)->get();
        return $this->groups()->wherePivot('role_id', $roleId)->wherePivot('accepted', $accepted)->get();*/
    }
}
?>