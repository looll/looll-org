<?php  
namespace App\Looll\Repo\User;

//use Illuminate\Contracts\Cache\Repository as Cache;
use Illuminate\Support\Facades\Cache;

class CachingUserRepository implements UserRepository
{
	protected $user;

	public function __construct(UserRepository $user)
	{
		$this->user = $user;
	}

	public function getById($id, array $with = array())
	{
		$key = "user.id.".$id;

		return Cache::remember($key, 30, function() use ($id, $with)
		{
			return $this->user->getById($id, $with);
		});
	}

	public function getAll(array $with = array())
	{
		$key = "userGetAll";

		return Cache::remember($key, 30, function() use ($with)
		{
			return $this->user->getAll($with);
		});
	}
	
	public function getFirstBy($key, $value, array $with = array())
	{
		$cacheKey = "user.first.by.".$key.'.'.$value;

		return Cache::remember($cacheKey, 30, function() use ($key, $value, $with)
		{
			return $this->user->getFirstBy($key, $value, $with);
		});
	}

	public function getManyBy($key, $value, array $with = array())
	{
		$cacheKey = "userGetManyBy".$key.$value;

		return Cache::remember($cacheKey, 30, function() use ($key, $value, $with)
		{
			return $this->user->getManyBy($key, $value, $with);
		});
	}
	
	public function getByPage($page = 1, $limit = 10, $with = array())
	{
		$cacheKey = "getByPage".$page;

		return Cache::remember($cacheKey, 30, function() use ($page, $limit, $with)
		{
			return $this->user->getByPage($page, $limit, $with);
		});
	}

	public function getRandomBy($number)
	{
		return $this->user->getRandomBy($number);
	}

	public function  confirm($code)
	{
		return $this->user->confirm($code);
	}

	public function delete($id)
	{
		$key = "userGetById".$id;
		$this->cache->forget($key);

		return $this->user->delete($id);
	}

    public function getGroupsByRole($userId, $roleId, $ownerId = 0, $accepted = true)
    {
        return $this->user->getGroupsByRole($userId, $roleId, $ownerId, $accepted);
    }

	public function destroy($id)
	{
		return $this->user->destroy($id);
	}
}
?>