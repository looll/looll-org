<?php  
namespace App\Looll\Repo\User;

interface UserRepository
{
	public function getById($id, array $with = array());
	public function getAll(array $with = array());
	public function getFirstBy($key, $value, array $with = array());
	public function getManyBy($key, $value, array $with = array());
	public function getByPage($page = 1, $limit = 10, $with = array());
	public function getRandomBy($number);
    public function getGroupsByRole($userId, $roleId, $ownerId = 0, $accepted = true);
	public function confirm($code);
	public function delete($id);
	public function destroy($id);
}
?>