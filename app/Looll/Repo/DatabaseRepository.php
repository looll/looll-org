<?php  
namespace App\Looll\Repo;

abstract class DatabaseRepository
{
	protected $model;
	public function __construct($model)
	{
		$this->model = $model;
	}

	 /**
     * Make a new instance of the entity to query on
     *
     * @param array $with
     */
    public function make(array $with = [])
    {
        return $this->model->with($with);
    }


    /**
     * Find all entities
     * @param array $with
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getAll(array $with = [])
    {
    	return $this->make($with)->get();
    }

    /**
     * Find an entity by id
     *
     * @param int $id
     * @param array $with
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getById($id, array $with = [])
    {
    	$query = $this->make($with);
        return $query->find($id);
    }

    public function getByFirstOrCreate($data, $value , $key = "email")
    {
        $model = $this->getFirstBy($key, $value);
        if(isset($model)) return $model;
    }

    /**
     * Find a single entity by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getFirstBy($key, $value, array $with = array())
    {
        return $this->make($with)->where($key, '=', $value)->first();
    }

    /**
     * Find many entities by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getManyBy($key, $value, array $with = array())
    {
        return $this->make($with)->where($key, '=', $value)->get();
    }


    /**
     * Get Results by Page
     *
     * @param int $page
     * @param int $limit
     * @param array $with
     * @return StdClass Object with $items and $totalItems for pagination
     */
    public function getByPage($page = 1, $limit = 10, $with = array())
    {
      $result             = new StdClass;
      $result->page       = $page;
      $result->limit      = $limit;
      $result->totalItems = 0;
      $result->items      = array();
     
      $query = $this->make($with);
     
      $model = $query->skip($limit * ($page - 1))
                     ->take($limit)
                     ->get();
     
      $result->totalItems = $this->model->count();
      $result->items      = $model->all();
     
      return $result;
    }
    public function destroy($id)
    {
        $this->model->destroy($id);
    }
    public function delete($id)
    {
        $this->model->delete($id);
    }
}

?>