<?php
namespace App\Looll\Repo\Announcement;
use App\Announcement;
use App\Looll\Repo\DatabaseRepository;
use Illuminate\Database\Eloquent\Model;

class EloquentAnnouncementRepository extends DatabaseRepository implements AnnouncementRepository
{
    protected $model;

    public function __construct(Announcement $model)
    {
        $this->model = $model;
    }

    public function create(array $data, $announcementAble)
    {
        $announcement = $this->model->newInstance($data);
        $data = $announcementAble->announcements()->save($announcement);

        return $data;
    }

    public function update(array $data, $id, $announcementAble)
    {
        $announcement = $this->model->find($id)->fill($data);
        $announcementAble->announcements()->save($announcement);
    }
}