<?php
/**
 * Created by PhpStorm.
 * User: jondi
 * Date: 5.4.2015
 * Time: 15:31
 */
namespace App\Looll\Repo\Photo;

use App\Looll\Repo\Illuminate;
use App\Looll\Repo\StdClass;

interface PhotoRepository
{
    /**
     * Find all entities
     * @param array $with
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getAll(array $with = []);

    /**
     * Find an entity by id
     *
     * @param int $id
     * @param array $with
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getById($id, array $with = []);

    public function getByFirstOrCreate($data, $value, $key = "email");

    /**
     * Find a single entity by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getFirstBy($key, $value, array $with = array());

    /**
     * Find many entities by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getManyBy($key, $value, array $with = array());

    /**
     * Get Results by Page
     *
     * @param int $page
     * @param int $limit
     * @param array $with
     * @return StdClass Object with $items and $totalItems for pagination
     */
    public function getByPage($page = 1, $limit = 10, $with = array());

    public function destroy($id);

    public function delete($id);

    public function savePhoto($data, $profileId, $groupId);
    public function updatePhoto(array $data, $photoId);
}