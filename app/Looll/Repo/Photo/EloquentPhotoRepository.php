<?php
namespace App\Looll\Repo\Photo;


use App\Looll\Repo\DatabaseRepository;
use App\Looll\Repo\Group\GroupRepository;
use App\Looll\Repo\Profile\ProfileRepository;
use App\Photo;

class EloquentPhotoRepository extends DatabaseRepository implements PhotoRepository
{
    protected $model;
    protected $photo;
    protected $profile;
    protected $group;

    public function __construct(Photo $photo, ProfileRepository $profile, GroupRepository $group)
    {
        $this->model = $photo;
        $this->photo = $photo;
        $this->profile = $profile;
        $this->group = $group;
    }

    public function getById($id, array $with = [])
    {
        return $this->photo->find($id);
    }

    public function savePhoto($data, $profileId, $groupId)
    {

        $photo = $this->photo->newInstance($data);
        if($profileId > 0)
        {
            $photoAble = $this->profile->getById($profileId);
        }

        if($groupId > 0)
        {
            $photoAble = $this->group->getById($groupId);
        }

        $photoAble->photos()->save($photo);

        return $photo;
    }
    public function updatePhoto(array $data, $photoId)
    {
        $profile_id = $data['profile_id'];
        $organisation_id = $data['group_id'];

        if ($profile_id > 0)
        {
            $profile = $this->profile->getById($profile_id, ['photos']);
            $photos = $profile->photos()->get();
        }

        if ($organisation_id > 0)
        {
            $group = $this->group->getById($organisation_id, ['photos']);
            $photos = $group->photos()->get();
        }

        foreach ($photos as $photo)
        {
            $photo = $this->model->find($photo['id']);
            $photo->primary_photo = 0;
            $photo->save();
        }

        $photo = $this->photo->find($photoId);
        $photo->primary_photo = 1;
        $photo->save();
    }

    public function destroy($id)
    {
        $this->photo->find($id)->destroy($id);
    }

    public function delete($id)
    {
        $this->photo->find($id)->delete();
    }
}