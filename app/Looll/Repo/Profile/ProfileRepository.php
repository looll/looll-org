<?php  
namespace App\Looll\Repo\Profile;

interface ProfileRepository
{
	public function getById($id, array $with = array());
	public function getAll(array $with = array());
	public function getFirstBy($key, $value, array $with = array());
	public function getManyBy($key, $value, array $with = array());
	public function getByPage($page = 1, $limit = 10, $with = array());
	public function getByUser($username, array $with = []);
}