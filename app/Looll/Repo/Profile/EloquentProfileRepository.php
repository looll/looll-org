<?php  
namespace App\Looll\Repo\Profile;
use App\Looll\Repo\DatabaseRepository;
use App\Looll\Repo\User\UserRepository;
use App\Profile;

class EloquentProfileRepository extends DatabaseRepository implements ProfileRepository{
    
    protected $model;
    protected $user;

    public function __construct(Profile $model, UserRepository $user)
    {
        $this->model = $model;
        $this->user = $user;
    }

    public function getByUser($username, array $with = [])
	{
		$user = $this->users->getFirstBy("username", $username, $with);
        return $user;
	}
}