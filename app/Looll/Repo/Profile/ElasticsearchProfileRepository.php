<?php
namespace App\Looll\Repo\Profile;

use Elasticsearch\Client;
use App\Profile;
use Illuminate\Support\Collection;
use Sleimanx2\Plastic\DSL\SearchBuilder;

class ElasticsearchProfileRepository implements ProfileRepository
{
    protected $profiles;
    protected $profile;
    protected $client;

    public function __construct(ProfileRepository $profiles, Profile $profile)
    {
        $this->profiles = $profiles;
        $this->profile = $profile;
    }

    public function getById($id, array $with = array())
    {
        return $this->profiles->getById($id, $with);
    }

    public function getAll(array $with = array())
    {
        return $this->profiles->getAll($with);
    }

    public function getFirstBy($key, $value, array $with = array())
    {
        return $this->profiles->getFirstBy($key, $value, $with);
    }

    public function getManyBy($key, $value, array $with = array())
    {
        return $this->profiles->getManyBy($key, $value, $with);
    }

    public function getByPage($page = 1, $limit = 10, $with = array())
    {
        return $this->profiles->getByPage($page, $limit, $with);
    }

    public function getByUser($username, array $with = [])
    {
        return $this->profiles->getByUser($username, $with);
    }

    public function search($query = "")
    {
        return $this->profile->search($query)->get();
    }

    public function create(array $data, $user)
    {
        return $this->profiles->create($data, $user);
    }

    public function update(array $data, $id)
    {
        return $this->profiles->update($data, $id);
    }
    public function delete($id)
    {
        return $this->profiles->delete($id);
    }

    public function destroy($id)
    {
        return $this->profiles->destroy($id);
    }

}

?>