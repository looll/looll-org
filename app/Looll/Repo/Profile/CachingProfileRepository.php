<?php
namespace App\Looll\Repo\Profile;
use Illuminate\Contracts\Cache\Repository as Cache;
class CachingProfileRepository implements ProfileRepository
{
    protected $profiles;
    protected $cache;

    public function __construct(ProfileRepository $profiles, Cache $cache )
    {
        $this->cache = $cache;
        $this->profiles = $profiles;
    }

    public function getById($id, array $with = array())
    {
        $cacheKey = "ProfileGetById".$id;
        return $this->cache->remember($cacheKey,30,function() use($id, $with)
        {
            return $this->profiles->getById($id, $with);
        });
    }

    public function getAll(array $with = array())
    {
        $cacheKey = "ProfileGetAll";
        return $this->cache->remember($cacheKey, 30, function() use($with)
        {
            return $this->profiles->getAll($with);
        });
    }
    public function getFirstBy($key, $value, array $with = array())
    {
        $cacheKey = "ProfileGetAll".$key.$value;

        return $this->cache->remember($cacheKey,30,function() use($key, $value, $with)
        {
            return $this->profiles->getFirstBy($key, $value, $with);
        });
    }
    public function getManyBy($key, $value, array $with = array())
    {
        $cacheKey = "ProfileGetManyBy".$key.$value;

        return $this->cache->remember($cacheKey, 30, function() use($key, $value, $with)
        {
            return $this->profiles->getManyBy($key, $value, $with);
        });
    }
    public function getByPage($page = 1, $limit = 10, $with = array())
    {
        $cacheKey = "ProfileGetByPage".$page.$limit;

        return $this->cache->remember($cacheKey, 30, function() use($page, $limit, $with)
        {
            return $this->profiles->getByPage($page, $limit, $with);
        });
    }
    public function getByUser($username, array $with = [])
    {
        $cacheKey = "ProfileGetByUser".$username;

        return $this->cache->remember($cacheKey, 30, function() use($username, $with)
        {
            return $this->profiles->getByUser($username, $with);
        });
    }
    public function search($query = "")
    {
        return $this->profiles->search($query);
    }

    public function create(array $data, $user)
    {
        return $this->profiles->create($data, $user);
    }

    public function update(array $data, $id)
    {
         return $this->profiles->update($data, $id);
    }

    public function delete($id)
    {
        return $this->profiles->delete($id);

    }

    public function destroy($id)
    {
        return $this->profiles->destroy($id);
    }
}
?>