<?php
namespace App\Looll\Repo\MaritalStatus;

use App\Looll\Repo\DatabaseRepository;
use Illuminate\Database\Eloquent\Model;

class EloquentMaritalStatusRepository extends DatabaseRepository implements MaritalStatusRepository
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

}