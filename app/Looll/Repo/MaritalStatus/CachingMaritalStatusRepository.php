<?php 
namespace App\Looll\Repo\MaritalStatus;

use Illuminate\Support\Facades\Cache;

class CachingMaritalStatusRepository implements MaritalStatusRepository
{
    private $maritalStatus;
    public function __construct(MaritalStatusRepository $maritalStatus)
    {
        $this->maritalStatus = $maritalStatus;
    }

     /**
     * Find all entities
     *
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getAll(array $with = array())
    {
        $ttl = 30;

        return Cache::remember('all', $ttl, function () use($with) {
            return new $this->maritalStatus->getAll($with);
        });
    }

    /**
     * Find an entity by id
     *
     * @param int $id
     * @param array $with
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getById($id, array $with = array())
    {
        $ttl = 30;
        return Cache::remember('maritalStatus.'.$id, $ttl, function () use($id, $with) {
            return $this->maritalStatus->getById($id, $with);
        });
    }

    public function getByFirstOrCreate($data, $value, $key = "email")
    {
        return $this->maritalStatus->getByFirstOrCreate($data, $value, $key);
    }

    /**
     * Find a single entity by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getFirstBy($key, $value, array $with = array())
    {
        return $this->maritalStatus->getFirstBy($key, $value, $with);
    }

    /**
     * Find many entities by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getManyBy($key, $value, array $with = array())
    {
        return $this->maritalStatus->getManyBy($key, $value, $with);
    }

    /**
     * Get Results by Page
     *
     * @param int $page
     * @param int $limit
     * @param array $with
     * @return StdClass Object with $items and $totalItems for pagination
     */
    public function getByPage($page = 1, $limit = 10, $with = array())
    {
        return $this->maritalStatus->getByPage($page, $limit, $with);
    }
}