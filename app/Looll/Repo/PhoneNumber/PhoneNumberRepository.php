<?php  
namespace App\Looll\Repo\PhoneNumber;

interface PhoneNumberRepository
{
    public function getAll(array $with = array());
    public function getFirstBy($key, $value, array $with = array());
    public function create(array $data, $profile);
    public function update(array $data, $profile);
    public function search($query);
    public function delete($id);
    public function destroy($id);
}

?>