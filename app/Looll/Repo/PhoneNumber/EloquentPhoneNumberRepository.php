<?php
namespace App\Looll\Repo\PhoneNumber;

use App\Looll\Repo\DatabaseRepository;
use App\PhoneNumber;

class EloquentPhoneNumberRepository extends DatabaseRepository implements PhoneNumberRepository
{
    protected $model;
    public function __construct(PhoneNumber $model)
    {
        $this->model = $model;
    }

    public function create(array $data, $profile)
    {
        $phone_number = $this->model->newInstance($data);
        $phone_number->profile()->associate($profile);
        $phone_number->save();

        return $phone_number;
    }

    public function update(array $data, $profile)
    {

        $id = intval($data['id']);

        if($id == 0)
        {
            $phone_number = $this->create($data, $profile);

            return $phone_number;
        }
        elseif($id > 0)
        {
            $phone_number = $this->model->find($id)->fill($data)->save();

            /*
            $phone_number =  $this->model->find($data['id'])->fill($data)->save();
            return $phone_number;*/
        }
    }

    public function search($query)
    {
        return $this->model->with('profile')->where('phone_number','like','%'.$query.'%')->get();
    }
}

?>