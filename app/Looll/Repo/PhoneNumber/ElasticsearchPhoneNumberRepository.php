<?php  
namespace App\Looll\Repo\PhoneNumber;

use App\PhoneNumber;
use Elasticsearch\Client;
use Illuminate\Support\Collection;

class ElasticsearchPhoneNumberRepository implements PhoneNumberRepository
{
    protected $phoneNumber;
    protected $client;

    public function __construct(Client $client, PhoneNumberRepository $phoneNumber)
    {
        $this->client = $client;
        $this->phoneNumber = $phoneNumber;
    }

    public function getAll(array $with = array())
    {
        return $this->phoneNumber->getAll($with);
    }

    public function getFirstBy($key, $value, array $with = array())
    {
        return $this->phoneNumber->getFirstBy($key, $value, $with);
    }

    public function create(array $data, $profile)
    {
        return $this->phoneNumber->create($data, $profile);
    }

    public function update(array $data, $profile)
    {
        return $this->phoneNumber->update($data, $profile);
    }

    public function search($query)
    {
        $items = $this->searchOnElasticsearch($query);
        return $this->buildCollection($items);

        //return $this->phoneNumber->search($query);
    }

    public function delete($id)
    {
        return $this->phoneNumber->delete($id);
    }

    public function destroy($id)
    {
        return $this->phoneNumber->destroy($id);
    }

    private function searchOnElasticsearch($query)
    {
        $items = $this->client->search([
            "index"=>"looll",
            "type"=>"phoneNumbers",
            "analyzer"=>[
                "my_index_analyzer" => [
                    "type" => "custom",
                    "tokenizer" => "standard",
                    "filter" => ["lowercase", "mynGram"]
                ]
            ],
            "body"=>[
                "query" => [
                    'query_string' => [
                        'query' => $query
                    ]
                ]
            ]
        ]);

        return $items;

    }

    private function buildCollection($items)
    {
        $result = $items['hits']['hits'];

        return Collection::make(array_map(function($r) {
            $phoneNumber = new PhoneNumber();
            $phoneNumber->newInstance($r['_source'], true);
            $phoneNumber->with('profile');
            $phoneNumber->setRawAttributes($r['_source'], true);
            return $phoneNumber;
        }, $result));
    }
}

?>