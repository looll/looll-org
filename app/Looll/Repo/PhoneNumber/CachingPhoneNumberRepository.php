<?php
namespace App\Looll\Repo\PhoneNumber;

use Illuminate\Contracts\Cache\Repository as Cache;

class CachingPhoneNumberRepository implements PhoneNumberRepository
{
	protected $cache;
	protected $phoneNumber;
	
	public function __construct(PhoneNumberRepository $phoneNumber, Cache $cache)
	{
		$this->phoneNumber = $phoneNumber;
		$this->cache = $cache;
	}

    public function getAll(array $with = array())
    {
        return $this->phoneNumber->getAll($with);
    }

    public function getFirstBy($key, $value, array $with = array())
    {
        return $this->phoneNumber->getFirstBy($key,$value,$with);
    }

    public function create(array $data, $profile)
    {
        return $this->phoneNumber->create($data, $profile);
    }

    public function update(array $data, $profile)
    {
        return $this->phoneNumber->update($data, $profile);
    }

    public function search($query)
    {
        return $this->phoneNumber->search($query);
    }

    public function delete($id)
    {
        return $this->phoneNumber->delete($id);
    }

    public function destroy($id)
    {
        return $this->phoneNumber->destroy($id);
    }
}

?>