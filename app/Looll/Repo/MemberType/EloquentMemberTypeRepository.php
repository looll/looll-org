<?php
namespace App\Looll\Repo\MemberType;


use App\Looll\Repo\DatabaseRepository;
use App\MemberType;

class EloquentMemberTypeRepository extends DatabaseRepository implements MemberTypeRepository
{
    protected $model;

    public function __construct(MemberType $model)
    {
        $this->model = $model;
    }

    public function getAll(array $with = [])
    {
        $memberTypes = $this->model->orderBy("id", "ASC")->get();

        return $memberTypes;
    }
}