<?php


namespace App\Looll\Repo\Group;

use App\Group;
use App\Looll\Repo\Country\CountryRepository;
use App\Looll\Repo\DatabaseRepository;
use App\Looll\Repo\MemberType\MemberTypeRepository;
use App\Looll\Repo\User\UserRepository;
use Illuminate\Database\Eloquent\Model;

class EloquentGroupRepository extends DatabaseRepository implements GroupRepository
{
    protected $model;
    protected $users;
    protected $country;
    protected $memberType;

    public function __construct(Group $model, UserRepository $users, CountryRepository $country, MemberTypeRepository $memberType)
    {
        $this->model = $model;
        $this->users = $users;
        $this->memberType = $memberType;
        $this->country = $country;

        parent::__construct($model);
    }

    /**
     * @param array $data
     * @param int $user_id
     * @param int $role_id
     * @param int $parent_id
     * @param int $accepted
     * @return \Illuminate\Support\Collection|null|static
     */
    public function create(array $data, $user_id, $parent_id, $role_id = 1, $accepted = true)
    {
        $memberTypeId = $data['member_type_id'];
        $memberType = $this->memberType->getById($memberTypeId);
        $parent = $this->getById($parent_id);
        $country = $this->country->getById($data['country_id']);

        $group = $this->model->newInstance($data);
        if(!empty($parent))
        {
            $group->parent()->associate($parent);
        }

        $group->memberType()->associate($memberTypeId);
        $group->country()->associate($data['country_id']);
        $group->save();

        $group->users()->attach($user_id, ['role_id' => $role_id, 'accepted'=> $accepted]);
    }

    public function isAdmin($groupId, $userId)
    {
        $group = $this->getById($groupId, ['users']);

        $user = $group->users()->where('user_id', $userId)->whereHas('groupRoles', function($q)
        {
            return $q->where('name', 'member')->orWhere('name', 'Admin');
        })->count();

        return $user;
    }

    public function getChildren($id)
    {
        $group = $this->getById($id, ['children']);
        $children = $group->children()->where('accepted_by_parent', 1)->get();
        return $children;
    }

    public function getMembers($id)
    {
        $group = $this->getById($id, ['users']);

        $members = $group->users()->whereHas('groupRoles', function($q)
        {
            $q->where('name', 'member')->orWhere('name', 'admin');
        })->get();

        return $members;
    }

    public function getContacts($id)
    {
        $group = $this->getById($id, ['users']);
        $contacts = $group->users()->whereHas('groupRoles', function($q)
        {
            $q->where('name', 'contact')->orWhere('name', 'admin');
        })->get();

        return $contacts;
    }

    public function attachUser($userId, $groupId, $roleId)
    {
        $group = $this->model->find($groupId);
        $group->users()->attach($userId,['role_id'=>$roleId, 'accepted'=>false]);
        return $group;
    }

    public function update(array $data, $id)
    {
        $memberTypeId = 0;
        $parentId = 0;
        $countryId = 0;
        if(isset($data['member_type_id']))
        {
            $memberTypeId = $data['member_type_id'];
        }

        if(isset($data['parent_id']))
        {
            $parentId = $data['parent_id'];
        }

        if(isset($data['country_id']))
        {
            $countryId = $data['country_id'];
        }

        $group = $this->model->find($id)->fill($data)->save();
        if($memberTypeId > 0)
            $this->model->find($id)->memberType()->associate($memberTypeId)->save();

        if($parentId > 0)
            $this->model->find($id)->parent()->associate($parentId)->save();

        if($countryId > 0)
            $this->model->find($id)->country()->associate($countryId)->save();


    }

    public function updatePivot($groupId, $userId, array $data)
    {
      return $this->model->findOrNew($groupId)->users()->updateExistingPivot($userId,$data);
    }

    public function search($query)
    {
        return $this->model->where("address","like","%".$query."%")->orWhere("name","like","%".$query."%")->get();
    }

    public function removeGroup($parentId, $groupId)
    {
        $groupId = (int) $groupId;
        $group = $this->model->findOrNew($groupId)->fill(['parent_id'=>0])->save();
    }

    public function removeUser($groupId, $userId)
    {
        $this->model->findOrNew($groupId)->users()->detach($userId);
    }

    public function getRandomBy($number = 10)
    {
        $counts = $this->model->has('photos')->count();
        $groups = [];
        if($counts >= $number)
        {
            $groups = $this->model->has('photos')->get()->random($number);
        }
        elseif($counts < $number)
        {
            $groups = $this->model->has('photos')->get();
        }

        return $groups;
    }
}