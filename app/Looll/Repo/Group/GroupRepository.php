<?php
/**
 * Created by PhpStorm.
 * User: jondi
 * Date: 21.3.2015
 * Time: 13:50
 */
namespace App\Looll\Repo\Group;

use App\Looll\Repo\Illuminate;
use App\Looll\Repo\StdClass;

interface GroupRepository
{
    public function create(array $data, $user_id, $parent_id, $role_id = 1, $accepted = true);
    public function update(array $data, $id);
    public function search($query);

    /**
     * Find all entities
     *
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getAll(array $with = array());

    /**
     * Find an entity by id
     *
     * @param int $id
     * @param array $with
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getById($id, array $with = array());

    public function getByFirstOrCreate($data, $value, $key = "email");

    /**
     * Find a single entity by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getFirstBy($key, $value, array $with = array());

    /**
     * Find many entities by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getManyBy($key, $value, array $with = array());

    /**
     * Get Results by Page
     *
     * @param int $page
     * @param int $limit
     * @param array $with
     * @return StdClass Object with $items and $totalItems for pagination
     */
    public function getByPage($page = 1, $limit = 10, $with = array());
    public function isAdmin($groupId, $userId);
    public function getRandomBy($number = 10);
    public function getChildren($id);
    public function getMembers($id);
    public function getContacts($id);
    public function attachUser($userId, $groupId, $roleId);
    public function updatePivot($groupId, $userId, array $data);
    public function removeGroup($parentId, $groupId);
    public function removeUser($groupId, $userId);

}