<?php


namespace App\Looll\Repo\Group;
use Illuminate\Contracts\Cache\Repository as Cache;

class CachingGroupRepository implements GroupRepository
{
    protected $group;
    protected $caching;

    public function __construct(GroupRepository $group, Cache $cache)
    {
        $this->group = $group;
        $this->caching = $cache;
    }

    public function create(array $data, $user_id, $parent_id, $role_id = 1, $accepted = 1)
    {
        return $this->group->create($data, $user_id,$parent_id,$role_id,$accepted);
    }

    public function update(array $data, $id)
    {
        return $this->group->update($data, $id);
    }

    public function search($query)
    {
        return $this->group->search($query);
    }

    /**
     * Find all entities
     *
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getAll(array $with = array())
    {
        return $this->group->getAll($with);
    }

    /**
     * Find an entity by id
     *
     * @param int $id
     * @param array $with
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getById($id, array $with = array())
    {
        return $this->group->getById($id, $with);
    }

    public function getByFirstOrCreate($data, $value, $key = "email")
    {
        return $this->group->getByFirstOrCreate($data, $value, $key);
    }

    /**
     * Find a single entity by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getFirstBy($key, $value, array $with = array())
    {
        return $this->group->getFirstBy($key, $value, $with);
    }

    /**
     * Find many entities by key value
     *
     * @param string $key
     * @param string $value
     * @param array $with
     */
    public function getManyBy($key, $value, array $with = array())
    {
        return $this->group->getManyBy($key, $value, $with);
    }

    /**
     * Get Results by Page
     *
     * @param int $page
     * @param int $limit
     * @param array $with
     * @return StdClass Object with $items and $totalItems for pagination
     */
    public function getByPage($page = 1, $limit = 10, $with = array())
    {
        return $this->group->getByPage($page, $limit, $with);
    }
    public function isAdmin($groupId, $userId)
    {
        return $this->group->isAdmin($groupId, $userId);
    }
    public function getChildren($id)
    {
        return $this->group->getChildren($id);
    }

    public function getMembers($id)
    {
        return $this->group->getMembers($id);
    }

    public function getContacts($id)
    {
        return $this->group->getContacts($id);
    }
    public function attachUser($userId, $groupId, $roleId)
    {
        return $this->group->attachUser($userId, $groupId, $roleId);
    }
    public function updatePivot($groupId, $userId, array $data)
    {
        return $this->group->updatePivot($groupId, $userId, $data);
    }

    public function removeUser($groupId, $userId)
    {
        return $this->group->removeUser($groupId, $userId);
    }

    public function removeGroup($parentId, $groupId)
    {
        return $this->group->removeGroup($parentId, $groupId);
    }

    public function getRandomBy($number = 10)
    {
        return $this->group->getRandomBy($number);
    }

}