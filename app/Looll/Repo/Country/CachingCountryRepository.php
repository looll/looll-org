<?php  
namespace App\Looll\Repo\Country;

use Illuminate\Support\Facades\Cache;

//use Illuminate\Contracts\Cache\Repository as Cache;
/**
* 
*/
class CachingCountryRepository implements CountryRepository
{
	protected $cache;
	protected $country;
	
	public function __construct(CountryRepository $country)
	{
		$this->country = $country;
	}

	public function getById($id, array $with = array())
	{
		return $this->country->getById($id, $with);
	}

	public function getAll(array $with = array())
	{
		$key = "countries.all";
		$minutes = 30;

		return Cache::remember($key, $minutes, function() use($with)
		{
			return $this->country->getAll($with);
		});
	}
	
	public function getFirstBy($key, $value, array $with = array())
	{
		
		return $this->country->getFirstBy($key, $value, $with);
	}

	public function getManyBy($key, $value, array $with = array())
	{
		return $this->country->getManyBy($key, $value, $with);
	}
	
	public function getByPage($page = 1, $limit = 10, $with = array())
	{
		return $this->country->getByPage($page, $limit, $with);
	}
	
}

?>