<?php  
namespace App\Looll\Repo\Country;

use App\Looll\Repo\DatabaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
* Countries
*/
class EloquentCountryRepository extends DatabaseRepository implements CountryRepository
{
	protected $model;

	public function __construct(Model $model)
	{
		$this->model = $model;
	}
}


?>