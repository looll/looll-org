<?php
namespace App\Looll\Repo\Company;

use App\Looll\Repo\DatabaseRepository;
use Illuminate\Database\Eloquent\Model;
use App\Company;

class EloquentCompanyRepository extends DatabaseRepository implements CompanyRepository
{
    protected $model;

    public function __construct(Company $model)
    {
        $this->model = $model;
    }

    public function create(array $data)
    {
        $company = $this->model->create($data);
        return $company;
    }


    public function update(array $data, $id)
    {
        if($id == 0)
        {
            $company = $this->create($data);
            return $company;
        }

        $this->model->find($id)->fill($data)->save();

        return $this->model->find($id);
    }
}