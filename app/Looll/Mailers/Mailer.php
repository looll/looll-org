<?php
namespace App\Looll\Mailers;
use Mail;

/**
 * Class Mailer
 * @package App\Looll\Mailers
 */
abstract class Mailer
{
    /**
     * @param $user
     * @param $subject
     * @param $view
     * @param $data
     */
    public function sendTo($user, $subject, $view, $data)
    {
        Mail::send($view, $data, function($message)use($user, $subject)
        {
            $message->from('looll.is@looll.org', 'looll.org');
            $message->to($user->email)->subject($subject);
        });
    }
}