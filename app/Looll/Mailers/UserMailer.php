<?php
namespace App\Looll\Mailers;
use App\User;
use Mail;

class UserMailer extends Mailer
{
    public function welcome(User $user)
    {
        $view = "emails.confirmation";
        $data = ["token"=>$user->token, 'update'=>'false'];
        $subject = "Thank you for registration at looll.is. Please verify your email!";
        $this->sendTo($user, $subject, $view, $data);
    }

    public function updateEmail(User $user)
    {
        $view = "emails.confirmation";
        $data = ["token"=>$user->token, 'update'=>true];
        $subject = "Please verify your new email.";
        $this->sendTo($user, $subject, $view, $data);
    }
}