<?php 

function errors_for($attribute, $errors)
{
	return $errors->first($attribute, '<span class="error">:message</span>');
}

function url_to_confirm()
{
	return route('auth.confirm');
}

function url_to_profile()
{
	return route('profile', \Auth::user()->username);
}

function link_to_profile($text = 'Profile')
{
	return link_to_route('profile', $text, \Auth::user()->username);
}

function link_to_create_profile($text = 'Edit')
{
	return link_to_route('profile.create', $text);
}

function link_to_edit_profile($text = 'Edit')
{
	return link_to_route('profile.edit', $text, \Auth::guard('api')->user()->username);
}

/**
 *
 */
function users_photo_path()
{
	return public_path().'/img/profile/'.Auth::guard('api')->user()->username.'/';
}

function users_photo_src($newFileName)
{
	return URL::asset('img/profile/'.Auth::guard('api')->user()->username.'/'.$newFileName,  env('APP_SECURITY', false));
}

function groups_photo_path($group)
{
    return public_path().'/img/groups/'.str_slug($group->name).'/';
}

function groups_photo_src($newFileName, $group)
{
    return URL::asset('img/groups/'.str_slug($group->name).'/'.$newFileName,  env('APP_SECURITY', false));
}

function set_active($path, $active = "active")
{
	return Request::is($path) ? $active : '';
}

