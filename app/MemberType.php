<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberType extends Model
{
    protected $table = "member_types";
    protected $fillable = ['name'];

    public function groups()
    {
        return $this->hasMany('App\Group', 'member_type_id', 'id');
    }
}
