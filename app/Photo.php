<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Photo extends Model {

    use SoftDeletes;

	protected $table = "photos";
	protected $fillable = ['src','filename','path', "thumbnail_src", "thumbnail_path",'caption','primary_photo'];

	public function photoable()
	{
		return $this->morphTo();
	}

	/**
	 * Get the primary photo
	 *
	 * @param  string  $value
	 * @return string
	 */
	public function getSrcAttribute($value)
	{
		if($value == null)
			$value = '/img/no-image.jpg';

		return $value;
	}
}
