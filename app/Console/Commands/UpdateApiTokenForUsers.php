<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class UpdateApiTokenForUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:api-token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set new api token for user that exists.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::where('api_token', null)->get();
        foreach ($users as $user)
        {
            $user->api_token = str_random(60);
            $user->save();
        }
    }
}
