<?php namespace App\Console\Commands;

use App\Group;
use Elasticsearch\Client;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class IndexGroupsToElasticSearch extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
    protected $name = 'es:index-groups';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Indexes all groups to ElasticSearch.';

	/**
	 * Create a new command instance.
	 *
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $groups = Group::get();

        foreach ($groups as $group)
        {
            Group::with(["country", 'photos'])->where('id', $group->id)->first()->document()->save();
        }

	    /*$group = Group::first();
        $group->document()->bulkSave($group->books);*/
	}
}
