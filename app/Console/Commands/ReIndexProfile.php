<?php

namespace App\Console\Commands;

use App\Profile;
use Illuminate\Console\Command;

class ReIndexProfile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'es:reindex-profiles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reindexing profiles in bulk';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = \Plastic::getClient();

        //index delete
        //$client->indices()->delete(['index'=> \Plastic::getDefaultIndex()]);
        $client->indices()->create(['index' => \Plastic::getDefaultIndex()]);

        //$profile = new Profile();
        //$profile->document()->reindex($profile->all());
    }
}
