<?php

namespace App\Console\Commands;

use App\Photo;
use App\User;
use Illuminate\Console\Command;

class UpdatePathForImageThatExist extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'photos:path';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update path for exist images';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $photos = Photo::all();

        foreach($photos as $photo)
        {
            if(!\File::exists($photo->path))
            {
                Photo::find($photo->id)->delete();
            }
        }
    }
}
