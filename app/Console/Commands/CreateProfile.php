<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class CreateProfile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'profiles:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create profile for existing users that do not have one';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::doesntHave('profile')->get();

        foreach ($users as $user)
        {
            $name = $user->username;
            if($user->name != null)
            {
                $name = $user->name;
            }

            $user->profile()->create(['name'=>$name]);
        }
    }
}
