<?php namespace App\Console\Commands;

use App\Profile;
use Elasticsearch\Client;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class IndexProfilesToElasticsearch extends Command {
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'es:index-profiles';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Indexes all profiles to elasticsearch';

	/**
	 * Create a new command instance.
	 *
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

        $profiles = Profile::with(['phoneNumbers', 'country', 'photos'])->get();
        foreach ($profiles as $profile)
        {
            Profile::where('id', $profile->id)->first()->document()->save();
        }
	}

	protected function create_index()
	{

	}
}