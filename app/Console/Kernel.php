<?php namespace App\Console;

use App\Console\Commands\CreateProfile;
use App\Console\Commands\ReIndexGroup;
use App\Console\Commands\ReIndexProfile;
use App\Console\Commands\UpdateApiTokenForUsers;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\Inspire',
		'App\Console\Commands\IndexProfilesToElasticsearch',
		'App\Console\Commands\IndexGroupsToElasticSearch',
        'App\Console\Commands\UpdatePathForImageThatExist',
        UpdateApiTokenForUsers::class,
        ReIndexProfile::class,
        ReIndexGroup::class,
        CreateProfile::class
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		$schedule->command('inspire')->hourly();
	}

}
