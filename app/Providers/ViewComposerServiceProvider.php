<?php namespace App\Providers;

use App\Http\Composers\AppComposer;
use App\Http\Composers\CompanyLayoutComposer;
use App\Http\Composers\CompanyMenuComposer;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Http\Composers\HeaderComposer;
use KgBot\LaravelLocalization\Facades\ExportLocalizations as ExportLocalization;

class ViewComposerServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		View::composer("profile.partials.form", 'App\Http\Composers\ProfileFormComposer');
		View::composer("companies.layout", CompanyLayoutComposer::class);
		View::composer("companies.partials._menu", CompanyMenuComposer::class);
		View::composer("groups.partials._form", 'App\Http\Composers\GroupFormComposer');
        View::composer("groups.partials._vueform", 'App\Http\Composers\GroupFormComposer');
		View::composer("components.header", HeaderComposer::class);
		View::composer("components.layout", AppComposer::class);
        View::composer( 'groups.edit', function ( $view ) {

            return $view->with( [
                'messages' => ExportLocalization::export()->toFlat(),
            ] );
        } );
		
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

}
