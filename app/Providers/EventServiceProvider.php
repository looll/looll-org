<?php namespace App\Providers;

use App\Looll\Mailers\UserMailer;
use App\Observers\ElasticSearchGroupObserver;
use App\Observers\ElasticSearchProfileObserver;
use App\Observers\CachingProfileObserver;
use App\User;
use App\Profile;
use App\Group;
use Elasticsearch\Client;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Contracts\Cache\Repository as Cache;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider {


	/**
	 * The event handler mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		'App\Events\CreatedPost' => [
			'App\Listeners\AssignPostToUser',
            'App\Listeners\DeleteOutdatedPost'
        ],
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
	];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
