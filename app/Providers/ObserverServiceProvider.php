<?php namespace App\Providers;


use App\Observers\CachingProfileObserver;
use Illuminate\Support\ServiceProvider;

use App\Profile;
use App\Group;
use App\Observers\ElasticSearchProfileObserver;
use App\Observers\ElasticSearchGroupObserver;
use Elasticsearch\Client;
class ObserverServiceProvider extends ServiceProvider {


	/**
	 * Bootstrap the application services.
	 *
	 * @param $profile
	 * @param $group
	 */
	public function boot(Profile $profile, Group $group)
	{
        Profile::observe(CachingProfileObserver::class);
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

}
