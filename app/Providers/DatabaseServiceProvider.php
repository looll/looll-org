<?php namespace App\Providers;

use App\Company;
use App\Country;
use App\Group;
use App\Looll\Repo\Company\CompanyRepository;
use App\Looll\Repo\Company\EloquentCompanyRepository;
use App\Looll\Repo\Country\CachingCountryRepository;
use App\Looll\Repo\Country\CountryRepository;
use App\Looll\Repo\Country\EloquentCountryRepository;
use App\Looll\Repo\Group\EloquentGroupRepository;
use App\Looll\Repo\Group\GroupRepository;
use App\Looll\Repo\MaritalStatus\EloquentMaritalStatusRepository;
use App\Looll\Repo\MaritalStatus\MaritalStatusRepository;
use App\Looll\Repo\MemberType\EloquentMemberTypeRepository;
use App\Looll\Repo\MemberType\MemberTypeRepository;
use App\Looll\Repo\MyBook\EloquentMyBookRepository;
use App\Looll\Repo\MyBook\MyBookRepository;
use App\Looll\Repo\PhoneNumber\EloquentPhoneNumberRepository;
use App\Looll\Repo\PhoneNumber\PhoneNumberRepository;
use App\Looll\Repo\Profile\ProfileRepository;
use App\Looll\Repo\Role\RoleRepository;
use Illuminate\Support\ServiceProvider;
use App\Looll\Repo\Profile\EloquentProfileRepository;
use App\Looll\Repo\Role\EloquentRoleRepository;
use App\Looll\Repo\User\CachingUserRepository;
use App\Looll\Repo\User\EloquentUserRepository;
use App\Looll\Repo\User\UserRepository;
use App\MaritalStatus;
use App\MemberType;
use App\MyBook;
use App\PhoneNumber;
use App\Profile;
use App\Role;
use App\User;

class DatabaseServiceProvider extends ServiceProvider {

	public function boot()
	{

	}

	public function register()
	{
		

		$this->app->bind(UserRepository::class, function($app){			
			$user = new EloquentUserRepository(new User());
			return new CachingUserRepository($user);
		});

		$this->app->bind(CountryRepository::class, function ($app) {
			$eloquentRepo = new EloquentCountryRepository(new Country());
			return new CachingCountryRepository($eloquentRepo);
		});

		$this->app->bind(MaritalStatusRepository::class, function($app){
            return new EloquentMaritalStatusRepository(new MaritalStatus());
		});

		$this->app->bind(CompanyRepository::class, function($app)
		{
			return new EloquentCompanyRepository(new Company());
		});


		$this->app->bind(ProfileRepository::class, function($app)
		{
			return new EloquentProfileRepository(
				new Profile(),
				$this->app->make(UserRepository::class)
			);
		});

		$this->app->bind(GroupRepository::class, function()
		{
			return new EloquentGroupRepository(new Group(), $this->app->make(UserRepository::class),
			$this->app->make(CountryRepository::class),
			$this->app->make(MemberTypeRepository::class)
		);
		});

		$this->app->bind(MemberTypeRepository::class, function()
		{
			return new EloquentMemberTypeRepository(new MemberType());
		});

		$this->app->bind(CountryRepository::class, function()
		{
		    $country = new EloquentCountryRepository(new Country());
			return new CachingCountryRepository($country);
		});

		$this->app->bind(PhoneNumberRepository::class, function()
		{
			return new EloquentPhoneNumberRepository(new PhoneNumber());
		});

		$this->app->bind(MyBookRepository::class, function()
		{
			return new EloquentMyBookRepository(new MyBook());
		});		

        $this->app->bind(RoleRepository::class, function()
        {
            return new EloquentRoleRepository(new Role());
        });
	}
	
}
