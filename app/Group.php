<?php namespace App;

use Illuminate\Database\Eloquent\Model;

use Laravel\Scout\Searchable;
use Kalnoy\Nestedset\NodeTrait;

class Group extends Model
{
    use Searchable, NodeTrait;
    use Searchable {
        \Laravel\Scout\Searchable::usesSoftDelete insteadof \Kalnoy\Nestedset\NodeTrait;
    }

    protected $table = "groups";
	protected $fillable = [
        'name',
        'email',
        'address',
        'zip',
        'city',
        'description',
        'parent_id',
        'accepted_by_parent'
    ];

    public function scopeByMemberType($query, $memberType = "members.only")
    {
        return $query->whereHas('memberType', function ($q) use($memberType){
            $q->where('name', $memberType);
        });
    }

	public function announcements()
    {
        return $this->morphMany('App\Announcement', 'announcement_able');
    }

    public function privateAnnouncements($take = 5)
    {
        return $this->announcements()->where('public', 0)->take($take)->get();
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }
    public function memberType()
    {
        return $this->belongsTo('App\MemberType');
    }
    public function photos()
    {
    	return $this->morphMany('App\Photo', 'photoable');
    }

    public function users()
    {
    	return $this->belongsToMany('App\User', 'group_role_user')
            ->using(GroupRoleUser::class)
            ->as('role')
            ->withPivot(['role_id', 'accepted']);
    }

    function byMemberTypes($memberType = "members.only")
    {
        return $this->whereHas('memberType', function ($q) use($memberType)
        {
            $q->where('name', $memberType);
        });
    }




    public function findUsersByRole($role, $role2 = null, $accepted = 1)
    {
        if($role2 === null)
        {
            return $this->users()->with('profile')->wherePivot('role_id', '=', $role);
        }

        return $this->users()->with('profile')->wherePivot('role_id', '=', $role)->orWherePivot('role_id', '=', $role2);

    }


    public function owner($role = "Owner")
    {
        $role = Role::findByName($role);
        return $this->findUsersByRole($role->id);
    }

    public function members()
    {
        $role = Role::findByName('Member');
        return $this->findUsersByRole($role->id);
    }

    public function admin()
    {
        $role = Role::findByName('Admin');
        return $this->findUsersByRole($role->id);
    }

    public function contacts($role1 = "Owner", $role2 = "Admin")
    {
        return $this->findUsersByRole($role1, $role2);
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role')
            ->using(GroupRoleUser::class)
            ->withPivot('accepted','user_id')
            ->withTimestamps();
    }

    public function posts()
    {
        return $this->morphMany('App\Post', 'postable');
    }
}
