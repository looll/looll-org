<?php

namespace App\Listeners;

use App\Events\CreatedPost;
use App\Group;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AssignPostToUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreatedPost  $event
     * @return void
     */
    public function handle(CreatedPost $event)
    {
        $postable = $event->postable;
        foreach ($postable->users as $user)
        {
            $event->post->users()->attach($user->id);
        }

        /*$descendantCount = $group->getDescendantCount();
        if($descendantCount > 0)
        {
            $descendantsGroups = $group->getDescendants();
            foreach ($descendantsGroups as $key => $group)
            {
                if($group->users->count())
                {
                    foreach ($group->users as $user)
                    {
                        $event->post->users()->attach($user->id);
                    }
                }
            }

        }
        else
        {

        }*/

        //
    }

    protected function getUsers(Group $group)
    {
        $group->load('users');
        $users = $group->users->pluck('id');

        return $users;

    }
}
