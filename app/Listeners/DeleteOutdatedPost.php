<?php

namespace App\Listeners;

use App\Events\CreatedPost;
use App\Post;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeleteOutdatedPost
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreatedPost  $event
     * @return void
     */
    public function handle(CreatedPost $event)
    {
        $older = Carbon::now()->gte($event->post->published_at);
        if($older)
        {
            $postCount = $event->postable->posts()->count();
            if($postCount > 6)
            {
                $post = $event->postable->posts()->oldest('published_at')->first();
                $post->delete();
            }
        }
    }
}
