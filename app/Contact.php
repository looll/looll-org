<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ["name"];

    public function contactable()
    {
        return $this->morphTo();
    }
}
