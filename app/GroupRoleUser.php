<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class GroupRoleUser extends Pivot
{
    protected $table = "group_role_user";

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }


}
