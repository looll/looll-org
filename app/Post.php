<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
class Post extends Model
{
    protected $dates = ['created_at', 'updated_at', 'published_at'];
    protected $fillable =  ['published_at', 'subject', 'body'];

    public function scopePublished($query)
    {
        return $query->where('published_at', '!=', null);
    }

    public function postable()
    {
        return $this->morphTo();
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
