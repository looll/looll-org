<?php


namespace App\Observers;

use App\Profile;
use Illuminate\Contracts\Cache\Repository as Cache;

class CachingProfileObserver
{
    protected $cache;


    public function __construct(Cache $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @param Profile $profile
     * @return bool
     */
    public function updating(Profile $profile)
    {

        $key = "username";
        $value = $profile->user->username;
        $cacheKey = "user.first.by.".$key.'.'.$value;
        if($this->cache->has($cacheKey))
            $this->cache->forget($cacheKey);
        
        $cacheKey = "ProfileGetById".$profile->id;
        if($this->cache->has($cacheKey))
            $this->cache->forget($cacheKey);

        $cacheKey = "ProfileGetAll";
        if($this->cache->has($cacheKey))
            $this->cache->forget($cacheKey);

        $cacheKey = "ProfileGetManyBy".$key.$value;
        if($this->cache->has($cacheKey))
            $this->cache->forget($cacheKey);

        $cacheKey = "ProfileGetByUser".$value;
        if($this->cache->has($cacheKey))
            $this->cache->forget($cacheKey);

        return true;
    }


}