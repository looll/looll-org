<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
	protected $table = "companies";
	protected $fillable = [
	    "name",
        "tin",
        "url",
        "email",
        "address",
        "city",
        "zip",
    ];

	public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function hours()
    {
        return $this->hasMany(Hour::class);
    }

    public function contacts()
    {
        return $this->morphMany(Contact::class, "contactable");
    }

    public function phoneNumbers()
    {
        return $this->morphMany(PhoneNumber::class, 'phone_able');
    }

    public function photos()
    {
        return $this->morphMany('App\Photo','photoable');
    }

    public function announcements()
    {
        return $this->morphMany('App\Announcement', 'announcement_able');
    }

    public function tags()
    {
        return $this->hasMany(Tag::class);
    }

    public function messages()
    {
        return $this->morphMany('App\Post', 'postable');
    }

}

