<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaritalStatus extends Model
{
    protected $table = "marital_status";
    public function profiles()
    {
        return $this->hasMany('App\Profile');
    }
}
