<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;


class Category extends Model
{
    use NodeTrait;
    protected $fillable = ['name'];

    public function myBook()
    {
        return $this->belongsTo(MyBook::class);
    }

    public function profiles()
    {
        return $this->morphedByMany('App\Profile', 'subject', 'category_subjects')->withTimestamps();
    }

    public function add($profile)
    {
        $this->profiles()->attach($profile->id);
    }

}
