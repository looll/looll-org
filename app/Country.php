<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {

	protected $table = "countries";
	protected $fillable = [];

	public function profiles()
	{
		return $this->hasMany("App/Profile");
	}

	public function groups()
	{
		return $this->hasMany('App\Group');
	}

	public function companies()
    {
        return $this->hasMany(Company::class);
    }
}
