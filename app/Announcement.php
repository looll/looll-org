<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Announcement extends Model {

	protected $table = 'announcements';
	protected $fillable = ['body'];

	public function announcement_able()
	{
		return $this->morphTo();
	}

	public function setPublishedAtAttribute($value)
	{
		if(empty($value))
		{
			$value = Carbon::now()->format('Y-m-d H:i:s');
		}

		$this->attributes['published_at'] = $value;
	}

}
