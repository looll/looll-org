<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Tag extends Model
{
    use Searchable;

    protected $fillable = ["title"];
    public function Company()
    {
        return $this->belongsTo(Company::class);
    }
}
