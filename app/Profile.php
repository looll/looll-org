<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Profile extends Model
{
    use Searchable;
	protected $table = "profiles";
	protected $fillable = [
		"active",
		"name",
		"gender",
		"birthday",
		"description",
		"email",
		"address",
		"zip",
		"city",
		"workplace",
        "position",
        "country_id",
        "marital_status_id"
	];

	public static function boot()
	{
		parent::boot();
	}

	protected $dates = ["created_at", "updated_at", "birthday"];
	protected $touches = ['user'];

    public function toSearchableArray()
    {
        $array = $this->toArray();
        $array['username'] = $this->user->username;
        $array['phoneNumbers'] = $this->phoneNumbers()->get(['phone_number'])->toArray();
        return $array;
    }

	public function announcements()
    {
        return $this->morphMany('App\Announcement', 'announcement_able');
    }

	public function photos()
	{
		return $this->morphMany('App\Photo','photoable');
	}

	public function primaryPhotos()
    {
        return $this->morphMany('App\Photo','photoable')->where('primary_photo', true);
    }

	public function company()
	{
		return $this->belongsTo('App\Company');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function marital_status()
	{
		return $this->belongsTo(MaritalStatus::class);
	}

	public function country()
	{
		return $this->belongsTo('App\Country');
	}

	public function phoneNumbers()
	{
		return $this->morphMany(PhoneNumber::class, "phone_able");
	}

	public function username()
	{
		return $this->user()->first()->username;
	}

	public function addToAnnouncements($announcements, $profileId)
	{
		$profile = $this->findOrNew($profileId);
		$announcement = $profile->announcements()->findOrNew($announcements['id']);
		$announcement->fill($announcements)->save();
		return $announcement->toArray();
	}

    public function setGenderAttribute($value)
    {
        if(empty($value))
        {
            $this->attributes['gender'] = null;
        }
        else{
            $this->attributes['gender'] = $value;
        }
    }

    public function savePhoneNumber($phoneNumber, $phoneType)
    {

        $this->phoneNumbers()->updateOrCreate(['phone_able_id'=>$this->id, 'phone_type'=>$phoneType],[
            'phone_type'=>$phoneType,"phone_number"=> $phoneNumber
        ]);

        /* $this->phoneNumbers()->updateOrCreate(
            ['phone_type'=>$phoneType,"phone_number"=> $phoneNumber]
        );*/

        //$this->phoneNumbers()->updateOrCreate(['profile_id'=>$this->id, 'phone_type'=>$phoneType],['phone_type'=>$phoneType,"phone_number"=> $phoneNumber]);
    }

    public function getBirthdayAttribute($value)
    {
        if($value !== null)
            return Carbon::make($value)->format("Y-m-d");

        return $value;

        //return $value->format("Y-m-d");
    }

    public function isPublished()
    {
        if($this->published_at !== null)
        {
            return true;
        }

        return false;
    }

    public function shouldBeSearchable()
    {
        return $this->isPublished();
    }

	public function categories()
	{
		return $this->morphToMany('App\Category', 'subject', 'category_subjects')->withTimestamps();
	}


    /*
    public function searchableAs()
    {
        return 'profiles_index';
    }*/
}
