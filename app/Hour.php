<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hour extends Model
{
    protected $fillable = ["day", "open_time", "close_time"];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
