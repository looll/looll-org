<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Laravel\Cashier\Billable;
use Laravel\Passport\HasApiTokens;
//use Laravel\Sanctum\HasApiTokens;
use Laravel\Scout\Searchable;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Cashier\Order\Contracts\ProvidesInvoiceInformation;
use Illuminate\Support\Str;



class User extends Authenticatable implements MustVerifyEmail, ProvidesInvoiceInformation
{
    use HasApiTokens, Notifiable, HasRoles, Billable, Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function posts()
    {
        return $this->belongsToMany(Post::class)->withPivot(['read_by']);
    }

    public function unReadPostsCount()
    {
        return $this->posts()->wherePivot('read_by', '=', null)->count();
    }

    public function hasPersonInMyBook($profile)
    {
        $this->load("myBook", "myBook.categories", "myBook.categories.profiles");


        if($this->myBook !== null)
        {
            $myBook = $this->myBook->first();
            $category = $myBook->categories->where('name', 'All')->first();
            if($category != null)
            {
                $profileCount = $category->profiles->where('profiles.id', $profile->id)->count();
                if($profileCount > 0)
                {
                    return true;
                }
            }
        }

        return false;

    }

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function company()
    {
      return $this->hasOne(Company::class);
    }

    public function myBook()
    {
        return $this->hasOne(MyBook::class, 'user_id');
    }

    public function hasMyBook()
    {
        if($this->myBook()->first() == null)
        {
            return false;
        }

        return true;
    }



    public function groups()
    {
        return $this->belongsToMany('App\Group', 'group_role_user')
            ->using(GroupRoleUser::class)->as("role")
            ->withPivot('role_id')
            ->withTimestamps();
    }

    public function roleOnGroup($group)
    {
        return $this->rolesInGroups()->wherePivot("group_id","=", $group->id);
    }

    public function isOwnerOf(Group $group)
    {
        $owner = $group->owner()->first();

        if($owner->id === $this->id)
        {
            return true;
        }

        return false;
    }

    public function isAdminOf(Group $group)
    {
        $admin = $group->admin()->where('users.id', '=', Auth::id())->first();

        if($admin->id === $this->id)
        {
            return true;
        }

        return false;
    }

    public function rolesInGroups()
    {
        return $this->belongsToMany('App\Role', 'group_role_user')
            ->using(GroupRoleUser::class)
            ->as('group')
            ->withPivot('group_id', 'accepted');
    }


    /**
     * Get the receiver information for the invoice.
     * Typically includes the name and some sort of (E-mail/physical) address.
     *
     * @return array An array of strings
     */
    public function getInvoiceInformation()
    {
        return [$this->name, $this->email];
    }

    /**
     * Get additional information to be displayed on the invoice. Typically a note provided by the customer.
     *
     * @return string|null
     */
    public function getExtraBillingInformation()
    {
        return null;
    }

    public function hasRoleByGroup($roles, $group)
    {

        if(is_array($roles))
        {
            foreach ($roles as $role)
            {
                return $this->hasRoleByGroup($role, $group);
            }
        }

        $rolesWithGroupsCount = $this->rolesInGroups()->where('name', $roles)->wherePivot('group_id', $group->id)->count();

        if($rolesWithGroupsCount > 0)
        {
            return true;
        }

        return false;
    }

    public function getGroupsByRole($role, $accepted)
    {
        return $this->groups()
            ->wherePivot('accepted',"=", $accepted)
            ->wherePivot("role_id","=", $role)
            ->get();
    }

    public function canByGroup($permission, $groupId, $requireAll = false)
    {

        if (is_array($permission)) {
            foreach ($permission as $permName) {
                $hasPerm = $this->can($permName);

                if ($hasPerm && !$requireAll) {
                    return true;
                } elseif (!$hasPerm && $requireAll) {
                    return false;
                }
            }

            // If we've made it this far and $requireAll is FALSE, then NONE of the perms were found
            // If we've made it this far and $requireAll is TRUE, then ALL of the perms were found.
            // Return the value of $requireAll;
            return $requireAll;
        } else {
            foreach ($this->cachedGroupRoles($groupId) as $role) {
                // Validate against the Permission table
                foreach ($role->getAllPermissions() as $perm) {
                    if (Str::is( $permission, $perm->name) ) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
    public function cachedGroupRoles($groupId)
    {
        $userPrimaryKey = $this->primaryKey;
        $cacheKey = 'entrust_roles_for_user_'.$this->$userPrimaryKey;

        return Cache::tags('group_role_user')->remember($cacheKey, Config::get('cache.ttl'), function () use($groupId) {
            return $this->rolesInGroups()->wherePivot('group_id', $groupId)->get();
        });
    }

    public function taxPercentage() {
        return 24;
    }
}
