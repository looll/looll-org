<?php
/**
 * Created by PhpStorm.
 * User: jondijam
 * Date: 23.12.2016
 * Time: 10:00
 */

namespace App\Http\Composers;

use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AppComposer
{
    public function __construct()
    {

    }

    public function compose(View $view)
    {

        if(Auth::check())
        {
            $myBookId = 0;
            $user =  Auth::user();


            if($user != null && $user->hasMyBook())
            {
                $myBook = $user->myBook()->first();
                $myBookId = $myBook->id;
            }

            $unReadPost = 0;
            $unReadPost = Auth::user()->unReadPostsCount();;

            $view->with('unReadPost', $unReadPost);
            $view->with('myBookId', $myBookId);
        }
    }
}