<?php


namespace App\Http\Composers;


use App\Company;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CompanyMenuComposer
{
    public function compose(View $view)
    {

        $company = Company::with([])->find(1);
        $view->with('company', $company);
    }
}