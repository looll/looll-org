<?php
/**
 * Created by PhpStorm.
 * User: jondijam
 * Date: 20.3.2016
 * Time: 08:00
 */

namespace App\Http\Composers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Auth;

class HeaderComposer
{
    public function compose(View $view)
    {
        if(Auth::check())
        {
            $myBookId = 0;
            $user =  Auth::user();


            if($user != null && $user->hasMyBook())
            {
                $myBook = $user->myBook()->first();
                $myBookId = $myBook->id;
            }

            $unReadPost = 0;
            $unReadPost = $user->posts()->wherePivot('read_by', '=', null)->get();
            $unReadPost = $unReadPost->count();

            $view->with('unReadPost', $unReadPost);
            $view->with('myBookId', $myBookId);
        }
    }
}