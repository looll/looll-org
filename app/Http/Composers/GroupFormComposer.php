<?php
namespace App\Http\Composers;
use App\Looll\Repo\Country\CountryRepository;
use App\Looll\Repo\Group\GroupRepository;
use App\Looll\Repo\MaritalStatus\MaritalStatusRepository;
use App\Looll\Repo\MemberType\MemberTypeRepository;
use Request;
use Auth;
use App\Looll\Repo\Profile\ProfileRepository;
use Illuminate\View\View;
class GroupFormComposer
{
    protected $groups;
    protected $country;
    protected $memberType;

    public function __construct(GroupRepository $groups, CountryRepository $country, MemberTypeRepository $memberType)
    {
        $this->groups = $groups;
        $this->country = $country;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $announcements = [];
        $contacts = [];
        $countries = [];
        $parents = [];
        $group = [];
        $id = 0;

        $countries = $this->country->getAll();

        $view->with('announcements', $announcements);
        $view->with('contacts', $contacts);
        $view->with('countries', $countries);
        $view->with('id', $id);
    }
}