<?php
namespace App\Http\Composers;

use App\Looll\Repo\Country\CountryRepository;
use App\Looll\Repo\MaritalStatus\MaritalStatusRepository;
use Illuminate\Contracts\View\View;

class ProfileFormComposer {
    protected $countries;
    protected $marital_status_list;

    public function __construct(CountryRepository $countries,
                                MaritalStatusRepository $marital_status_list)
    {
        $this->countries = $countries;
        $this->marital_status_list = $marital_status_list;
    }

    public function compose(View $view)
    {
        $countries = $this->countries->getAll();
        $marital_status_list = $this->marital_status_list->getAll();

        $view->with('countries', $countries);
        $view->with('marital_status_list', $marital_status_list);

    }
}