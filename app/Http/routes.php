<?php
Route::group(['middleware' => ['web']], function ()
{
    //"middleware"=>['role:admin', 'auth']
    Route::group(["prefix"=>"admin", "namespace"=>"Admin", "middleware"=>['role:admin', 'auth']], function ()
    {
        Route::get('/', ['as'=>'admin.dashboard', "uses"=>'AdminController@index']);
        Route::resource('users', 'UsersController');
        Route::resource('groups', 'GroupsController');
        Route::resource('companies', 'CompaniesController');
    });

    Route::get('/', ['as'=>'home', 'uses'=>'HomeController@index']);
    Route::get('/about', ['as'=>'about', 'uses'=>'PagesController@about']);
    Route::get('/disclaimer', ['as'=>'disclaimer', 'uses'=> 'PagesController@disclaimer']);
    Route::auth();
    Route::get('confirm', ['as'=>'auth.confirm', 'uses'=>'Auth\AuthController@confirm']);
    Route::get('auth/confirmation/{code}', ['as'=>"auth.confirmation", 'uses'=>'Auth\AuthController@getConfirmation']);
    Route::resource('announcements', 'AnnouncementsController');
    Route::resource('messages', 'MessagesController');
    Route::resource('posts', 'PostsController');
    Route::resource('categories', 'CategoriesController');
    Route::resource('groups', 'GroupsController');
    Route::resource('groups.users', 'GroupUserController', ['only' => ['store', 'update','destroy']]);
    Route::resource('groups.children', 'GroupChildController', ['only'=>['store','update','destroy']]);
    Route::resource('groups.announcements', 'GroupAnnouncement');
    Route::resource('groups.posts', 'GroupPostController');
    Route::resource('myBooks', 'MyBookController');
    Route::resource('myBooks.profiles', 'MyBookProfileController', ['only'=>'store']);
    Route::resource('searches', 'SearchesController');
    Route::get('/{profiles}', ['as' => 'profiles', 'uses' => 'ProfilesController@show']);
    Route::resource('profiles', 'ProfilesController');
    Route::resource('profiles.categories', 'ProfileCategoryController');
    Route::resource('photos', 'PhotosController',['only'=>['store','update','destroy']]);
    Route::resource('users', 'UsersController');
    Route::resource('users.profiles', 'UserProfileController', ['only' => ['store', 'update', 'destroy']]);
});

Route::group(['middleware' => ['api','auth:api'], 'prefix'=>'api', 'namespace'=>"Api"], function ()
{
    Route::resource('visitors', 'VisitorsController');
    Route::resource('photos', 'PhotosController',['only'=>['store','update','destroy']]);
    Route::resource('categories', 'CategoriesController');
    Route::resource('profiles.photos', 'ProfilePhotoController');
    Route::resource('groups', 'GroupsController');
    Route::resource('groups.photos', 'GroupPhotoController');
    Route::resource('groups.users', 'GroupUserController');
    Route::resource('profiles.categories', 'ProfileCategoryController');
    Route::resource('users.profiles', 'UserProfileController', ['only' => ['store', 'update', 'destroy']]);
});