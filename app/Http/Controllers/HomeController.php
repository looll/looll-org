<?php namespace App\Http\Controllers;

use App\Looll\Repo\Country\CountryRepository;
use App\Looll\Repo\Group\GroupRepository;
use App\Looll\Repo\User\UserRepository;
use App\User;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller {
	public function index()
	{
	    $user = Auth::user();
	    //$user = User::with([])->find($user->id);

		return view('welcome');
	}
}