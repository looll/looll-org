<?php

namespace App\Http\Controllers;

use App\Announcement;
use App\Group;
use App\Profile;
use Illuminate\Http\Request;

use App\Http\Requests;

class AnnouncementsController extends Controller
{
    public function index()
    {
        return ["Announcments"];
    }

    public function store(Request $request)
    {
        $this->validate($request, ["announcement.body"=>"required"]);
        $profileId = $request->get('profile_id');
        $groupId = $request->get('group_id');
        $subject = null;
        $url = "";

        if(isset($profileId))
        {
            $url = route('profiles.edit', ['profiles'=>\Auth::user()->username, 'tab="announcement"']);
            $subject = Profile::find($profileId);
        }
        elseif(isset($groupId))
        {
            $url = route('groups.edit', ['groups'=>$groupId, 'tab="announcement"']);
            $subject = Group::find($groupId);
        }

        $announcement = new Announcement($request->get('announcement'));
        $announcement->announcement_able()->associate($subject);
        $announcement->save();


        return redirect($url);

        /*$this->validate($request, ["announcement.body"=>"required"]);
        $profileId = $request->get('profile_id');
        $groupId = $request->get('group_id');

        if(isset($profileId))
        {
            $subject = Profile::with([])->findOrNew($profileId);
            //$profile->announcements()->create($request->get('announcement'));
        }
        else
        {
            $subject = Group::with('')->find($groupId);
        }

        dd($subject);

        $announcement = new Announcement($request->get('announcement'));
        $announcement->announcement_able()->associate($subject);
        $announcement->save();

        return redirect()->back();*/

        /*$profileId = $request->get('profile_id');
        Profile::with('')->find($profileId)->announcements()->create($request->get('announcement'));
        return redirect()->back();*/

    }

    public function update(Request $request, Announcement $announcements)
    {
        $url = "";
        $profileId = $request->get('profile_id');
        $groupId = $request->get('group_id');

        if(isset($profileId))
        {
            $url = route('profiles.edit', ['profiles'=>\Auth::user()->username, 'tab="announcement"']);
        }
        elseif(isset($groupId))
        {
            $url = route('groups.edit', ['groups'=>$groupId, 'tab'=>'announcement']);
        }

        $this->validate($request, ["announcement.body"=>"required"]);
        $announcements->update($request->get('announcement'));


        return redirect($url);
    }
}
