<?php

namespace App\Http\Controllers;

use App\Company;
use App\Tag;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class CompanyAnnouncementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Company $company
     * @return Application|Factory|View
     */
    public function index(Company $company)
    {
        $announcement = $company->announcements->first();
        return view('companies.announcements.index', compact("company", "announcement"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param Company $company
     * @return RedirectResponse
     */
    public function store(Request $request, Company $company)
    {
        $request->validate(["body"=>["required"]]);

        $announcement = $company->announcements()->first();

        if($announcement === null)
            $company->announcements()->create($request->all());


        dd($announcement);

        $announcement->update($request->all());
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Company $company
     * @param Tag $tag
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Company $company, Tag $tag)
    {
        dd("hi");

    }
}
