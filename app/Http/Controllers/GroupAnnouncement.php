<?php

namespace App\Http\Controllers;

use App\Looll\Repo\Announcement\AnnouncementRepository;
use App\Looll\Repo\Group\GroupRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GroupAnnouncement extends Controller
{
    private $announcement;
    private $group;

    public function __construct(AnnouncementRepository $announcement, GroupRepository $group)
    {
        $this->announcement = $announcement;
        $this->group = $group;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $groupId)
    {
        $data = $request->all();
        $group = $this->group->getById($groupId);
        $data = $this->announcement->create($data, $group);
        $data['url'] = route('groups.announcements.update', ['groups'=>$groupId,'announcements'=>$data->id]);
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $groupId
     * @param int $announcementId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $groupId, $announcementId)
    {
        $data = $request->all();
        $group = $this->group->getById($groupId);
        $data = $this->announcement->update($data, $announcementId, $group);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
