<?php namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Looll\Repo\MyBook\MyBookRepository;
use App\MyBook;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class MyBookController extends Controller {

	protected $myBook;
	public function __construct(MyBookRepository $myBook)
	{
		$this->myBook = $myBook;
		$this->middleware('user.has.myBook',['only'=>['create']]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        ///

		return view('myBook.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('myBook.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * @param Request $request
	 * @return mixed
	 */
	public function store(Request $request)
	{
		$this->validate($request, ['name'=>'required']);
		$user = $request->user();
		$data = $request->all();

        $myBook = new MyBook($data);
		$myBook->owner()->associate($user);
		$myBook->save();

        $myBook = MyBook::find($myBook->id);
        $parent = new Category(['name'=>'All', 'slug'=>Str::slug("All")]);
        $parent->myBook()->associate($myBook);
        $parent->save();

		$category = new Category(['name'=>"Family", "slug"=>Str::slug("Family")]);
		$category->parent()->associate($parent);
		$category->myBook()->associate($myBook);
		$category->save();

		$category2 = new Category(['name'=>"Friends", "slug"=>Str::slug("Friends")]);
		$category2->parent()->associate($parent);
		$category2->myBook()->associate($myBook);
		$category2->save();

		return redirect()->route('myBooks.show', ['myBook' => $myBook->id]);
	}

	public function show(MyBook $myBook)
	{
	    $myBook->load(['categories' => function($query)
        {
            $query->orderBy('name', 'ASC');
        }, 'categories.profiles']);
	    $categories = $myBook->categories->all();
	    $parent = $myBook->categories->where('name', 'All')->first();
	    $parentId = $parent->id;

	    return view('myBook.show', compact('categories', 'parentId', 'myBook'));

	    //$categories->orderBy('id', 'DESC');

	    /*
		$myBook = $this->myBook->getById($id, ['categories']);
		$categories = $myBook->categories()->with('profiles')->orderBy("name", 'ASC')->get();
		$parent = $myBook->categories()->where('name', 'All')->first();
		$parentId = $parent->id;
		$activeCategory = $myBook->categories()->where('name', 'all')->with('profiles')->first();
		return view('myBook.show', compact('categories', 'activeCategory', 'parentId', 'myBook'));*/
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
