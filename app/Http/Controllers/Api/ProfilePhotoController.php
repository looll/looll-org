<?php

namespace App\Http\Controllers\Api;

use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Image;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProfilePhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \Auth::guard("api")->user();
        $user->load("profile.photos");
        //$user = User::with('profile.photos')->find($user->id);
        return $user->profile->photos;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Profile $profile
     *
     * @return string
     */
    public function store(Request $request, Profile $profile)
    {

        $this->validate($request, ["file*"=>"required|image"]);
        $user = Auth::guard('api')->user();


        $file = $request->file("file");
        $path = $file->store('photos/'. $user->username, ["disk"=>"public"]);
        $url =  Storage::disk("photos")->url($path);
        $filename = $file->getFilename();


        $size = "200x200";
        $pathThumbnail = $file->hashName('photos/'.$user->username.'/'.$size);
        $img = Image::make($file->getRealPath());
        $img->resize(200, null, function ($constraint) {
            $constraint->aspectRatio();
        })->crop(200, 200)->encode('jpg',80);

        $primary_photo = false;
        $photosCount = $profile->photos->count();
        $photosCount > 0 ? $primary_photo = false : $primary_photo = true;
        Storage::disk('public')->put($pathThumbnail,  $img);
        $thumbnailUrl = Storage::disk('public')->url($pathThumbnail);
        //Storage::disk()
        $data = [
            "filename"=>$filename,
            "path" => $path,
            "src" => $url,
            "thumbnail_src"=>$thumbnailUrl,
            "thumbnail_path"=>$pathThumbnail,
            "primary_photo" => $primary_photo
        ];

        $photoData = $profile->photos()->create($data);
        return $photoData;


        /*$this->validate($request, ["file"=>"required|image"]);
        $file =  $request->file('file');

        $filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $newFileName = time().str_random(60).'.'.$extension;
        $img = Image::make($file->getRealPath());
        \File::exists(users_photo_path()) or \File::makeDirectory(users_photo_path());
        $img->save(users_photo_path().$newFileName);

        $img->resize(200, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save(users_photo_path().'200x200-'.$newFileName);

        $photosCount = $profile->photos->count();
        $photosCount > 0 ? $primary_photo = false : $primary_photo = true;
        $data = [
            "filename"=>$filename,
            "path" => users_photo_path().$newFileName,
            "src" => users_photo_src($newFileName),
            "thumbnail_src" =>users_photo_src('200x200-'.$newFileName),
            "primary_photo" => $primary_photo
        ];
        $photo = $profile->photos()->create($data);

        return $photo;
        /*
        if($img->width() > 800)
        {
            $img->resize(800, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->fit(800);
        }
        $img->save(users_photo_path().$newFileName);

        $img->resize(200, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        $img->save(users_photo_path().'200x200-'.$newFileName);
        $photosCount = $profiles->photos()->count();
        $photosCount > 0 ? $primary_photo = false : $primary_photo = true;
        $data = [
            "filename"=>$filename,
            "path" => users_photo_path().$newFileName,
            "src" => users_photo_src($newFileName),
            "thumbnail_src" =>users_photo_src('200x200-'.$newFileName),
            "primary_photo" => $primary_photo
        ];

        return $profiles->photos()->create($data);*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
