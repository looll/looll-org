<?php

namespace App\Http\Controllers\Api;

use App\Profile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Profile $profile
     * @return Profile
     * @internal param int $id
     */
    public function show(Profile $profile)
    {
        $profile->load(['marital_status', 'country', 'phoneNumbers']);
        $data['profile'] = $profile;
        $data['business'] = $profile->phoneNumbers()->where('phone_type', 'business')->first();
        $data['mobile'] = $profile->phoneNumbers()->where('phone_type', 'mobile')->first();
        $data['home'] = $profile->phoneNumbers()->where('phone_type', 'home')->first();

        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Profile $profile
     * @return void
     */
    public function edit(Profile $profile)
    {
        $profile->load('marital_status', 'country', 'phoneNumbers');
        $phoneNumber = $profile->phoneNumbers->where('phone_type', 'phone')->first();
        $businessPhoneNumber = $profile->phoneNumbers->where('phone_type', 'business')->first();

        $data['profile'] = $profile;
        $data['profile']["phone_number"] = $phoneNumber->phone_number ?? "";
        $data['profile']['business_phone_number'] = $businessPhoneNumber->phone_number ?? "";

        return $data;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {
        $profileData = $request->get('profile');
        $published = $request->get("published");

        $profile->update($profileData);
        $profile->country()->associate($profileData['country_id']);
        $profile->marital_status()->associate($profileData["marital_status_id"]);
        if($published === 1){
            $profile->published_at = now()->format("Y-m-d H:i:s");
        }
        else {
            $profile->published_at = null;
        }
        $profile->save();

        $phoneNumber = $profileData["phone_number"];
        $businessPhoneNumber = $profileData["business_phone_number"];
        if($phoneNumber !== null)
            $profile->savePhoneNumber($phoneNumber, "phone");

        if($businessPhoneNumber !== null)
            $profile->savePhoneNumber($businessPhoneNumber, "business");
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
