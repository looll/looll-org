<?php

namespace App\Http\Controllers\Api;

use App\Events\CreatedPost;
use App\Events\Event;
use App\Group;
use App\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index(Group $group, Request $request)
    {
        $group->load('posts');

        $drafts = $group->posts->where('published_at','=', null)->flatten()->all();
        $pendingPosts = $group->posts()->where('published_at','>', Carbon::now()->format('Y-m-d'))->get();
        $posts = $group->posts->where("published_at", "!=", null)->flatten()->all();


        $data = [];
        foreach ($posts as $post)
        {
            $post->published_at_humans = $post->published_at->diffForHumans();
        }

        $data['drafts'] = $drafts;
        $data['posts'] = $posts;

        return $data;

        /*$drafts = $group->posts()->where('draft', true)->get();
        $pendingPosts = $group->posts()->where('draft', false)->where('published_at','>', Carbon::now()->format('Y-m-d'))->get();
        $posts = $group->posts()->where('draft', false)->get();

        $data = [];
        foreach ($posts as $post)
        {
            $post->published_at_humans = $post->published_at->diffForHumans();
        }


        $data['drafts'] = $drafts;
        $data['posts'] = $posts;

        return $data;*/

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Group $group)
    {
        $this->validate($request,[
            'subject' => 'required|max:50',
            'body' => 'required',
        ]);

        $post = $group->posts()->create($request->all());
        $post = Post::with('postable')->find($post->id);
        $postable = $post->postable->first();

        if($post->published_at !== null)
        $post->published_at_humans = $post->published_at->diffForHumans();

        if($request->get('published_at'))
        {
            CreatedPost::dispatch($post, $postable);
        }

        return $post;

        /*$post = Post::find($post->id);
        if($request->get('publish'))
        {
            //\Event::fire(new CreatedPost($post, $group));
        }*/


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group, Post $post)
    {

        if( !empty($request->get('published_at') ))
        {
            $postArray = [
                'subject'=>$request->get('subject'),
                'body'=>$request->get('body'),
                'draft'=>false,
                "published_at" => Carbon::now()->format('Y-m-d H:i:s')
            ];
        }
        else
        {
            $postArray = [
                'subject'=>$request->get('subject'),
                'body'=>$request->get('body')
            ];
        }


        $post->update($postArray);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
