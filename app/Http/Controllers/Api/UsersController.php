<?php

namespace App\Http\Controllers\Api;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->load('profile.photos', 'myBook.categories.profiles');
        $owner = Role::where('name', 'owner')->first();
        $admin = Role::where('name', 'admin')->first();
        $groups = $user->groups()->wherePivot('role_id', $owner->id)->get();
        foreach ($groups as $group)
        {
            $numberOfAdminOfGroups = $group->users()->wherePivot('role_id', $admin->id)->count();
            if($numberOfAdminOfGroups === 0)
            {
                $group->delete();
            }
        }

        $user->groups()->detach();

        if($user->profile != null)
        {
            foreach ($user->profile->photos as $photo)
            {
                $photo->delete();
            }
        }

        if($user->profile != null)
        {
            $user->profile()->delete();
        }


        if($user->myBook != null)
        {
            $categories = $user->myBook->categories;
            foreach ($categories as $category)
            {
                $category->profiles()->detach();
            }

            $user->myBook()->delete();
        }

        $user->delete();
    }
}
