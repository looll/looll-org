<?php

namespace App\Http\Controllers\Api;

use App\Photo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class PhotosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Photo $photo
     * @internal param int $id
     */
    public function update(Request $request, Photo $photo)
    {
        //$user = \Auth::guard('api')->user();
        $owner = $photo->photoable->with('photos')->first();
        $photos = $owner->photos;

        foreach ($photos as $item)
        {
            $item->primary_photo = false;
            $item->save();
        }

        $photo->primary_photo = true;
        $photo->save();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Photo $photo
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Photo $photo)
    {
        Storage::disk('public')->delete([$photo->path, $photo->thumbnailPath]);


        $photo->delete();
    }
}
