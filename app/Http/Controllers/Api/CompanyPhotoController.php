<?php

namespace App\Http\Controllers\Api;

use App\Company;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Image;

class CompanyPhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \Auth::guard('api')->user();
        $user = User::with('profile.photos')->find($user->id);

        return $user->company->photos;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param Company $company
     * @return Model
     */
    public function store(Request $request, Company $company)
    {
        $user = $company->user;

        $file = $request->file('file');
        $path = $file->store('photos/' . $user->username, ["disk" => "public"]);
        $url = Storage::disk("photos")->url($path);
        $filename = $file->getFilename();

        $size = "200x200";
        $pathThumbnail = $file->hashName('photos/' . $user->username . '/' . $size);
        $img = \Image::make($file->getRealPath());
        $img->resize(200, null, function ($constraint) {
            $constraint->aspectRatio();
        })->crop(200, 200)->encode('jpg', 80);

        $primary_photo = false;
        $photosCount = $company->photos->count();
        $photosCount > 0 ? $primary_photo = false : $primary_photo = true;

        Storage::disk('public')->put($pathThumbnail, $img);
        $thumbnailUrl = Storage::disk('public')->url($pathThumbnail);

        $data = [
            "filename" => $filename,
            "path" => $path,
            "src" => $url,
            "thumbnail_src" => $thumbnailUrl,
            "thumbnail_path" => $pathThumbnail,
            "primary_photo" => $primary_photo
        ];

        return $company->photos()->create($data);

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}