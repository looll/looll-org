<?php

namespace App\Http\Controllers\Api;

use App\Group;
use App\Looll\Repo\Group\GroupRepository;
use App\Looll\Repo\Role\RoleRepository;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GroupUserController extends Controller
{

    private $group;
    private $role;
    public function __construct(GroupRepository $group, RoleRepository $role)
    {

        $this->group = $group;
        $this->role = $role;


    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param Group $group
     * @return \Illuminate\Support\Collection
     */
    public function index(Request $request, Group $group)
    {
        $type = $request->get('type');

        $memberRole = $this->role->getFirstBy('name', 'Member');
        $contactRole = $this->role->getFirstBy('name', 'Contact');

        if($type === "contacts")
        {
            $contacts = $group->users()->wherePivot('role_id','=', $contactRole->id)->get();
            return $contacts->pluck('id');
        }


        $members = $group->users()->wherePivot('role_id','=', $memberRole->id)
            ->orWherePivot('role_id', '=', $contactRole->id)->get();
        return $members;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $groupId)
    {
        $user = \Auth::guard('api')->user();
        $role = $this->role->getFirstBy('name','member');
        $this->group->attachUser($user->id, $groupId, $role->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Group $groups
     * @param User $users
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Request $request, Group $group, User $user)
    {
        $role = $request->get('role', 'Member');
        $role = $this->role->getFirstBy('name', $role);
        $accepted = $request->get('accepted');
        $group->users()->updateExistingPivot($user->id,['role_id'=>$role->id,'accepted'=>$accepted]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Group $groups
     * @param User $users
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Group $group, User $user)
    {
        $group->users()->detach($user->id);
    }
}
