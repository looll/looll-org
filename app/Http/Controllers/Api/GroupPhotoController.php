<?php
namespace App\Http\Controllers\Api;

use App\Group;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class GroupPhotoController extends Controller
{
    public function index(Group $group)
    {
        return $group->photos;
    }

    public function store(Request $request, Group $group)
    {
        $file = $request->validate([
            'file*' => ['required', 'image'],
        ]);

        $user = Auth::guard('api')->user();

        $slugName = Str::slug($group->name);

        $primary_photo = false;
        $file = $request->file("file");
        $path = $file->store('photos/'. $slugName, ["disk"=>"public"]);
        $url =  Storage::disk("photos")->url($path);
        $filename = $file->getFilename();

        $size = "200x200";
        $pathThumbnail = $file->hashName('photos/'.$slugName.'/'.$size);
        $img = Image::make($file->getRealPath());
        $img->resize(200, null, function ($constraint) {
            $constraint->aspectRatio();
        })->crop(200, 200)->encode('jpg',80);

        $photosCount = $group->photos->count();
        $photosCount > 0 ? $primary_photo = false : $primary_photo = true;
        Storage::disk('public')->put($pathThumbnail,  $img);
        $thumbnailUrl = Storage::disk('public')->url($pathThumbnail);
        $data = [
            "filename"=>$filename,
            "path" => $path,
            "src" => $url,
            "thumbnail_src"=>$thumbnailUrl,
            "thumbnail_path"=>$pathThumbnail,
            "primary_photo" => $primary_photo
        ];

        $photoData = $group->photos()->create($data);
        return $photoData;



        /*$this->validate($request, ["file"=>"required|image"]);
        $file =  $request->file('file');

        $filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $newFileName = time().str_random(60).'.'.$extension;

        $img = Image::make($file->getRealPath());

        \File::exists(groups_photo_path($group)) or \File::makeDirectory(groups_photo_path($group));

        if($img->width() > 800)
        {
            $img->resize(800, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->fit(800);
        }
        $img->save(groups_photo_path($group).$newFileName);

        $img->resize(200, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        $img->save(groups_photo_path($group).'200x200-'.$newFileName);
        $photosCount = $group->photos()->count();
        $photosCount > 0 ? $primary_photo = false : $primary_photo = true;
        $data = [
            "filename"=>$filename,
            "path" => groups_photo_path($group).$newFileName,
            "src" => groups_photo_src($newFileName, $group),
            "thumbnail_src" =>groups_photo_src('200x200-'.$newFileName, $group),
            "primary_photo" => $primary_photo
        ];

        return $group->photos()->create($data);*/
    }
}
