<?php

namespace App\Http\Controllers\Api;

use App\Announcement;
use App\Group;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupAnnouncementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Group $group)
    {
        $group->load('announcements');
        return $group->announcements->first();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Group $group
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function store(Group $group, Request $request)
    {
        $this->validate($request, ['body'=>"required"]);
        $group->announcements()->delete();
        $announcement = $group->announcements()->create($request->all());
        return $announcement;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Group $group
     * @param Announcement $announcement
     */
    public function update(Request $request, Group $group, Announcement $announcement)
    {
        $this->validate($request, ['body'=>"required"]);
        $announcement->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
