<?php

namespace App\Http\Controllers\Api;

use App\Category;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Looll\Repo\MyBook\MyBookRepository;
use App\MyBook;

class CategoriesController extends Controller
{

    private $myBook;
    public function __construct(MyBookRepository $myBook)
    {
        $this->myBook = $myBook;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \Auth::guard('api')->user();

        $myBook = $this->myBook->getById($user->myBook->id, [
            'categoriesOrderByAndGet.profiles.primaryPhotos',
            'categoriesOrderByAndGet.profiles.user',
            'categoriesOrderByAndGet.profiles.categories'=>function($q)
            {
                $q->get(['categories.id', 'name']);
            }
        ]);
        $categories= $myBook->categoriesOrderByAndGet->all();
        return $myBook->categoriesOrderByAndGet->all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['name'=>'required']);
        $myBookId = $request->get('my_book_id');
        $parentId = $request->get('parent_id');

        $category = new Category($request->all());
        $category->myBook()->associate($myBookId);
        $category->parent()->associate($parentId);
        $category->save();

        $category = $category->with('profiles')->find($category->id);

        return $category->toJson();

        /*
        $myBookId = $request->get('my_book_id');
        $parentId = $request->get('parent_id');

        $category = new Category($request->all());
        $category->myBook()->associate($myBookId);
        $category->parent()->associate($parentId);
        $category->save();

        $category = $category->with('profiles')->find($category->id);

        return $category->toJson();*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @internal param int $id
     */
    public function destroy(Category $category)
    {
        $category->delete();
    }
}
