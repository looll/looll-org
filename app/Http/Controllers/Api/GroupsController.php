<?php

namespace App\Http\Controllers\Api;

use App\Group;
use App\Looll\Repo\Group\GroupRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GroupsController extends Controller
{

    protected $group;

    public function __construct(GroupRepository $group)
    {
        $this->group = $group;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(Request $request)
    {
        $type = $request->get('type');

        $groups = Group::with([])->whereHas('memberType', function($q){
            $q->where('name', 'members.only');
        })->get();

        return $groups;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function show(Group $group)
    {
        $group->load(['members', 'country', 'memberType', 'posts', 'announcements', 'parent']);

        return $group;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Group $group
     * @internal param int $id
     */
    public function update(Request $request, Group $group)
    {
        $this->validate($request, [
            'name' => "required",
            "email" => "nullable|email",
            "address"=> "nullable",
            "city" => "nullable",
            "zip" => "nullable",
            "description" => "nullable"
        ]);

        $group->update($request->all());
        $group->country()->associate($request->get('country_id'));
        $group->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
