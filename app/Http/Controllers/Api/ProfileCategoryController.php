<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\Profile;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProfileCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $profileId
     * @param $categoryId
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Request $request, Profile $profile, Category $category)
    {

        $profile->categories()->attach($category);
        //Profile::findOrNew($profileId)->categories()->attach($categoryId);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param $profileId
     * @param $categoryId
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy( $profileId, $categoryId)
    {
        Profile::findOrNew($profileId)->categories()->detach($categoryId);
    }
}
