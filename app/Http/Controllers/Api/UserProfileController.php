<?php

namespace App\Http\Controllers\Api;

use App\Looll\Repo\Profile\ProfileRepository;
use App\Profile;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserProfileController extends Controller
{
    private $profiles;
    public function __construct(ProfileRepository $profiles)
    {
        $this->profiles = $profiles;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\SaveProfileRequest|Request $request
     * @param $username
     * @param Profile $profiles
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Requests\SaveProfileRequest $request, $username, Profile $profile)
    {
        $data = $request->all();
        $home_phones = $data['home'];
        $mobile_phones = $data['mobile'];
        $business_phones = $data['business'];
        $profileData = $request->get('profile');
        $maritalStatusId = 0;
        if(isset($profileData['marital_status_id']))
        {
            $maritalStatusId = $profileData['marital_status_id'];
        }

        $country_id = 0;
        if(isset($profileData['country_id']))
        {
            $country_id = $profileData['country_id'];
        }
        
        $home = [];
        $mobile = [];
        $business = [];
        $result = [];

        if(!empty($home_phones['phone_number']))
        {
            $home['home'] = $profile->addToPhoneNumbers($home_phones, $profile->id);
        }

        if(!empty($mobile_phones['phone_number']))
        {
            $mobile['mobile'] = $profile->addToPhoneNumbers($mobile_phones, $profile->id);
        }

        if(!empty($business_phones['phone_number']))
        {
            $business['business'] = $profile->addToPhoneNumbers($business_phones, $profile->id);
        }

        $profile->update($profileData);
        if($country_id > 0)
        {
            $profile->country()->associate($country_id)->save();
        }

        if($maritalStatusId > 0)
        {
            $profile->marital_status()->associate($maritalStatusId)->save();
        }

        $result = array_merge($home, $mobile);
        $result = array_merge($result, $business);

        return json_encode($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
