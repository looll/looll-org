<?php namespace App\Http\Controllers;

use App\Group;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Looll\Repo\Role\RoleRepository;
use App\MemberType;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use function Aws\map;

class GroupsController extends Controller {

    protected $role;

    public function __construct(RoleRepository $role)
    {
        $this->role = $role;
        $this->middleware("auth")->except("show");
    }

    public function index()
    {
        $role = $this->role->getFirstBy('name', 'Member');
        $user = Auth::user();
        $groupsByMember = $user->getGroupsByRole($role->id, $accepted = 1);
        $pendingGroupsByMember = $user->getGroupsByRole($role->id, $accepted = 0);

        $role = $this->role->getFirstBy('name', 'Admin');
        $groupsByAdmin =  $user->getGroupsByRole($role->id, $accepted = 1);

        $role = $this->role->getFirstBy('name', 'Owner');
        $groupsByOwner =  $user->getGroupsByRole($role->id, $accepted = 1);

        if($groupsByOwner !== null && $groupsByAdmin !== null)
            $groupsByOwner = $groupsByOwner->merge($groupsByAdmin);

        return view('groups.index', compact('groupsByMember', 'groupsByOwner', 'pendingGroupsByMember'));
    }

    public function create()
    {
        $memberTypes = MemberType::all();
        return view('groups.create', compact("memberTypes"));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => ["required", "max:255"],
            "email" => ["required", "email", "max:255"]
        ]);

        $user = Auth::user();
        $role = Role::findByName("Owner");

        $countryId = $request->get("country_id");
        $group = new Group($request->all());
        $group->country()->associate($countryId);
        $group->save();
        $group->users()->attach($user->id, ["role_id"=>$role->id, "accepted"=>true]);

        return redirect()->route("groups.index");
    }

    public function show(Group $group)
    {
        $isAdmin = false;
        $groupUser = null;
        $userId = 0;
        $group->load(["users", "users.rolesInGroups"=>function($query) use($group)
        {
            $query->wherePivot('group_id', $group->id);
        }]);


        if(Auth::check())
        {
            $user = Auth::user();
            $userId = Auth::user()->id;

            $groupUser =  $group->users->where('username', "=", $user->username)->first();
            if($groupUser != null)
            {
                $isAdmin = $groupUser->hasRoleByGroup("Owner", $group);
            }
        }

        return view("groups.show", compact('group', "isAdmin", "groupUser", "userId"));
    }

    public function edit(Request $request, Group $group)
    {
        $group->load([
            'members',
            'owner',
            'members.profile',
            'owner.profile',
            'contacts',
            'contacts.profile',
            'memberType',
            'parent',
            'children'
        ]);

        $parent = null;
        if($group->parent !== null)
        {
            $parent = $group->parent;
        }
        $membersOnlyGroups = Group::all();
        $photosIndexUrl = route('api.groups.photos.index', ['group'=>$group->id]);
        $photosStoreUrl = route('api.groups.photos.store', ['group'=>$group->id]);
        $announcementUrl = route('groups.announcements.store', ['group'=>$group->id]);
        $role = Role::findByName("Owner");
        $announcementId = 0;
        $owner = $group->owner()->first();

        $role2 = Role::findByName("Member");
        $role = Role::findByName("Admin");

        $members = $group->members()->get();
        $admin = $group->admin()->where('users.id', '!=', Auth::id())->get();
        $members = $members->merge($admin->all());
        //$members = $members->all();

        $children = $group->children->all();
        $descendants = null;
        $childMembers = [];

        return view('groups.edit', compact(
            'group',
            'parent',
            'membersOnlyGroups',
            'members',
            'children',
            "role",
            'descendants',
            'photosIndexUrl',
            'photosStoreUrl',
            'announcementUrl',
            'announcementId'
        ));



        /*

        $photosIndexUrl = route('api.groups.photos.index', ['group'=>$group->id]);
        $photosStoreUrl = route('api.groups.photos.store', ['group'=>$group->id]);
        $announcementUrl = route('groups.announcements.store', ['group'=>$group->id]);
        $announcementId = 0;
        $groupMembers = $group->members->all();

        return view('groups.edit', compact('group', 'parent', "groupMembersOnly", 'groupMembers',
        'photosIndexUrl', 'announcementId', 'photosStoreUrl', 'announcementUrl'));

        */
    }

    public function update()
    {

    }

    public function destroy($id)
    {
        
    }
}