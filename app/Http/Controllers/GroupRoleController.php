<?php

namespace App\Http\Controllers;

use App\Group;
use App\Role;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GroupRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param Group $group
     * @param Role $role
     */
    public function update(Request $request, Group $group, Role $role)
    {
        $currentUser = Auth::user();

        $memberRole = Role::findByName('Member');
        $userId = $request->get('user_id');
        $user = \App\User::with([])->find($userId);



        if($group->isRoot())
        {
            $group->users()->detach($currentUser->id);
            if($user->roleOnGroup($group)->count() > 1)
            {
                $group->users()->updateExistingPivot($userId, ['role_id'=>$role->id]);
            }
            else {
                $group->users()->attach($userId, ['role_id'=>$role->id]);
            }



            return redirect()->route('groups.index');
        }

        $group->users()->updateExistingPivot($userId, ['role_id'=>$role->id]);
        $group->users()->updateExistingPivot($currentUser->id, ['role_id'=>$memberRole->id]);


        return redirect()->back('group.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
