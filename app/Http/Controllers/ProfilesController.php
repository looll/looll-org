<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Looll\Repo\MaritalStatus\MaritalStatusRepository;
use App\Looll\Repo\User\UserRepository;
use App\User;
use App\Profile;


use App\Looll\Repo\Profile\ProfileRepository as ProfileRepo;
use App\Looll\Repo\Group\GroupRepository as Group;
use App\Looll\Repo\Country\CountryRepository as Country;
use App\Looll\Repo\PhoneNumber\PhoneNumberRepository as PhoneNumber;

use \Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Laravel\VaporCli\Commands\DatabaseDeleteCommand;

class ProfilesController extends Controller
{
    protected $users;
    protected $profile;
    protected $organisation;
    protected $phoneNumber;
    protected $country;
    protected $maritalStatus;

    public function __construct(
        Profile $profile,
        Group $group,
        Country $country,
        PhoneNumber $phoneNumber,
        UserRepository $users,
        MaritalStatusRepository $martialStatus
    )
    {
        $this->profile = $profile;
        $this->country = $country;
        $this->group = $group;
        $this->phoneNumber = $phoneNumber;
        $this->users = $users;
        $this->maritalStatus = $martialStatus;

        /*$this->middleware('auth', ['only'=>["edit",'store','create','update']]);
        $this->middleware("currentUser",["only"=>["edit","store","create","update"]]);
        $this->middleware("checkUser", ["only"=>["show"]]);
        $this->middleware("checkProfile", ["only"=>["show"]]);*/
        $this->middleware('auth', ['only'=>["edit",'store','create','update']]);
        $this->middleware('verified', ['only'=>["edit",'store','create','update']]);
        $this->middleware("user.has.profile", ['only'=>["edit"]]);
        //$this->middleware('check.profile', ["only"=>["show"]]);

        //$this->middleware('check.profile', ['except'=>['create', 'store']]);
    }

    public function create()
    {
        return view('profile.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            "name"=>["required", "string"],
            "email" => ['required', 'email', 'unique:profile'],
            "address" => ['string', 'nullable'],
            "zip" => ["numeric", "required_if:address, city", "nullable"],
            "city" => ["string", "required_if:address, zip", "nullable"],
            "country_id" => ["integer"],
            "gender" => ["alpha"],
            "birthday"=>["date", "nullable"],
            "phone_number" => ["numeric", "digits:10", "nullable"]
        ]);

        $user = Auth::user();
        $countryId = $request->get('country_id');
        $maritalStatusId = $request->get('marital_status_id');
        $phoneNumber = $request->get('phone_number');
        $businessPhoneNumber = $request->get('business_phone_number');

        $profile = new Profile($request->all());
        $profile->user()->associate($user);
        $profile->country()->associate($countryId);
        $profile->marital_status()->associate($maritalStatusId);
        $profile->save();

        if($phoneNumber != null)
            $profile->phoneNumbers()->create(['phone_number'=>$phoneNumber, 'phone_type'=>"phone"]);
        if($businessPhoneNumber != null)
            $profile->phoneNumbers()->create(['phone_number'=>$businessPhoneNumber, 'phone_type'=>"business"]);


        return redirect()->route('profiles.edit', ['profile'=>$user->username]);

        /*

        /*$user = Auth::user();
        $countryId = $request->get('country_id');
        $maritalStatusId = $request->get('marital_status_id');
        $phoneNumber = $request->get('phone_number');
        $businessPhoneNumber = $request->get('business_phone_number');

        $profile = new Profile($request->all());
        $profile->user()->associate($user);
        $profile->country()->associate($countryId);
        $profile->marital_status()->associate($maritalStatusId);
        $profile->save();

        if($phoneNumber != null)
            $profile->phoneNumbers()->create(['phone_number'=>$phoneNumber, 'phone_type'=>"phone"]);
        if($businessPhoneNumber != null)
            $profile->phoneNumbers()->create(['phone_number'=>$businessPhoneNumber, 'phone_type'=>"business"]);

        return redirect()->route('profiles.edit', ['profile'=>$user->username]);*/
    }


    /**
     * Display the specified resource.
     *
     * @param $username
     * @param \Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param int $id
     */

    public function show($username)
	{
        $user = $this->users->getFirstBy('username', $username, ['profile','profile.photos', 'profile.marital_status', 'profile.phoneNumbers', 'profile.announcements', 'groups']);
        $profile = $user->profile;
        $carbon = new Carbon($profile->birthday);
        $birthday = $carbon->format('d.M Y');

        $currentUser = Auth::user();
        $currentUser->load("myBook");
        $myBook = $currentUser->myBook()->first();
        $profile->birthday = $birthday;

        // $groups = $user->groups->get('*');
        $announcements = $profile->announcements->where('public', 1)->first();
        $photos = $profile->photos;

        return view('profile.show', compact('user',"profile", 'currentUser', "birthday", 'myBook', "announcements", "photos"));



        /*
        $user = $this->users->getFirstBy('username', $username, ['profile.photos', 'profile.marital_status', 'profile.phoneNumbers', 'profile.announcements', 'groups']);
        $profile = $user->profile;
        $groups = $user->groups->get('*');
        $photos = $profile->photos;
        $homePhone = $profile->phoneNumbers->where('phone_type', 'home')->first();
        $mobilePhone = $profile->phoneNumbers->where('phone_type', 'mobile')->first();
        $businessPhone = $profile->phoneNumbers->where('phone_type', 'business')->first();
        $announcements = $profile->announcements->where('public', 1)->first();

        return view('profile.show', compact('user','profile','photos', 'groups', 'announcements','homePhone','mobilePhone','businessPhone'));

    */
        //show user's profiles
        //get the first by username with his profile
        /*$user = $this->users->getFirstBy('username', $username,['profile', 'groups']);
        $profile = $user->profile;

        //if profile is set get the photos, phoneNumbers, company and announcments
        if(isset($profile))
        {
            $profile = $this->profile->getById($profile->id,['photos','phoneNumbers','company','announcements']);
            //$user = $profile->user->with('groups')->first();


            $groups = $user->groups()->get();

            $photos = $profile->photos()->get();
            $homePhone = $profile->phoneNumbers()->where('phone_type', 'home')->first();
            $mobilePhone = $profile->phoneNumbers()->where('phone_type', 'mobile')->first();
            $businessPhone = $profile->phoneNumbers()->where('phone_type', 'business')->first();
            $announcements = $profile->announcements()->where('public', 1)->first();

            return view('profile.show', compact('user','profile','photos', 'groups', 'announcements','homePhone','mobilePhone','businessPhone'));
        }

        return redirect('searches');*/
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  string $username
	 * @return \Illuminate\View\View
	 */
	public function edit($username)
	{


	    $user = User::with([
	        'profile',
            'profile.photos',
            'profile.country',
            'profile.phoneNumbers',
            'profile.marital_status'
        ])->where('username', $username)->first();


	    //$profile = $user->profile->first();
        $profile = $user->profile;

	    $phoneNumber = $profile->phoneNumbers->where('phone_type', 'phone')->first();
	    $businessPhoneNumber = $profile->phoneNumbers->where('business', 'phone')->first();
        $photosIndexUrl = route("api.profiles.photos.index", ["profile"=>$profile->id]);
        $photosStoreUrl = route("api.profiles.photos.store", ["profile"=>$profile->id]);
        $announcementUrl = route('profiles.announcements.store', ['profile'=>$profile->id]);
        $announcementApiUrl = route('api.profiles.announcements.store', ['profile'=>$profile->id]);
        $announcementID = 0;

        /*if ($profile->has('announcements') && $profile->announcements->count() != 0)
        {
            $announcement = $profile->announcements->first();
            $announcementUrl = route('announcements.update', ['announcement'=>$announcement->id]);
            $announcementApiUrl = route('api.announcements.update', ['announcement'=>$announcement->id]);
            $announcementID = $announcement->id;
        }*/


	    return view("profile.edit", compact('profile', 'phoneNumber', 'announcementUrl', 'announcementID', 'announcementApiUrl', 'photosStoreUrl', 'photosIndexUrl', 'businessPhoneNumber'));

        //return redirect()->back();






        /*$user = User::with(["profile.announcements", "profile.photos", "profile.marital_status"])->where('username', $username)->first();
        $profile = $user->profile;
        $profileId = $profile->id;
        $url = "";
        $birthday = null;

        if(isset($profile->birthday))
            $birthday = $profile->birthday->format("Y-m-d");

        $photosIndexUrl = route('api.profiles.photos.index', ['profile'=>$profileId, 'api_token'=>Auth::user()->api_token]);
        $photosStoreUrl = route("api.profiles.photos.store", ["profile"=>$profileId, 'api_token'=>Auth::user()->api_token]);

        $announcement = $profile->announcements->first();
        $mobilePhone = $profile->phoneNumbers()->where('phone_type', 'mobile')->first();
        $homePhone = $profile->phoneNumbers()->where('phone_type', 'home')->first();
        $businessPhone = $profile->phoneNumbers()->where('phone_type', 'business')->first();

        isset($announcement) ? $announcement_body = $announcement->body : $announcement_body = "";
        isset($announcement) ? $announcement_id = $announcement->id : $announcement_id = 0;

        if($announcement_id > 0)
        {
            $url = route('announcements.update', ['announcements'=>$announcement_id]);
        }
        else
        {
            $url = route('announcements.store');
        }


        $marital_status_list = $this->maritalStatus->getAll();
        $countries = \App\Country::all();
        $photos = $profile->photos->get("*");

        return view('profile.edit', compact(
            "user",
            "profile",
            "profileId",
            "mobilePhone",
            "homePhone",
            "businessPhone",
            'marital_status_list',
            'countries',
            'photos',
            "announcement",
            'announcement_id',
            "announcement_body",
            "url",
            "birthday",
            'photosIndexUrl',
            'photosStoreUrl'
        ));

        */


    }

    public function getBirthdayAttribute($value)
    {
        if(empty($value) || $value == "0000-00-00")
        {
            return null;
        }

        return $value;
    }
}
