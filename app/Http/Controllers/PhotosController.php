<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Looll\Repo\Group\GroupRepository;
use App\Looll\Repo\Photo\PhotoRepository;
use App\Looll\Repo\Profile\ProfileRepository;
use Illuminate\Http\Request;
use Image;

class PhotosController extends Controller {

	private $profile;
	private $group;
	private $photo;
	public function __construct(ProfileRepository $profile, GroupRepository $group, PhotoRepository $photo)
	{
		$this->profile = $profile;
		$this->group = $group;
		$this->photo = $photo;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{


		/*
		 * if (Input::hasFile('file'))
			{
				(object) $file = (object) Input::file('file');
				(int) $profileId = (int) Input::get('profile_id');
				(int) $organisationId = (int) Input::get('organisation_id');
				(array) $input = (array) Input::only('filename','file_ori_size', 'file_upload_size', 'image_primary');

				(array) $file_entry = $this->file->upload($file);

				(boolean) $isValid = true;
				(string) $errors = "";

				$input['src'] = $file_entry['src'];
				$input['path'] = $file_entry['path'];

				(int) $photo_id = (int) $this->photo->create($input, $profileId, $organisationId);

				return Response::json(array('photo_id' => $photo_id, 'errors'=>$errors, 'isValid' => $isValid, 'profile_id'=>$profileId, 'organisation_id'=>$organisationId));

			}
		 */

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Requests\SavePhotoRequest $request)
	{
		$file =  $request->file('file');
		$groupId = $request->get('group_id');
		$profileId = $request->get('profile_id');
		$input = (array) $request->only('filename','file_ori_size', 'file_upload_size', 'primary_photo');

		$filename = $file->getClientOriginalName();
		$extension = $file->getClientOriginalExtension();
		$newFileName = str_random(60).'.'.$extension;
		$img = Image::make($file->getRealPath());

		\File::exists(users_photo_path()) or \File::makeDirectory(users_photo_path());
		$img->save(users_photo_path().$newFileName);
		$img->resize(200, null, function ($constraint) {
			$constraint->aspectRatio();
		});
		$img->save(users_photo_path().'200x200-'.$newFileName);

		$input['src'] = users_photo_src($newFileName);
		$input['path'] = users_photo_path().$newFileName;

		$photo = $this->photo->savePhoto($input,$profileId, $groupId);

		return $photo->toJson();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = \Input::all();
		$this->photo->updatePhoto($data, $id);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$photo = $this->photo->getById($id,[]);

		$file = \File::exists($photo->path);
		if($file) \File::delete($photo->path);

		$this->photo->delete($id);
	}

}
