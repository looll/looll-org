<?php

namespace App\Http\Controllers;

use App\Company;
use App\Tag;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CompanyTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Company $company
     * @return Application|Factory|View
     */
    public function index(Company $company)
    {
        return view('companies.tags.index', compact('company'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create(Company $company)
    {
        return \view('companies.tags.create', compact("company"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param Company $company
     * @return RedirectResponse
     */
    public function store(Request $request, Company $company)
    {
        $request->validate(['title'=>"required"]);
        $company->tags()->create($request->all());

        return redirect()->route('companies.tags.index', ['company'=>$company->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Company $company
     * @param Tag $tag
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Company $company, Tag $tag)
    {
        $tag->delete();
        return redirect()->route("companies.tags.index", ["company" => $company->id]);
    }
}
