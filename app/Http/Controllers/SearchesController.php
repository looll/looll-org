<?php namespace App\Http\Controllers;

use Algolia\AlgoliaSearch\Exceptions\NotFoundException;
use App\Group;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Looll\Repo\Group\GroupRepository;
use App\Looll\Repo\PhoneNumber\PhoneNumberRepository;
use App\Looll\Repo\Profile\ProfileRepository;
use App\PhoneNumber;
use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Intervention\Image\Image;
use mysql_xdevapi\Exception;
use Symfony\Component\Serializer\Tests\Fixtures\DenormalizerDecoratorSerializer;

class SearchesController extends Controller
{
    public function index(Request $request)
    {
        $query = $request->get('q');
        $groups = null;
        $profiles = null;
        $phoneNumbers = null;

        try {




            $profiles = Profile::search($query)->get();
            $profiles->load(['photos'=>function($q)
            {
                return $q->where('primary_photo', 1);
            }]);

            $groups = Group::search($query)->get();
            $groups->load("photos");

        }catch (NotFoundException$e)
        {

        }


        return view('searches.index', compact('profiles', 'groups'));
        //dd($profiles);



    }
}