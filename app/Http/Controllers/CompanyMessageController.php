<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CompanyMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Company $company
     * @return Application|Factory|View
     */
    public function index(Company $company)
    {
        $company->load('messages');
        $messages = $company->messages->all();


        return view("companies.messages.index", compact("company", "messages"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Company $company)
    {
        return view("companies.messages.create", compact("company"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, Company $company)
    {
        $data = $request->validate([
            "subject" => ["required"],
            "body" => ["required"]
        ]);

        $data["published_at"] = null;
        if($request->get('publish'))
        {
            $data["published_at"] = now();
        }

        $company->messages()->create($data);

        return redirect()->route("companies.messages.index", ["company"=>$company->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
