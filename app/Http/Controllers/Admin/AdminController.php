<?php

namespace App\Http\Controllers\Admin;

use App\Group;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Spatie\Analytics\Period;

class AdminController extends Controller
{
    public function index()
    {



        $users = User::where('email_verified_at',"!=", null)->count();
        $groups = Group::count();

        /*$startDate = Carbon::now()->subYear();
        $endDate = Carbon::now();
        Period::create($startDate, $endDate)
        $analyticsData = \Analytics::fetchTopReferrers(Period::days(30), 10);*/

        return view('admin.home', compact('users', 'groups'));
    }
}
