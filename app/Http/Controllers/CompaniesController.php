<?php
namespace App\Http\Controllers;

use App\Company;
use App\Looll\Repo\Country\CountryRepository;
use App\PhoneNumber;
use App\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use  \Illuminate\View\View;
use \Illuminate\Http\Response;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     *
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @param CountryRepository $country
     * @return View
     */
    public function create(CountryRepository $country)
    {
        $countries = $country->getAll();
        return view("companies.create", compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'name' => ["required", "max:255"],
            "tin"=>["required", "unique:companies", "numeric"],
            "contact.0.name" => ["required", "max:255"],
            "address" => ["required", "max:255"],
            "city" => ["required", "max:255"],
            "zip" => ["required", "max:255"],
            "url" => ["url"],
            "email" => ["required", "email:rfc,dns", "max:255"]
        ]);

        $company = new Company([
            "name" => $request->get('name'),
            "email" => $request->get("email"),
            "tin" => $request->get("tin"),
            "address" => $request->get("address"),
            "city" => $request->get("city"),
            "zip" => $request->get("zip"),
            "url" => $request->get("url")
        ]);
        $company->user()->associate(Auth::id());
        $company->country()->associate($request->get('country_id'));
        $company->save();

        $order = 1;
        array_map(function ($item)use($company, $order){
            if(!empty($item["name"]))
            {
                $company->contacts()->create(["name"=>$item["name"], "order_by"=>$order]);
                $order++;
            }
        }, $request->get('contact'));

        array_map(function ($item) use($company)
        {
            if(!empty($item["phone_number"]))
            {
                $company->phoneNumbers()->create(["phone_number" => $item["phone_number"], "phone_type"=>$item['phone_type']]);
            }
        }, $request->get('phone_number'));

        return redirect()->route('companies.edit', ["company"=>$company->id]);

    }

    /**
     * Display the specified resource.
     *
     * @param String $username
     * @return Application|Factory|View
     */
    public function show($username)
    {
        $user = User::with(["company"])->where('username', '=', $username)->first();



        $company = $user->company->first();


        return \view('companies.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Company $company
     * @param CountryRepository $country
     * @return View
     */
    public function edit(Company $company, CountryRepository $country)
    {
        $user = Auth::user();
        $countries = $country->getAll();

        $mainPhoneNumber = $company->phoneNumbers()->where('phone_type', 'main')->first();
        $secondPhoneNumber = $company->phoneNumbers()->where('phone_type', 'second')->first();
        $thirdPhoneNumber = $company->phoneNumbers()->where('phone_type', 'third')->first();

        $contact_1 = $company->contacts()->where('order_by', 1)->first();
        $contact_2 = $company->contacts()->where('order_by', 2)->first();
        $contact_3 = $company->contacts()->where('order_by', 3)->first();

        return \view("companies.edit", compact('company', 'user', 'contact_1', 'contact_2', 'contact_3', 'countries', 'mainPhoneNumber', 'secondPhoneNumber', 'thirdPhoneNumber'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Company $company
     * @return void
     */
    public function update(Request $request, Company $company)
    {
        $company->update($request->all());
        if(!empty($request->get('contact_1')))
        {
            $contact1 = $company->contacts()->findOrNew($request->get('contact_1_id'));
            $contact1->name = $request->get('contact_1');
            $contact1->order_by = 1;
            $contact1->save();
        }

        if(!empty($request->get('contact_2')))
        {
            $contact2 = $company->contacts()->findOrNew($request->get('contact_2_id'));
            $contact2->name = $request->get('contact_2');
            $contact2->order_by = 2;
            $contact2->save();

        }

        if(!empty($request->get('contact_3')))
        {
            $contact3 = $company->contacts()->findOrNew($request->get('contact_3_id'));
            $contact3->name = $request->get('contact_3');
            $contact3->order_by = 3;
            $contact3->save();
        }

        if(!empty($request->get('main_phone_number')))
        {
            $mainPhoneNumber = $company->phoneNumbers()->findOrNew($request->get('main_phone_number_id'));
            $mainPhoneNumber->phone_number = $request->get('main_phone_number');
            $mainPhoneNumber->phone_type = "main";
            $mainPhoneNumber->save();
        }

        if(!empty($request->get('second_phone_number')))
        {
            $secondPhoneNumber = $company->phoneNumbers()->findOrNew($request->get('second_phone_number_id'));
            $secondPhoneNumber->phone_number = $request->get('second_phone_number');
            $secondPhoneNumber->phone_type = "second";
            $secondPhoneNumber->save();
        }

        if(!empty($request->get('third_phone_number')))
        {
            $thirdPhoneNumber = $company->phoneNumbers()->findOrNew($request->get('third_phone_number_id'));
            $thirdPhoneNumber->phone_number = $request->get('third_phone_number');
            $thirdPhoneNumber->phone_type = "third";
            $thirdPhoneNumber->save();
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
