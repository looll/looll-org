<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Profiler\Profile;
use Image;


class ProfilePhotoController extends Controller {


	/**
	 * Display a listing of the resource.
	 *
	 * @param \App\Profile $profiles
	 */
	public function index(\App\Profile $profiles)
	{
		//$profile = $profiles->with(['photos'])->first();
		return $profiles->photos()->get()->toJson();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @param \App\Profile|Profile $profiles
	 */
	public function store(Request $request, \App\Profile $profiles)
	{
        $this->validate($request, ["file*"=>"required|image"]);

        $file = $request->file("file");
	    $path = $file->store('/photos/'.Auth::user()->username, ["disk"=>"images"]);

	    $data = Storage::disk("images")->get($path);
	    return $data;

		/*$this->validate($request, ["file*"=>"required|image"]);
		$file =  $request->file('file');

		$filename = $file->getClientOriginalName();
		$extension = $file->getClientOriginalExtension();
		$newFileName = time().str_random(60).'.'.$extension;
		$img = Image::make($file->getRealPath());

		\File::exists(users_photo_path()) or \File::makeDirectory(users_photo_path());


		if($img->width() > 800)
		{
			$img->resize(800, null, function ($constraint) {
				$constraint->aspectRatio();
			});
			$img->fit(800);
		}
		$img->save(users_photo_path().$newFileName);

		$img->resize(200, null, function ($constraint) {
			$constraint->aspectRatio();
		});


		$img->save(users_photo_path().'200x200-'.$newFileName);

		$profile = $profiles->with('photos')->first();
		$photos = $profile->photos->count();
		$photos > 0 ? $primary_photo = false : $primary_photo = true;

		$data = [
				"filename"=>$filename,
				"path" => users_photo_path().$newFileName,
				"src" => users_photo_src($newFileName),
				"thumbnail_src" =>users_photo_src('200x200-'.$newFileName),
				"primary_photo" => $primary_photo
		];

		return $profiles->photos()->create($data);*/
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($username)
	{
		dd('username');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
