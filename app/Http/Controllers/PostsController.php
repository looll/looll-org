<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class PostsController extends Controller
{
    public function index()
    {
        $user = \Auth::user();
        $user->load('posts');
        $posts = $user->posts()->published()->orderBy('published_at', 'DESC')->get();


        foreach ($posts as $post){
            $user->posts()->updateExistingPivot($post->id, ['read_by'=>Carbon::now()]);
        }

        return view('posts.index', compact('posts'));
    }
}
