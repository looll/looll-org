<?php

namespace App\Http\Controllers;

use App\Company;
use App\Hour;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class CompanyHourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Company $company
     * @return View
     */
    public function index(Company $company)
    {
        $company->load('hours');
        $hours = $company->hours;


        return view('companies.hours.index', compact('hours', 'company'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Company $company)
    {
        return \view("companies.hours.create", compact('company'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request, Company $company)
    {
        $request->validate([
            "day"=>["required"],
            "open_time"=>["required"],
            "close_time" => ["required"]
        ]);

        $company->hours()->create($request->all());

        return redirect()->route("companies.hours.index", ["company"=>$company->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Company $company
     * @param Hour $hour
     * @return Application|Factory|View
     */
    public function edit(Company $company, Hour $hour)
    {
        return \view("companies.hours.edit", compact("company", "hour"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Company $company
     * @param Hour $hour
     * @return RedirectResponse
     */
    public function update(Request $request, Company $company, Hour $hour)
    {
        $data = $request->all();
        $hour->update($data);

        return redirect()->route("companies.hours.index", ["company"=>$company->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Company $company
     * @param Hour $hour
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Company $company, Hour $hour)
    {
        $hour->delete();        return redirect()->route("companies.hours.index", ["company"=>$company->id]);
    }
}
