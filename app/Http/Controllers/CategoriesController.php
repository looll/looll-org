<?php

namespace App\Http\Controllers;

use App\Category;
use App\Looll\Repo\MyBook\MyBookRepository;
use App\MyBook;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    private $myBook;
    public function __construct(MyBookRepository $myBook)
    {
        $this->myBook = $myBook;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @internal param $
     */
    public function index(Request $request)
    {

            $user = $request->user();
            $myBook = $this->myBook->getById($user->myBook->id, ['categories']);
            $categories = $myBook->categories()->with(['profiles'=>function($q)
            {
                $q->with(['user', 'photos'=>function($q)
                {
                    $q->where('primary_photo', true);
                },'categories'=>function($q)
                {
                    $q->get(['categories.id', 'name']);
                }]);
            }])->orderBy("name", 'ASC')->get(['id','name','slug']);
            return $categories;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\SaveCategoryRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\SaveCategoryRequest $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
