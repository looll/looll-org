<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Looll\Repo\Group\GroupRepository;
use Illuminate\Http\Request;
use Input;

class GroupUserController extends Controller {

	protected $group;

	public function __construct(GroupRepository $group)
	{
		$this->group = $group;
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($groupId)
	{

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $groupId
	 * @param int $userId
	 * @return void
	 */
	public function update(Request $request, $groupId, $userId)
	{
		$data = $request->all();
		$this->group->updatePivot($groupId, $userId, $data);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $groupId
	 * @param int $userId
	 * @return void
	 */
	public function destroy($groupId, $userId)
	{
		$this->group->removeUser($groupId, $userId);

        return redirect()->route('groups.index');
	}
}
