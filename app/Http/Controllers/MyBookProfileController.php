<?php

namespace App\Http\Controllers;

use App\Category;
use App\MyBook;
use App\Profile;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MyBookProfileController extends Controller
{


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param MyBook $myBook
     * @return RedirectResponse
     */
    public function store(Request $request, MyBook $myBook): RedirectResponse
    {
        $profileId = $request->get('profile_id');
        $myBook->load("categories");
        $category = $myBook->categories->where("name", '=', "All")->first();
        $category->profiles()->attach($profileId);

        return back();


       /* $profileId = $request->get('profile_id');
        $myBook = MyBook::with('categories')->find($myBookId);
        $category = $myBook->categories()->where('name', 'All')->first();
        dd($profileId);;

        $category->profiles()->attach($profileId);

        return back();*/
    }
}
