<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Looll\Repo\Announcement\AnnouncementRepository;
use App\Looll\Repo\Country\CountryRepository;
use App\Looll\Repo\MaritalStatus\MaritalStatusRepository;
use App\Looll\Repo\PhoneNumber\PhoneNumberRepository;
use App\Looll\Repo\Profile\ProfileRepository;
use App\Looll\Repo\User\UserRepository;
use App\Profile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Input;

class UserProfileController extends Controller {

	private $users;
	private $profiles;
	private $countries;
	private $maritalStatus;
	private $annoucement;
	private $phoneNumbers;

	public function __construct(
		UserRepository $users,
		ProfileRepository $profiles,
		CountryRepository $countries,
		MaritalStatusRepository $maritalStatus,
		AnnouncementRepository $announcement,
		PhoneNumberRepository $phoneNumbers
	)
	{
		//$this->middleware('currentUser');
		$this->countries = $countries;
		$this->users = $users;
		$this->profiles = $profiles;
		$this->maritalStatus = $maritalStatus;
		$this->annoucement = $announcement;
		$this->phoneNumbers = $phoneNumbers;
	}

    /**
     * Store a newly created resource in storage.
     * @param $request
     * @param string $username
     *
     * @return string
     */
	public function store(Requests\SaveProfileRequest $request, $username)
	{
        $data = $request->all();
        $announcement = $data['announcement'];
        $announcement['published_at'] = Carbon::now()->format('Y-m-d H:i:s');
        $mobile_phones = $data['mobile'];
        $home_phones = $data['home'];
        $business_phones = $data['business'];

        $user = $this->users->getFirstBy("username", $username);
        $profile = $this->profiles->create($data, $user);


        /*
       $data = $request->all();
       $announcement = $data['announcement'];
       $announcement['published_at'] = Carbon::now()->format('Y-m-d H:i:s');
       $mobile_phones = $data['mobile'];
       $home_phones = $data['home'];
       $business_phones = $data['business'];

       $user = $this->users->getFirstBy("username", $username);
       $profile = $this->profiles->create($data, $user);

       if(!empty($announcement['body']))
       {
           $this->annoucement->create($announcement, $profile);
       }

       if(!empty($home_phones['phone_number']))
       {
           $this->phoneNumbers->create($home_phones, $profile);
       }

       if(!empty($mobile_phones['phone_number']))
       {
           $this->phoneNumbers->create($mobile_phones, $profile);
       }

       if(!empty($business_phones['phone_number']))
       {
           $this->phoneNumbers->create($business_phones, $profile);
       }

       return redirect()->back();

       /*$data = $request->all();

       $announcement = $data['announcement'];
       $announcement['published_at'] = Carbon::now()->format('Y-m-d H:i:s');
       $home_phones = $data['home'];
       $mobile_phones = $data['mobile'];
       $business_phones = $data['business'];

       //Create a profile.
       $user = $this->users->getFirstBy("username", $username);
       $profile = (object) $this->profiles->create($data, $user);

       //Save the phone numbers and announcements if he has any.
       if(!empty($announcement['body']))
       {
           $this->annoucement->create($announcement, $profile);
       }

       if(!empty($home_phones['phone_number']))
       {
           $this->phoneNumbers->create($home_phones, $profile);
       }

       if(!empty($mobile_phones['phone_number']))
       {
           $this->phoneNumbers->create($mobile_phones, $profile);
       }

       if(!empty($business_phones['phone_number']))
       {
           $this->phoneNumbers->create($business_phones, $profile);
       }

       //return json string
       //if(isset($announcementData) && !empty($announcementData)) $this->annoucement->createAnnouncement($announcementData, $profile);
       $url = route('users.profiles.update',['users'=>$username, 'profiles'=>$profile->id]);
       $profileId = $profile->id;
       $return = ['url'=>$url, 'profileId'=>$profileId]+$profile->toArray();

       if($request->ajax())
           return json_encode($return);

       return redirect()->back();*/
	}

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\SaveProfileRequest $request
     * @param string $username
     * @param Profile $profiles
     * @return array $input
     * @internal param int $profileId
     */
	public function update(Requests\SaveProfileRequest $request, $username, Profile $profiles)
	{
        $data = $request->all();
        $home_phones = $data['home'];
        $mobile_phones = $data['mobile'];
        $business_phones = $data['business'];
        $profileData = $request->get('profile');
        $home = [];
        $mobile = [];
        $business = [];
        $result = [];
        $profileId = $profiles->id;
        $this->profiles->update($profileData, $profileId);

        if(!empty($home_phones['phone_number']))
        {

            $home['home'] = $profiles->addToPhoneNumbers($home_phones, $profileId);
        }

        if(!empty($mobile_phones['phone_number']))
        {
            $mobile['mobile'] = $profiles->addToPhoneNumbers($mobile_phones, $profileId);
        }

        if(!empty($business_phones['phone_number']))
        {
            $business['business'] = $profiles->addToPhoneNumbers($business_phones, $profileId);
        }


	    /*$data = $request->all();
        $home_phones = $data['home'];
        $mobile_phones = $data['mobile'];
        $business_phones = $data['business'];
        $profileData = $request->get('profile');
        $home = [];
        $mobile = [];
        $business = [];
        $result = [];
        $profileId = $profiles->id;
        $this->profiles->update($profileData, $profileId);

        if(!empty($home_phones['phone_number']))
        {

            $home['home'] = $profiles->addToPhoneNumbers($home_phones, $profileId);
        }

        if(!empty($mobile_phones['phone_number']))
        {
            $mobile['mobile'] = $profiles->addToPhoneNumbers($mobile_phones, $profileId);
        }

        if(!empty($business_phones['phone_number']))
        {
            $business['business'] = $profiles->addToPhoneNumbers($business_phones, $profileId);
        }


        $result = array_merge($home, $mobile);
        $result = array_merge($result, $business);


        return json_encode($result);



        /*$data = $request->all();
        $home_phones = $data['home'];
        $mobile_phones = $data['mobile'];
        $business_phones = $data['business'];
        $home = [];
        $mobile = [];
        $business = [];
        $announcement = [];


        $this->profiles->update($data, $profileId);

        $profile = new Profile();

        if(!empty($data['announcement']['body']))
        $announcement['announcement'] = $profile->addToAnnouncements($data['announcement'], $profileId);

        if(!empty($home_phones['phone_number']))
        {
            $home['home'] = $profile->addToPhoneNumbers($home_phones, $profileId);
        }

        if(!empty($mobile_phones['phone_number']))
        {
            $mobile['mobile'] = $profile->addToPhoneNumbers($mobile_phones, $profileId);
        }

        if(!empty($business_phones['phone_number']))
        {
            $business['business'] = $profile->addToPhoneNumbers($business_phones, $profileId);
        }

        $result = array_merge($announcement, $home);
        $result = array_merge($result, $mobile);
        $result = array_merge($result, $business);

        return json_encode($result);
        */
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $userId
	 * @param int $profileId
	 */
	public function destroy($userId, $profileId)
	{
		$this->users->delete($userId);
		$this->profiles->delete($profileId);
	}

}
