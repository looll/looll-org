<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Looll\Repo\Group\GroupRepository;
use Illuminate\Http\Request;

class GroupChildController extends Controller
{
	private $groups;
	public function __construct(GroupRepository $groups)
	{
		$this->groups = $groups;
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($groupId)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $parentId
	 * @param int $groupId
	 * @return void
	 */
	public function update($parentId, $groupId)
	{
		$input = \Input::all();
		$group = $this->groups->getById($groupId, ['country']);

		$input["accepted_by_parent"] = (boolean) $input["accepted_by_parent"];
		$input["member_type_id"] = $group->member_type_id;
		$input["parent_id"] = $parentId;
		$input["country_id"] = $group->country->id;

		$this->groups->update($input, $groupId);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $parentId
	 * @param int $groupId
	 * @return mixed
	 */
	public function destroy($parentId, $groupId)
	{
		$remove = $this->groups->removeGroup($parentId, $groupId);

		return $remove;
	}

}
