<?php namespace App\Http\Controllers;

use App\Looll\Repo\Country\CountryRepository;
use App\Looll\Repo\Group\GroupRepository;
use App\Looll\Repo\User\UserRepository;
use Mail;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	private $country;
	private $users;
	private $groups;

	/**
	 * Create a new controller instance.
	 *
	 * @param CountryRepository $country
	 * @param UserRepository $users
	 */
	public function __construct(CountryRepository $country, UserRepository $users, GroupRepository $groups)
	{
		$this->users = $users;
		$this->groups = $groups;

		$this->middleware('guest');
		$this->country = $country;
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = $this->users->getRandomBy(8);
		$groups = $this->groups->getRandomBy(8);
		$photo = [];


		foreach($users as $user)
		{
			$photo[$user->id] = $user->profile->photos()->where('primary_photo', true)->first();
		}

		return view('welcome', compact('users', 'groups', 'photo'));
	}

}
