<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Looll\Mailers\UserMailer;
use App\Rules\CurrentPassword;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class UsersController extends Controller
{

	public function __construct()
	{
		$this->middleware('CurrentUser');
	}

	public function edit($id, Request $register)
	{
        $value = session('status');
		$user = $register->user();
		return view('users.edit', compact('id', 'user', 'value'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $userId
	 * @return Response
	 */
	public function update($userId, Request $request)
	{
        $user = \Auth::user();
        $this->validate($request,[
            'email' => 'required|email|max:255|unique:users,email,'.$user->id,
            'current_password'=>['required', new CurrentPassword],
            'password' => 'required|different:current_password|confirmed|min:6',
            'password_confirmation'=>"required"
        ]);
        $email = $request->get('email');
        $password = $request->get('password');



        if($user->email != $email)
        {
            $user->email = $email;
            $user->email_verified_at = null;
            $user->password = Hash::make($password);
            $user->save();
            $user->sendEmailVerificationNotification();


            return redirect(url('email/verify'));
        }

        $user->password = Hash::make($password);
        $user->save();



        $request->session()->flash('status', 'Your account was updated successfully!');
        return redirect()->back();
	}


}
