<?php

namespace App\Http\Controllers;

use App\Events\CreatedPost;
use App\Group;
use Illuminate\Http\Request;

use App\Post;

use App\Http\Requests;
use Validator;
use Event;
class GroupPostController extends Controller
{
    public function store(Request $request, Group $groups)
    {
        $validator = Validator::make($request->all(), [
            'subject' => 'required|max:50',
            'body' => 'required',
        ]);

        if ($validator->fails())
        {
            return redirect()->route('groups.edit', ['groups'=>$groups->id, 'tab'=>'post'])
                ->withErrors($validator)
                ->withInput();
        }


        $post = $groups->posts()->create($request->all());

        $post = Post::find($post->id);

        if($request->get('publish'))
        {
            Event::fire(new CreatedPost($post, $groups));

        }

        return redirect()->route('groups.edit', ['groups'=>$groups->id, 'tab'=>'post']);
    }

    public function update(Request $request, Group $groups, Post $posts)
    {
        $validator = Validator::make($request->all(), [
            'subject' => 'required|max:50',
            'body' => 'required',
        ]);

        if ($validator->fails())
        {
            return redirect()->route('groups.edit', ['groups'=>$groups->id, 'tab'=>'post'])
                ->withErrors($validator)
                ->withInput();
        }





        //$posts->fill($request->all())->save();

        if($request->get('publish'))
        {
            $posts->subject = $request->get('subject');
            $posts->body = $request->get('body');
            $posts->published_at = $request->get('published_at');
            $posts->draft = false;
            $posts->save();

            Event::fire(new CreatedPost($posts, $groups));
         }
         else
         {
             $posts->fill($request->all())->save();
         }

        return redirect()->route('groups.edit', ['groups'=>$groups->id, 'tab'=>'post']);
    }
}
