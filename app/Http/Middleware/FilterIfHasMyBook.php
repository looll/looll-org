<?php

namespace App\Http\Middleware;

use Closure;

class FilterIfHasMyBook
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Request::user();
        $myBook = $user->mybook;

        if($myBook == null)
        {
            return $next($request);
        }

        return redirect()->route('myBook.show', ['myBook'=>$myBook->id]);
    }
}
