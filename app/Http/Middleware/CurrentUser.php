<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class CurrentUser {

	protected $auth;
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{

	    if($request->route()->profile === $request->user()->username || (int) $request->route()->user === $request->user()->id)
	    {
            return $next($request);
        }

        return redirect()->route('profile',['profile'=>$request->user()->username]);
	}

}
