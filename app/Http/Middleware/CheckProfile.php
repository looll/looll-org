<?php
namespace App\Http\Middleware;
use App\Looll\Repo\User\UserRepository;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckProfile
{
    protected $user;

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    public function handle(Request $request, Closure $next)
    {
        $username = $request->route()->parameter('profile');
        $user = $this->user->getFirstBy('username', $username, ['profile']);

        if(Auth::check())
        {
            $currentUser = Auth::user();
            $currentUser->load('profile');

            if($currentUser->profile === null)
            {
               return redirect()->route('profiles.create');
            }

            if(($user->profile === null || $user->profile->published_at == null) && $username !== $currentUser->username)
            {
                return redirect('/home');
            }
        }
        else if(Auth::guest())
        {
            if($user->profile == null || $user->profile->published_at == null)
            {
                return redirect('/');
            }
        }

        return $next($request);

        /*if(Auth::check() )
        {

            //redirect()->route('profiles.create');
        }

        /*
        $username = $request->route()->parameter('profile');
        $user = $this->user->getFirstBy('username', $username, ['profile']);

        if($user !== null && $user->profile != null)
        {
            if(Auth::check() && (Auth::user()->username !== $username) && $user->profile->published === null)
            {
                redirect("/home");
            }
            elseif(Auth::guest() && $user->profile->published === null)
            {
                redirect("/");
            }

            return $next($request);

        }

        return redirect("/");

        /*$username = $request->route()->parameter('profile');
        $user = $this->user->getFirstBy('username', $username);
        // A user can not edit another user.
        if((Auth::user()->username == $username) && Auth::user()->hasRole("Person"))
        {
            if(Auth::user()->doesntHave("profile"))
                redirect()->route('profiles.create');
            return $next($request);
        }

        redirect()->route("home");
        // A user should has a profile, if not user should create a profile.
        // A current user can only edit his own profile
        //*/
    }
}