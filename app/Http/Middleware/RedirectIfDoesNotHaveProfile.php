<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfDoesNotHaveProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check())
        {
            $user = Auth::user();
            $user = User::with(['company', 'profile', 'roles'])->find(Auth::id());

            if($user->hasRole(["Person"]))
            {
                if($user->profile === null)
                {
                    return redirect()->route("profiles.create");
                }
            }
            elseif ($user->hasRole(["Company"]))
            {
                if($user->company === null)
                {
                    return redirect()->route("companies.create");
                }
            }
        }


        return $next($request);
    }
}
