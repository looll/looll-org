<?php namespace App\Http\Middleware;

use App\Looll\Repo\User\UserRepository;
use Closure;

class CheckUser {

    private $user;

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        $username = $request->route()->parameter('profile');
        $user = $this->user->getFirstBy('username', $username,['profile']);
        if($user === null)
        {
            return redirect('/');
        }

		return $next($request);
	}

}
