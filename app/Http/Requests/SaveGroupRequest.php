<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class SaveGroupRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$group = (int) $this->route()->getParameter('group');
		return [
			'name' => "required|unique:groups",
			'email' => "email",
		];
	}
}
