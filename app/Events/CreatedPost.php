<?php

namespace App\Events;


use App\Post;
use App\Group;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CreatedPost extends Event
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $post;
    public $group;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Post $post, $postable)
    {
        $this->post = $post;
        $this->postable = $postable;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
