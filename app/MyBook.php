<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyBook extends Model
{
    protected $fillable = ['name'];

    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    public function categoriesOrderByAndGet()
    {
        return $this->hasMany(Category::class)->orderBy("name", 'ASC');
    }


    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
